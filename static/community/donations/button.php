<html>
<head>
<title>Paypal Donation</title>
</head>
<body>
<form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_blank">
<input type="hidden" name="business" value="kde-ev-paypal@kde.org">
 <input type="hidden" name="cmd" value="_donations">
 <input type="hidden" name="lc" value="GB">
 <input type="hidden" name="item_name" value="KDE e.V.">
 <input type="hidden" name="item_number" value="Donation to KDE e.V.">
 <input type="hidden" name="currency_code" value="EUR">
 <input type="hidden" name="cn" value="Enter your name or other publically listed donation message">
 <input type="hidden" name="cbt" value="Return to www.kde.org">
 <input type="hidden" name="return" value="https://kde.org/community/donations/thanks_paypal.php">
 <input type="hidden" name="notify_url" value="https://kde.org/community/donations/notify.php">
 <input type="image" name="submit" border="0"  src="https://www.paypal.com/en_US/i/btn/btn_donate_LG.gif" alt="PayPal">
</form>
</body>
</html>
