---
aliases:
- ../announce-applications-15.04.3
changelog: true
date: 2015-07-01
description: KDE levererar KDE-program 15.04.3
layout: application
title: KDE levererar KDE-program 15.04.3
version: 15.04.3
---
1:e juli, 2015. Idag ger KDE ut den andra stabilitetsuppdateringen av <a href='../15.04.0'>KDE-program 15.04</a>. Utgåvan innehåller bara felrättningar och översättningsuppdateringar, och är därmed en säker och behaglig uppdatering för alla.

Mer än 20 registrerade felrättningar omfattar förbättringar av kdenlive, kdepim, kopete, ktp-contact-list, marble, okteta och umbrello.

Utgåvan inkluderar också versioner för långtidsunderhåll av Plasma arbetsrymder 4.11.21, KDE:s utvecklingsplattform 4.14.10 och Kontakt-sviten 4.14.10.
