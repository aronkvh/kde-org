---
aliases:
- ../announce-applications-16.12-rc
date: 2016-12-02
description: KDE levererar Program 16.12 leveranskandidat.
layout: application
release: applications-16.11.90
title: KDE levererar leveranskandidat av KDE-program 16.12
---
2:e december, 2016. Idag ger KDE ut leveranskandidaten av de nya versionerna av KDE-program. Med beroenden och funktioner frysta, fokuserar KDE-grupperna nu på att rätta fel och ytterligare finputsning.

Titta i <a href='https://community.kde.org/Applications/16.12_Release_Notes'>gemenskapens versionsfakta</a> för information om nya komprimerade arkiv, komprimerade arkiv som nu är baserade på KF5 och kända problem. Ett fullständigare meddelande kommer att vara tillgängligt för den slutliga utgåvan.

Utgåva 16.12 av KDE Program behöver en omfattande utprovning för att behålla och förbättra kvaliteten och användarupplevelsen. Verkliga användare är väsentliga för att upprätthålla hög kvalitet i KDE, eftersom utvecklare helt enkelt inte kan prova varje möjlig konfiguration. Vi räknar med dig för att hjälpa oss hitta fel tidigt, så att de kan krossas innan den slutliga utgåvan. Överväg gärna att gå med i gruppen genom att installera betaversionen och <a href='https://bugs.kde.org/'>rapportera eventuella fel</a>.
