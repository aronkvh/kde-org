---
aliases:
- ../announce-applications-16.08.1
changelog: true
date: 2016-09-08
description: KDE levererar KDE-program 16.08.1
layout: application
title: KDE levererar KDE-program 16.08.1
version: 16.08.1
---
8:e september, 2016. Idag ger KDE ut den första stabilitetsuppdateringen av <a href='../16.08.0'>KDE-program 16.08</a>. Utgåvan innehåller bara felrättningar och översättningsuppdateringar, och är därmed en säker och behaglig uppdatering för alla.

Mer än 45 registrerade felrättningar omfattar förbättringar av bland annat kdepim, kate, kdenlive, konsole, marble, kajongg, kopete och umbrello.

Utgåvan inkluderar också versioner för långtidsunderhåll av KDE:s utvecklingsplattform 4.14.24.
