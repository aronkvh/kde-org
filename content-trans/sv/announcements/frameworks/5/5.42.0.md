---
aliases:
- ../../kde-frameworks-5.42.0
date: 2018-01-13
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Allmänna ändringar

- Rättningar för AUTOMOC-varningar från cmake 3.10+
- Mer utbredd användning av kategoriserad loggning, för att normalt stänga av felsökningsutmatning (använd kdebugsetting för att sätta på den igen)

### Baloo

- balooctl status: Behandla alla argument
- Rätta etikettförfrågningar med flera ord
- Förenkla villkor för namnbyte
- Rätta felaktigt visat namn för UDSEntry

### Breeze-ikoner

- Rätta ikonnamnet "weather-none" -&gt; "weather-none-available" (fel 379875)
- Ta bort Vivaldi-ikon eftersom programmets originalikon fungerar perfekt med Breeze (fel 383517)
- Lägg till några saknade Mime-typer
- Breeze-ikoner lägg till skicka-dokument ikon (fel 388048)
- Uppdatera ikon för albumartist
- Lägg till stöd för labplot-editlayout
- Ta bort dubbletter och uppdatera mörkt tema
- Lägg till stöd för gnumeric Breeze-ikon

### Extra CMake-moduler

- Använd readelf för att hitta projektberoenden
- Introducera INSTALL_PREFIX_SCRIPT för att enkelt ställa in prefix

### KActivities

- Undvik krasch i kactivities om ingen anslutning till D-bus tillgänglig

### KConfig

- Dokumentation av programmeringsgränssnitt: Förklara hur KWindowConfig används från en dialogkonstruktor
- Avråd från användning av KDesktopFile::sortOrder()
- Rätta resultatet från KDesktopFile::sortOrder()

### KCoreAddons

- Utöka CMAKE_AUTOMOC_MACRO_NAMES också för egen byggning
- Matcha licensnyckel med spdx

### KDBusAddons

- Rätta detektering av absolutsökväg för cmake 3.5 på Linux
- Lägg till cmake-funktionen 'kdbusaddons_generate_dbus_service_file'

### KDeclarative

- QML-kontroller när inställningsmoduler skapas

### KDED

- Använd cmake-funktionen 'kdbusaddons_generate_dbus_service_file' från kdbusaddons för att generera tjänstfil för D-bus.
- Lägg till använd egenskap i definitionen av tjänstfil

### Stöd för KDELibs 4

- Informera användaren om modulen inte kan registreras med org.kde.kded5 och avsluta med ett fel
- Kompileringsrättningar för Mingw32

### KDocTools

- Lägg till entitet för Michael Pyne
- Lägg till identiteter för Martin Koller i contributor.entities
- Rätta Debian-post
- Lägg till entiteten Debian i general.entities
- Lägg till entiteten kbackup, det har importerats
- Lägg till entiteten latex, vi har redan 7 entiteter i olika index.docbooks i KF5

### KEmoticons

- Lägg till schema (file://). Det behövs när vi använder det i QML och vi har lagt till alla

### KFileMetaData

- Ta bort extrahering baserat på QtMultimedia
- Sök efter Linux istället för TagLib och undvik att bygga usermetadatawritertest på Windows
- Återställ nr. 6c9111a9 tills lyckad byggning och länkning är möjlig utan TagLib
- Ta bort beroende av TagLib, orsakad av en kvarbliven inkludering

### KHTML

- Tillåt slutligen att inaktivera felsökningsutmatning genom att använda kategoriserad loggning

### KI18n

- Behandla inte ts-pmap-compile som körbar
- Rätta en minnesläcka i KuitStaticData
- KI18n: Rätta ett antal dubbeluppslagningar

### KInit

- Ta bort kod som är omöjlig att nå

### KIO

- Hantera datum i kakor på rätt sätt vid körning med landsinställningar som inte är engelska (fel 387254)
- [kcoredirlister] Rätta skapa delsökväg
- Reflektera papperskorgens tillstånd i iconNameForUrl
- Vidarebefordra QComboBox-signaler istället för QComboBox radredigeringssignaler
- Rättade att webbgenvägar visade sina sökvägar istället för det läsbara namnet
- TransferJob: Rättning för fallet då readChannelFinished redan har skickats innan start anropas (fel 386246)
- Rätta krasch, förmodligen sedan Qt 5.10? (fel 386364)
- KUriFilter: Returnera inte ett fel för filer som inte finns
- Rätta skapa sökvägar
- Implementera en kfile-dialogruta där en egen grafisk komponent kan läggas till
- Verifiera att qWaitForWindowActive inte misslyckas
- KUriFilter: Konvertera för att inte använda KServiceTypeTrader
- Dokumentation av programmeringsgränssnitt: Använd klassnamn i rubriker för exempelskärmbilder
- Dokumentation av programmeringsgränssnitt: Hantera också KIOWIDGETS  exportmakron när QCH skapas
- Rätta hantering av KCookieAdvice::AcceptForSession (fel 386325)
- Skapade 'GroupHiddenRole' för KPlacesModel
- Vidarebefordra felsträng från uttag till KTcpSocket
- Omstrukturera och ta bort duplicerad kod i kfileplacesview
- Skicka ut signalen 'groupHiddenChanged'
- Omstrukturering av användning av dölja eller visa animering inne i KFilePlacesView
- Användare kan nu dölja en hel platsgrupp från KFilePlacesView (fel 300247)
- Implementering av dölja platsgrupper i KFilePlacesModel (fel 300247)
- [KOpenWithDialog] Tog bort att skapa redundant KLineEdit
- Tillägg av stöd för att ångra i BatchRenameJob
- Tillägg av BatchRenameJob i KIO
- Rätta att doxygen kodblock inte slutar med slutkod

### Kirigami

- Behåll bläddringsbart objekt interaktivt
- Riktigt prefix för ikoner också
- Rätta formulärstorleksändring
- Läs wheelScrollLines från kdeglobals om det finns
- Lägg till ett prefix för kirigami-filer för att undvika konflikter
- Några rättningar av statisk länkning
- Flytta Plasma stilar till plasma-framework
- Använd enkla citationstecken för att matcha tecken + QLatin1Char
- Formulärlayout

### KJobWidgets

- Erbjud programmeringsgränssnittet QWindow för dekorationer i KJobWidgets::

### KNewStuff

- Begränsa begäran av cachestorlek
- Kräv samma interna version som du bygger
- Förhindra att globala variabler används efter de har frigjorts

### KNotification

- [KStatusNotifierItem] "Återställ" inte position för grafisk komponent då den först visas
- Använd positioner för föråldrade ikoner i systembrickan för åtgärderna minimera och återställ
- Hantera positioner för vänsterklick med musen på föråldrade ikoner i systembrickan
- Gör inte den sammanhangsberoende menyn till ett fönster
- Tillägg av förklarande kommentar
- Lat instansiering och laddning av insticksprogram i KNotification

### Ramverket KPackage

- Invalidera körtidscache vid installation
- Experimentellt stöd för inläsning av rcc-filer i kpackage
- Kompilera med Qt 5.7
- Rätta paketindexering och lägg till körtidscache
- Ny metod KPackage::fileUrl()

### Kör program

- [RunnerManager] Bråka inte med trådantal i ThreadWeaver

### KTextEditor

- Rätta matchning av jokertecken för modrader
- Rätta en regression orsakad av att ändra bakstegstangentens beteende
- Konvertera programmeringsgränssnitt som inte avråds från som redan används på andra platser (fel 386823)
- Lägg till saknad inkludering för std::array
- MessageInterface: Lägg till CenterInView som ytterligare position
- Uppstädning av QStringList initieringslista

### Ramverket KWallet

- Använd riktigt sökväg till körbart tjänstprogram för att installera D-Bus tjänsten för kwalletd på Win32

### Kwayland

- Rätta inkonsekvent namngivning
- Skapa gränssnitt för att skicka serverns dekorationspaletter
- Inkludera std::bind funktioner explicit
- [server] Lägg till metoden IdleInterface::simulateUserActivity
- Rätta regression orsakad av stöd för bakåtkompatibilitet i datakälla
- Lägg till stöd för version 3 av gränssnitt för dataenhetshanterare (fel 386993)
- Begränsa exporterade och importerade objekts räckvidd till testen
- Rätta fel i WaylandSurface::testDisconnect
- Lägg till explicit AppMenu-protokoll
- Rätta att genererad fil utesluts från automoc-funktion

### KWidgetsAddons

- Rätta krasch i setMainWindow på Wayland

### KXMLGUI

- Dokumentation av programmeringsgränssnitt: Få doxygen att täcka sessionsrelaterade makron och funktioner igen
- Koppla bort shortcutedit slot när komponenten förstörs (fel 387307)

### NetworkManagerQt

- 802-11-x: Stöd för metoden PWD EAP

### Plasma ramverk

- [Air theme] Lägg till grafisk för förlopp på aktivitetsrad (fel 368215)
- Mallar: Ta bort enstaka * från licenshuvuden
- Gör packageurlinterceptor så lik ingen åtgärd som möjligt
- Återställ "Riv inte ner återgivning och annat pågående arbete när Svg::setImagePath anropas med samma argument"
- Flytta Plasma-stilar för kirigami hit
- Rullningslister som försvinner på mobil
- Återanvänd instans av KPackage mellan PluginLoader och Applet
- [AppletQuickItem] Ställ bara in QtQuick stilen Controls 1 en gång per gränssnitt
- Ange inte en fönsterikon i Plasma::Dialog
- [RTL] - Justera markerad text på ett riktigt sätt för höger-till-vänster (fel 387415)
- Initiera skalfaktor till den senaste skalfaktorn inställd för en instans
- Återställ "Initiera skalfaktor till den senaste skalfaktorn inställd för en instans"
- Uppdatera inte när det underliggande FrameSvg är blockerat för omritning
- Initiera skalfaktor till den senaste skalfaktorn inställd för en instans
- Flytta kontroll med if in i #ifdef
- [FrameSvgItem] Skapa inte onödiga noder
- Riv inte ner återgivning och annat pågående arbete när Svg::setImagePath anropas med samma argument

### Prison

- Sök också efter qrencode med felsökningssuffix

### QQC2StyleBridge

- Förenkla och försök inte blockera mushändelser
- Om ingen wheel.pixelDelta, använd globala rullningsrader för mushjul
- Skrivbordets flikrader har olika bredd för varje flik
- Säkerställ att storlekstips inte är 0

### Sonnet

- Exportera inte interna körbara hjälpprogram
- Sonnet: Rätta felaktigt språk för förslag i texter med flera språk
- Ta bort uråldrig och felaktig provisorisk lösning
- Orsaka inte cirkulär länkning på Windows

### Syntaxfärgläggning

- Färgläggningsindexerare: Varna för kontextbyte fallthroughContext="#stay"
- Färgläggningsindexerare: Varna om tomma attribut
- Färgläggningsindexerare: Aktivera fel
- Färgläggningsindexerare: Rapportera oanvända itemData och saknade itemData
- Prolog, RelaxNG, RMarkDown: Rätta färgläggningsproblem
- Haml: Rätta ogiltiga och oanvända itemData
- ObjectiveC++: Ta bort duplicerade kommentarsammanhang
- Diff, ObjectiveC++: Städning och rättning av färgläggningsfiler
- XML (Debug): Rätta felaktig regel DetectChar
- Färgläggningsindexerare: Stöd kontextkontroll över indexerare
- Återställ: Lägg till GNUMacros i gcc.xml igen, använd av isocpp.xml
- email.xml: Lägg till *.email i filändelserna
- Färgläggningsindexerare: Kontrollera om oändliga snurror finns
- Färgläggningsindexerare: Kontrollera om tomma kontextnamn och reguljära uttryck finns
- Rätta referens av nyckelordslistor som inte finns
- Rätta enkla fall av duplicerade sammanhang
- Rätta duplicerade itemData
- Rätta reglerna DetectChar och Detect2Chars
- Färgläggningsindexerare: Kontrollera nyckelordslistor
- Färgläggningsindexerare: Varna om duplicerade sammanhang
- Färgläggningsindexerare: Kontrollera om duplicerade itemData finns
- Färgläggningsindexerare: Kontrollera DetectChar och Detect2Chars
- Validera att itemData finns för alla attribut

### Säkerhetsinformation

Den utgivna koden GPG-signerad genom att använda följande nyckel: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Primärt nyckelfingeravtryck: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB

Det går att diskutera och dela med sig av idéer om den här utgåvan via kommentarssektionen i <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>artikeln på Dot</a>.
