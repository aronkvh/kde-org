---
aliases:
- ../announce-applications-19.08.0
changelog: true
date: 2019-08-15
description: KDE Ships Applications 19.08.
layout: application
release: applications-19.08.0
title: KDE выпускает KDE Applications 19.08.0
version: '19.08'
version_number: 19.08.0
---
{{< peertube "/6443ce38-0a96-4b49-8fc5-a50832ed93ce" >}}

{{% i18n_date %}}

The KDE community is happy to announce the release of KDE Applications 19.08.

Этот выпуск является частью обязательства KDE постоянно предоставлять улучшенные версии программ, которые мы поставляем нашим пользователям. Новые версии приложений обладают улучшенным и расширенным функционалом, что повышает удобство использования и стабильность приложений, таких как Dolphin, Konsole, Kate, Okular и всех ваших любимых утилит от KDE. Наша цель — обеспечить вашу продуктивность и сделать программное обеспечение KDE более простым и приятным в использовании.

We hope you enjoy all the new enhancements you'll find in 19.08!

## What's new in KDE Applications 19.08

More than 170 bugs have been resolved. These fixes re-implement disabled features, normalize shortcuts, and solve crashes, making your applications friendlier and allowing you to work and play smarter.

### Dolphin

Dolphin — это диспетчер файлов и папок от KDE, который теперь можно запускать из любого места с помощью новой глобальной комбинации клавиш <keycap>Meta + E</keycap>. Существует также новая функция, которая сводит к минимуму беспорядок на вашем рабочем столе. Если Dolphin уже запущен, когда вы открываете папки из других приложений, эти папки будут открываться в новой вкладке в существующем окне вместо нового экземпляра Dolphin. Обратите внимание, что это поведение включено по умолчанию, но его можно отключить.

Панель сведений (по умолчанию расположенная справа от главной панели Dolphin) была улучшена. Например, вы можете настроить автоматическое воспроизведение мультимедийных файлов при выделении их на главной панели. Также вы можете выделять и копировать текст, выведенный на панель сведений. Если вы хотите изменить набор свойств, которые показывает панель сведений, вы можете сделать это прямо на панели, так как Dolphin больше не открывает отдельное окно, когда вы приступаете к настройке этой панели.

Мы также исправили множество неточностей и мелких ошибок, гарантируя, что ваш опыт использования Dolphin в целом будет намного более приятным.

{{<figure src="/announcements/applications/19.08.0/dolphin_bookmark.png" alt=`Новая функция закладок в Dolphin` caption=`Новая функция закладок в Dolphin` width="600px" >}}

### Gwenview

Gwenview is KDE's image viewer, and in this release the developers have improved its thumbnail viewing feature across the board. Gwenview can now use a \"Low resource usage mode\" that loads low-resolution thumbnails (when available). This new mode is much faster and more resource-efficient when loading thumbnails for JPEG images and RAW files. In cases when Gwenview cannot generate a thumbnail for an image, it now displays a placeholder image rather than re-using the thumbnail of the previous image. The problems Gwenview had with displaying thumbnails from Sony and Canon cameras have also been solved.

Помимо изменений в отделе миниатюр, в Gwenview также внедрено новое меню «Опубликовать», которое позволяет отправлять изображения в различные места, а также корректно загружать и показывать файлы из удалённых папок, доступ к которым осуществляется с помощью KIO. Новая версия Gwenview также показывает гораздо больше свойств из метаданных​​ EXIF для изображений RAW.

{{<figure src="/announcements/applications/19.08.0/gwenview_share.png" alt=`Новое меню «Опубликовать» в Gwenview` caption=`Новое меню «Опубликовать» в Gwenview` width="600px" >}}

### Okular

Разработчики внесли множество улучшений в функцию комментирования в Okular — средство просмотра документов от KDE. Помимо улучшенного пользовательского интерфейса для диалогов настройки комментирования, теперь прямые линии могут проявлять различные визуальные эффекты на их концах, например, их можно превращать в стрелки. Другая вещь, доступная для комментариев, — это возможность развернуть или свернуть их все сразу.

Поддержка документов EPub в Okular также получила развитие в этой версии. Okular больше не завершается аварийно при попытке загрузить искажённые файлы ePub, а производительность при работе с большими файлами ePub значительно улучшилась. Список изменений в этом выпуске включает улучшенные границы страниц и улучшенную работу инструмента «маркер» в режиме презентации в комбинации с экраном высокого разрешения.

{{<figure src="/announcements/applications/19.08.0/okular_line_end.png" alt=`Настройка комментирования в Okular с новыми параметрами для концов прямых линий` caption=`Настройка комментирования в Okular с новыми параметрами для концов прямых линий` width="600px" >}}

### Kate

Thanks to our developers, three annoying bugs have been squashed in this version of KDE's advanced text editor. Kate once again brings its existing window to the front when asked to open a new document from another app. The \"Quick Open\" feature sorts items by most recently used, and pre-selects the top item. The third change is in the \"Recent documents\" feature, which now works when the current configuration is set up not to save individual windows’ settings.

### Konsole

Наиболее заметным изменением в Konsole, приложении-эмуляторе терминала от KDE, является расширение мозаичного вида. Теперь вы можете разделить основную панель по своему усмотрению, как по вертикали, так и по горизонтали. Подпанели могут быть снова разделены, как вы хотите. В этой версии также добавлена ​​возможность перетаскивать панели, чтобы вы могли легко изменить компоновку в соответствии с вашим рабочим процессом.

Кроме того, окно настройки было переработано, чтобы сделать его более понятным и простым в использовании.

{{< video src-webm="/announcements/applications/19.08.0/konsole-tabs.webm" >}}

### Spectacle

Spectacle — это приложение от KDE для создания снимков экрана, которое приобретает всё новые и новые интересные функции с каждой новой версией. Эта версия не является исключением, представляя новые функции в режиме съёмки с задержкой. Делая снимок с задержкой по времени, Spectacle показывает оставшееся время до создания снимка экрана в заголовке окна и делает его видимым в панели задач.

Still on the Delay feature, Spectacle's Task Manager button will also show a progress bar, so you can keep track of when the snap will be taken. And, finally, if you un-minimize Spectacle while waiting, you will see that the “Take a new Screenshot” button has turned into a \"Cancel\" button. This button also contains a progress bar, giving you the chance to stop the countdown.

Сохранение снимков экрана также имеет новый функционал. Когда снимок сохранён, Spectacle показывает в своём окне сообщение, из которого можно открыть снимок или содержащую его папку.

{{< video src-webm="/announcements/applications/19.08.0/spectacle_progress.webm" >}}

### Kontact

Kontact, приложение от KDE для работы с электронной почтой, календарём, контактами и общим набором утилит для групповой работы, приобрело поддержку цветных изображений для эмодзи, включённых в стандарт Unicode, поддержку Markdown в окне редактирования электронного письма. Новая версия KMail не только позволит вам сделать ваши письма красивыми, но благодаря интеграции с программами проверки грамматики, такими как LanguageTool и Grammalecte, поможет вам проверить и исправить текст.

{{<figure src="/announcements/applications/19.08.0/kontact_emoji.png" alt=`Панель выбора эмодзи` caption=`Панель выбора эмодзи` width="600px" >}}

{{<figure src="/announcements/applications/19.08.0/kmail_grammar.png" alt=`Проверка грамматики в KMail` caption=`Проверка грамматики в KMail` width="600px" >}}

При планировании событий электронные письма с приглашениями в KMail больше не удаляются после ответа на них. Теперь можно переместить событие из одного календаря в другой в редакторе события в KOrganizer.

И последнее, но не менее важное, KAddressBook теперь может отправлять SMS-сообщения контактам через KDE Connect, что приводит к более удобной интеграции вашего настольного компьютера с мобильными устройствами.

### Kdenlive

Новая версия Kdenlive, программного обеспечения от KDE для редактирования видео, содержит новый набор комбинаций клавиатура-мышь, которые помогут повысить вашу продуктивность. Например, вы можете изменить скорость клипа на монтажном столе, зажав Ctrl и потянув за клип, или активировать предварительный просмотр видеоклипов в виде миниатюр, удерживая <keycap>Shift</keycap> и передвигая указатель мыши над миниатюрой клипа в дереве проекта. Разработчики также приложили много усилий к увеличению удобства использования, сделав трёхточечные операции редактирования совместимыми с другими видеоредакторами, что вам наверняка понравится, если вы переходите на Kdenlive с другого редактора.

{{<figure src="https://cdn.kde.org/screenshots/kdenlive/19-08.png" alt=`Kdenlive 19.08.0` caption=`Kdenlive 19.08.0` width="600px" >}}
