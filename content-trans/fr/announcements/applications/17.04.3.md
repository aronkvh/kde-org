---
aliases:
- ../announce-applications-17.04.3
changelog: true
date: 2017-07-13
description: KDE publie les applications KDE 17.04.3
layout: application
title: KDE publie les applications KDE 17.04.3
version: 17.04.3
---
13 Juillet 2017. Aujourd'hui, KDE a publié la troisième mise à jour de consolidation pour les <a href='../17.04.0'>applications 17.04 de KDE</a>. Cette version ne contient que des corrections de bogues et des mises à jour de traduction, permettant une mise à jour sûre et appréciable pour tout le monde.

Plus de 25 corrections de bogues apportent des améliorations à kdepim, dolphin, dragonplayer, kdenlive, umbrello et bien d'autres.

Cette mise à jour inclut aussi une version de prise en charge à long terme pour la plate-forme de développement 4.14.34 de KDE.
