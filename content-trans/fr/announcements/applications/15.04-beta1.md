---
aliases:
- ../announce-applications-15.04-beta1
date: '2015-03-06'
description: KDE publie la version 15.04 « bêta » 1 des applications.
layout: application
title: KDE publie la première version « Beta » pour les applications de version 15.04
---
06 Mars 2015. Aujourd'hui, KDE publie la version « bêta » des nouvelles versions des applications de KDE. Les dépendances et les fonctionnalités sont stabilisées et l'équipe KDE se concentre à présent sur la correction des bogues et sur les dernières finitions.

Avec toutes les applications reposant sur KDE Frameworks 5, les publications de KDE Applications 15.04 nécessitent des tests poussés pour maintenir et améliorer la qualité ainsi que l'expérience utilisateur. Les utilisateurs ont un grand rôle à jouer dans la qualité de KDE car les développeurs ne peuvent tout simplement pas tester toutes les configurations possibles. Nous comptons sur vous pour nous aider à détecter les bogues suffisamment tôt pour pouvoir les corriger avant la livraison finale. N'hésitez pas à rejoindre l'équipe en installant la beta <a href='https://bugs.kde.org/'>et en signalant les bogues</a>.
