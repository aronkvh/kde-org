---
aliases:
- ../announce-applications-18.08.1
changelog: true
date: 2018-09-06
description: ကေဒီအီးသည် ကေဒီအီးအပ္ပလီကေးရှင်းများ ၁၈.၀၈.၁ တင်ပို့ခဲ့သည်
layout: application
title: ကေဒီအီးသည် ကေဒီအီးအပ္ပလီကေးရှင်းများ ၁၈.၀၈.၁ တင်ပို့ခဲ့သည်
version: 18.08.1
---
September 6, 2018. Today KDE released the first stability update for <a href='../18.08.0'>KDE Applications 18.08</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

More than a dozen recorded bugfixes include improvements to Kontact, Cantor, Gwenview, Okular, Umbrello, among others.

တိုးတက်မှုများ -

- The KIO-MTP component no longer crashes when the device is already accessed by a different application
- Sending mails in KMail now uses the password when specified via password prompt
- Okular now remembers the sidebar mode after saving PDF documents
