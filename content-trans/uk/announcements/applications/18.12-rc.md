---
aliases:
- ../announce-applications-18.12-rc
date: 2018-11-30
description: KDE випущено кандидат у випуски Програм 18.12.
layout: application
release: applications-18.11.90
title: KDE випущено близьку до готовності версію програм KDE 18.12
version_number: 18.11.90
version_text: 18.12 Release Candidate
---
30 листопада 2018 року. Сьогодні командою KDE випущено близький до готовності варіант нових версій програм. Від цього моменту заморожено залежності та список можливостей, — команда KDE зосереджує зусилля на виправлені вад та удосконаленні нової версії.

Ознайомитися із даними щодо архівів з кодом та списком відомих проблем можна за допомогою <a href='https://community.kde.org/Applications/18.12_Release_Notes'>сторінки нотаток щодо випуску</a>. Повніше оголошення буде зроблено, щойно ми приготуємо остаточний випуск.

Випуск 18.12 потребує ретельного тестування з метою підтримання та поліпшення якості та зручності у користуванні. Користувачі є надзвичайно важливою ланкою у підтриманні високої якості випусків KDE, оскільки розробникам просто не вистачить часу перевірити всі можливі комбінацій обладнання та налаштувань системи. Ми розраховуємо на вашу допомогу у якомога швидшому виявленні вад, щоб уможливити виправлення цих вад до остаточного випуску. Будь ласка, долучіться до команди тестувальників, встановивши нову версію <a href='https://bugs.kde.org/'>і повідомивши про всі виявлені вади</a>.
