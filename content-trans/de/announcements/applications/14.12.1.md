---
aliases:
- ../announce-applications-14.12.1
changelog: true
date: '2015-01-08'
description: KDE veröffentlicht die KDE-Anwendungen 14.12.1.
layout: application
title: KDE veröffentlicht die KDE-Anwendungen 14.12.1
version: 14.12.1
---
13. Januar 2015. Heute veröffentlicht KDE die erste Aktualisierung der <a href='../14.12.0'>KDE-Anwendungen 14.12</a>. Diese Veröffentlichung enthält nur Fehlerkorrekturen und aktualisierte Übersetzungen und ist daher für alle Benutzer eine sichere und problemlose Aktualisierung.

More than 50 recorded bugfixes include improvements to the archiving tool Ark, Umbrello UML Modeller, the document viewer Okular, the pronunciation learning application Artikulate and remote desktop client KRDC.

This release also includes Long Term Support versions of Plasma Workspaces 4.11.15, KDE Development Platform 4.14.4 and the Kontact Suite 4.14.4.
