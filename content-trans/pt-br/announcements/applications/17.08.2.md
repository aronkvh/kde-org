---
aliases:
- ../announce-applications-17.08.2
changelog: true
date: 2017-10-12
description: O KDE disponibiliza o KDE Applications 17.08.2
layout: application
title: O KDE disponibiliza o KDE Applications 17.08.2
version: 17.08.2
---
12 de Outubro de 2017. Hoje o KDE lançou a segunda actualização de estabilidade para as <a href='../17.08.0'>Aplicações do KDE 17.08</a>. Esta versão contém apenas correcções de erros e actualizações de traduções, pelo que será uma actualização segura e agradável para todos.

Mais de 25 correções de erros registradas incluem melhorias no Kontact, Dolphin, Gwenview, Kdenlive, Marble, Okular, entre outros.

Esta versão também inclui as versões de Suporte de Longo Prazo da Plataforma de Desenvolvimento do KDE 4.14.37.

As melhorias incluem:

- Um vazamento de memória e crash nas configurações da extensão Eventos do Plasma foi reparado
- Mensagens lidas não são mais removidas imediatamente do filtro de Não-lidas no Akregator.
- O importador do Gwenview agora usa data/hora EXIF
