---
aliases:
- ../../kde-frameworks-5.8.0
date: '2015-03-13'
layout: framework
libCount: 60
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
Novos frameworks:

- KPeople, que fornece acesso a todos os contatos e as pessoas que os mantêm
- KXmlRpcClient, para interagir com serviços XMLRPC

### Geral

- Um conjunto de correções para compilação com a futura versão 5.5 do Qt

### KActivities

- O serviço de classificação dos recursos foi finalizado

### KArchive

- Terminaram os erros nos arquivos ZIP com descritores de dados redundantes

### KCMUtils

- Restauração do KCModule::setAuthAction

### KCoreAddons

- KPluginMetadata: Adição do suporte para a chave <i>Hidden</i>

### KDeclarative

- Preferência da exposição das listas para QML com o <i>QJsonArray</i>
- Tratamento de <i>devicePixelRatios</i> não-predefinidos nas imagens
- Exposição do <i>hasUrls</i> no <i>DeclarativeMimeData</i>
- Permitir que os usuários configurem quantas linhas horizontais são desenhadas

### KDocTools

- Correção da compilação no MacOS X quando é usado o Homebrew
- Melhor estilo dos objetos multimídia (imagens, ...) na documentação
- Codificação dos caracteres inválidos nos caminhos usados nas XML DTDs, evitando alguns erros

### KGlobalAccel

- Tempo da ativação definido como propriedade dinâmica na QAction acionada.

### KIconThemes

- Correção do QIcon::fromTheme(xxx, valorSubstituição), que não iria devolver o valor de substituição

### KImageFormats

- O leitor de imagens PSD agora é agnóstico em relação à ordem dos bytes.

### KIO

- Descontinuação do UDSEntry::listFields e adição do método UDSEntry::fields, que devolve um QVector sem conversões pesadas.
- Sincronização do <i>bookmarkmanager</i> apenas se a alteração foi feita por este processo (erro <a href='https://bugs.kde.org/show_bug.cgi?id=343735'>343735</a>)
- Correção da inicialização do serviço dbus <i>kssld5</i>
- Implementação do <i>quota-used-bytes</i> e do <i>quota-available-bytes</i> do RFC 4331 para habilitar a informação de espaço livre no ioslave http.

### KNotifications

- Atraso da inicialização do áudio até que seja realmente necessário
- Correção da aplicação não-instantânea da configuração das notificações
- Correção da interrupção das notificações de áudio após a reprodução do primeiro arquivo

### KNotifyConfig

- Adição da dependência opcional do QtSpeech para reativar as notificações de fala.

### KService

- KPluginInfo: Suporte de <i>stringlists</i> como propriedades

### KTextEditor

- Adição da estatística de número de palavras na barra de status
- Modo VI: Correção da finalização inesperada ao remover a última linha no modo Linha Visual

### KWidgetsAddons

- Fazer o <i>KRatingWidget</i> reagir bem com o <i>devicePixelRatio</i>

### KWindowSystem

- O KSelectionWatcher e o KSelectionOwner podem ser usados sem dependerem do QX11Info.
- O KXMessages pode ser usado sem depender do QX11Info

### NetworkManagerQt

- Adição de novas propriedades e métodos do NetworkManager 1.0.0

#### Framework do Plasma

- Correção do <i>plasmapkg2</i> para os sistemas traduzidos
- Melhorias no layout das dicas
- Tornar possível aos plasmóides carregarem programas fora do pacote do Plasma ...

### Alterações no sistema de compilação (extra-cmake-modules)

- Extensão da macro <i>ecm_generate_headers</i> para também permitir arquivos de cabeçalho em CamelCase.h

Você pode discutir e compartilhar ideias sobre esta versão na seção de comentários do <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>artigo do Dot</a>.
