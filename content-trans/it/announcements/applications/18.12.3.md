---
aliases:
- ../announce-applications-18.12.3
changelog: true
date: 2019-03-07
description: KDE rilascia Applications 18.12.2.
layout: application
major_version: '18.12'
release: applications-18.12.3
title: KDE rilascia KDE Applications 18.12.3
version: 18.12.3
---
{{% i18n_date %}}

Oggi KDE ha rilasciato il terzo aggiornamento di stabilizzazione per <a href='../18.12.0'>KDE Applications 18.12</a>. Questo rilascio contiene solo correzioni di errori e aggiornamenti delle traduzioni, e costituisce un aggiornamento sicuro e gradevole per tutti.

Più di venti errori corretti includono, tra gli altri, miglioramenti a Kontact, Ark, Cantor, Dolphin, Filelight, JuK, Lokalize e Umbrello.

I miglioramenti includono:

- È stato risolto il caricamento degli archivi .tar.zstd in Ark
- Dolphin non va in errore quando si ferma un'attività di Plasma
- Il passaggio a una partizione differente non blocca più Filelight
