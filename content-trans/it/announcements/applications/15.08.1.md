---
aliases:
- ../announce-applications-15.08.1
changelog: true
date: 2015-09-15
description: KDE rilascia KDE Applications 15.08.1
layout: application
title: KDE rilascia KDE Applications 15.08.1
version: 15.08.1
---
15 settembre 2015. Oggi KDE ha rilasciato il primo aggiornamento di stabilizzazione per <a href='../15.08.0'>KDE Applications 15.08</a>. Questo rilascio contiene solo correzioni di errori e aggiornamenti delle traduzioni, e costituisce un aggiornamento sicuro e gradevole per tutti.

Gli oltre 40 bug corretti includono miglioramenti a kdelibs, kdepim, Kdenlive, Dolphin, Marble, Kompare, Konsole, Ark e Umbrello.

Questo rilascio include inoltre le versioni con supporto a lungo termine di KDE Development Platform 4.14.12.
