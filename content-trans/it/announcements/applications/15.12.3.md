---
aliases:
- ../announce-applications-15.12.3
changelog: true
date: 2016-03-16
description: KDE rilascia KDE Applications 15.12.3
layout: application
title: KDE rilascia KDE Applications 15.12.3
version: 15.12.3
---
15 marzo 2016. Oggi KDE ha rilasciato il terzo aggiornamento di stabilizzazione per <a href='../15.12.0'>KDE Applications 15.12</a>. Questo rilascio contiene solo correzioni di errori e aggiornamenti delle traduzioni, costituisce un aggiornamento sicuro e gradevole per tutti.

Gli oltre 15 bug corretti includono, tra gli altri, miglioramenti a kdepim, akonadi, ark, kblocks, kcalc, ktouch e umbrello.

Questo rilascio include inoltre le versioni con supporto a lungo termine di KDE Development Platform 4.14.18.
