---
aliases:
- ../announce-applications-19.08.1
changelog: true
date: 2019-09-05
description: KDE rilascia Applications 19.08.1.
layout: application
major_version: '19.08'
release: applications-19.08.1
title: KDE rilascia Applications 19.08.1
version: 19.08.1
---
{{% i18n_date %}}

Oggi KDE ha rilasciato il primo aggiornamento di stabilizzazione per <a href='../19.08.0'>KDE Applications 19.08</a>. Questo rilascio contiene solo correzioni di errori e aggiornamenti delle traduzioni, costituisce un aggiornamento sicuro e gradevole per tutti.

Più di venti errori corretti includono, tra gli altri, miglioramenti a Kontact, Dolphin, Kdenlive, Konsole e Step.

I miglioramenti includono:

- Sono state corrette diverse regressioni nella gestione delle schede in Konsole
- Dolphin si avvia di nuovo correttamente quando si trova in modalità a vista divisa
- L'eliminazione di un corpo morbido nel simulatore fisico Step non causa più blocchi
