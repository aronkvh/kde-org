---
aliases:
- ../announce-applications-15.08-beta
date: 2015-07-28
description: KDE rilascia Applications 15.08 Beta.
layout: application
release: applications-15.07.80
title: KDE rilascia la beta di KDE Applications 15.08
---
28 luglio 2015. Oggi KDE ha rilasciato la beta della nuova versione di KDE Applications. Con il &quot;congelamento&quot; di dipendenze e funzionalità, l'attenzione degli sviluppatori KDE è adesso concentrata sulla correzione degli errori e sull'ulteriore rifinitura del sistema.

Dato che molte applicazioni sono ora basate su KDE Frameworks 5, i rilasci di KDE Applications 15.08 hanno bisogno di una verifica accurata per mantenere e migliorare la qualità e l'esperienza utente. Gli utenti &quot;reali&quot; sono fondamentali per mantenere la qualità di KDE, perché gli sviluppatori non possono testare completamente ogni possibile configurazione. Contiamo su di voi per aiutarci a trovare gli errori il più presto possibile perché possano essere eliminati prima della versione finale. Valutate la possibilità di contribuire al gruppo di sviluppo installando la versione &quot;beta&quot; <a href='https://bugs.kde.org/'>e segnalando ogni problema</a>.
