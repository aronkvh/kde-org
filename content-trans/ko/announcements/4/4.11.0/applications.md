---
date: 2013-08-14
hidden: true
title: KDE 프로그램 4.11은 개인 정보 관리자 기능을 대폭 개선했으며 그 외 다양한 개선이 이뤄졌습니다
---
The Dolphin file manager brings many small fixes and optimizations in this release. Loading large folders has been sped up and requires up to 30&#37; less memory. Heavy disk and CPU activity is prevented by only loading previews around the visible items. There have been many more improvements: for example, many bugs that affected expanded folders in Details View were fixed, no &quot;unknown&quot; placeholder icons will be shown any more when entering a folder, and middle clicking an archive now opens a new tab with the archive contents, creating a more consistent experience overall.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.11.0/screenshots/send-later.png" caption=`Kontact의 새로운 나중에 보내기 작업 방식` width="600px">}}

## Kontact 제품군 개선

The Kontact Suite has once again seen significant focus on stability, performance and memory usage. Importing folders, switching between maps, fetching mail, marking or moving large numbers of messages and startup time have all been improved in the last 6 months. See <a href='http://blogs.kde.org/2013/07/18/memory-usage-improvements-411'>this blog</a> for details. The <a href='http://www.aegiap.eu/kdeblog/2013/07/news-in-kdepim-4-11-archive-mail-agent/'>archive functionality has seen many bug fixes</a> and there have also been improvements in the ImportWizard, allowing importing of settings from the Trojitá mail client and better importing from various other applications. Find more information <a href='http://www.progdan.cz/2013/07/whats-new-in-the-akonadi-world/'>here</a>.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.11.0/screenshots/kmail-archive-agent.png" caption=`보관 에이전트에서 이메일을 압축된 형태로 저장` width="600px">}}

This release also comes with some significant new features. There is a <a href='http://www.aegiap.eu/kdeblog/2013/05/news-in-kdepim-4-11-header-theme-33-grantlee-theme-generator-headerthemeeditor/'>new theme editor for email headers</a> and email images can be resized on the fly. The <a href='http://www.aegiap.eu/kdeblog/2013/07/new-in-kdepim-4-11-send-later-agent/'>Send Later feature</a> allows scheduling the sending of emails on a specific date and time, with the added possibility of repeated sending according to a specified interval. KMail Sieve filter support (an IMAP feature allowing filtering on the server) has been improved, users can generate sieve filtering scripts <a href='http://www.aegiap.eu/kdeblog/2013/04/news-in-kdepim-4-11-improve-sieve-support-22/'>with an easy-to-use interface</a>. In the security area, KMail introduces automatic 'scam detection', <a href='http://www.aegiap.eu/kdeblog/2013/04/news-in-kdepim-4-11-scam-detection/'>showing a warning</a> when mails contain typical phishing tricks. You now receive an <a href='http://www.aegiap.eu/kdeblog/2013/06/news-in-kdepim-4-11-new-mail-notifier/'>informative notification</a> when new mail arrives. and last but not least, the Blogilo blog writer comes with a much-improved QtWebKit-based HTML editor.

## Kate의 언어 지원 확장

Advanced text editor Kate introduces new plugins: Python (2 and 3), JavaScript & JQuery, Django and XML. They introduce features like static and dynamic autocompletion, syntax checkers, inserting of code snippets and the ability to automatically indent XML with a shortcut. But there is more for Python friends: a python console providing in-depth information on an opened source file. Some small UI improvements have also been done, including <a href='http://kate-editor.org/2013/04/02/kate-search-replace-highlighting-in-kde-4-11/'>new passive notifications for the search functionality</a>, <a href='http://kate-editor.org/2013/03/16/kate-vim-mode-papercuts-bonus-emscripten-qt-stuff/'>optimizations to the VIM mode</a> and <a href='http://kate-editor.org/2013/03/27/new-text-folding-in-kate-git-master/'>new text folding functionality</a>.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.11.0/screenshots/kstars.png" caption=`KStars에서 현재 위치에서 볼 수 있는 천문 이벤트 표시` width="600px">}}

## 다른 프로그램 개선

In the area of games and education several smaller and larger new features and optimizations have arrived. Prospective touch typists might enjoy the right-to-left support in KTouch while the star-gazer's friend, KStars, now has a tool which shows interesting events coming up in your area. Math tools Rocs, Kig, Cantor and KAlgebra all got attention, supporting more backends and calculations. And the KJumpingCube game now has features larger board sizes, new skill levels, faster responses and an improved user interface.

The Kolourpaint simple painting application can deal with the WebP image format and the universal document viewer Okular has configurable review tools and introduces undo/redo support in forms and annotations. The JuK audio tagger/player supports playback and metadata editing of the new Ogg Opus audio format (however, this requires that the audio driver and TagLib also support Ogg Opus).

#### KDE 프로그램 설치

KDE software, including all its libraries and its applications, is available for free under Open Source licenses. KDE software runs on various hardware configurations and CPU architectures such as ARM and x86, operating systems and works with any kind of window manager or desktop environment. Besides Linux and other UNIX based operating systems you can find Microsoft Windows versions of most KDE applications on the <a href='http://windows.kde.org'>KDE software on Windows</a> site and Apple Mac OS X versions on the <a href='http://mac.kde.org/'>KDE software on Mac site</a>. Experimental builds of KDE applications for various mobile platforms like MeeGo, MS Windows Mobile and Symbian can be found on the web but are currently unsupported. <a href='http://plasma-active.org'>Plasma Active</a> is a user experience for a wider spectrum of devices, such as tablet computers and other mobile hardware.

KDE software can be obtained in source and various binary formats from <a href='http://download.kde.org/stable/4.11.0'>download.kde.org</a> and can also be obtained on <a href='/download'>CD-ROM</a> or with any of the <a href='/distributions'>major GNU/Linux and UNIX systems</a> shipping today.

##### 패키지

Some Linux/UNIX OS vendors have kindly provided binary packages of 4.11.0 for some versions of their distribution, and in other cases community volunteers have done so. <br />

##### 패키지 위치

For a current list of available binary packages of which the KDE's Release Team has been informed, please visit the <a href='http://community.kde.org/KDE_SC/Binary_Packages#KDE_4.11.0'>Community Wiki</a>.

The complete source code for 4.11.0 may be <a href='/info/4/4.11.0'>freely downloaded</a>. Instructions on compiling and installing KDE software 4.11.0 are available from the <a href='/info/4/4.11.0#binary'>4.11.0 Info Page</a>.

#### 시스템 요구 사양

In order to get the most out of these releases, we recommend to use a recent version of Qt, such as 4.8.4. This is necessary in order to assure a stable and performant experience, as some improvements made to KDE software have actually been done in the underlying Qt framework.<br /> In order to make full use of the capabilities of KDE's software, we also recommend to use the latest graphics drivers for your system, as this can improve the user experience substantially, both in optional functionality, and in overall performance and stability.

## 오늘 같이 출시됨:

## <a href="../plasma"><img src="/announcements/4/4.11.0/images/plasma.png" class="app-icon" alt="The KDE Plasma Workspaces 4.11" width="64" height="64" /> Plasma Workspaces 4.11 Continues to Refine User Experience</a>

Gearing up for long term maintenance, Plasma Workspaces delivers further improvements to basic functionality with a smoother taskbar, smarter battery widget and improved sound mixer. The introduction of KScreen brings intelligent multi-monitor handling to the Workspaces, and large scale performance improvements combined with small usability tweaks make for an overall nicer experience.

## <a href="../platform"><img src="/announcements/4/4.11.0/images/platform.png" class="app-icon" alt="The KDE Development Platform 4.11"/> KDE Platform 4.11 Delivers Better Performance</a>

이번 KDE 플랫폼 4.11 릴리스는 안정성에 계속 초점을 맞추고 있습니다. 미래의 KDE 프레임워크 5.0 릴리스에 추가할 기능이 계속 구현 중이며, 안정 릴리스에는 Nepomuk 프레임워크 최적화가 추가되었습니다.
