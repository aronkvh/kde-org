---
aliases:
- ../../kde-frameworks-5.30.0
date: 2017-01-14
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Breeze pictogrammen

- Haguichi pictogrammen van Stephen Brandt toevoegen (dank)
- maak ondersteuning voor calligra-pictogrammen af
- cups-pictogram toevoegen dankzij colin (bug 373126)
- kalarm-pictogram bijwerken (bug 362631)
- krfb-app pictogram toevoegen (bug 373362)
- r MIME-type ondersteuning toevoegen (bug 371811)
- en meer

### Extra CMake-modules

- appstreamtest: behandel niet geïnstalleerde programma's
- Gekleurde waarschuwingen in ninja's uitvoer inschakelen
- Ontbrekende :: in API docs om styling van code te activeren
- libs/includes/cmakeconfig bestanden van host in Android toolchain negeren
- Gebruik van gnustl_shared met Android toolchain documenteren
- Gebruik nooit -Wl,--no-undefined op Mac (APPLE)

### Frameworkintegratie

- KPackage KNSHandler verbeteren
- Zoek naar de meer precies vereiste versie van AppstreamQt
- Ondersteuning van KNewStuff voor KPackage toevoegen

### KActivitiesStats

- Laat compileren met -fno-operator-names
- Haal niet meer gekoppelde hulpbronnen op wanneer een hiervan weg gaat
- Modelrol toegevoegd aan ophalen van de lijst van gekoppelde activiteiten voor een hulpbron op te halen

### KDE Doxygen hulpmiddelen

- Android toevoegen aan de lijst met beschikbare platforms
- ObjC++ broncodebestanden invoegen

### KConfig

- Genereer een exemplaar met KSharedConfig::Ptr voor singleton en argument
- kconfig_compiler: nulptr gebruiken in gegenereerde code

### KConfigWidgets

- De actie "Menubalk tonen" verbergen als alle menubalken lokaal zijn
- KConfigDialogManager: kdelibs3 klassen laten vallen

### KCoreAddons

- Typen stringlist/boolean teruggeven in KPluginMetaData::value
- DesktopFileParser: honoreer ServiceTypes veld

### KDBusAddons

- Python-bindings toevoegen voor KDBusAddons

### KDeclarative

- Introduceer org.kde.kconfig QML importeren met KAuthorized

### KDocTools

- kdoctools_install: laat het volledige pad voor het programma overeenkomen (bug 374435)

### KGlobalAccel

- [runtime] Introduceer een omgevingsvariabele KGLOBALACCEL_TEST_MODE

### KDE GUI-addons

- Python-bindings toevoegen voor KGuiAddons

### KHTML

- Stel de gegevens van de plug-in in zodat de ingebouwde afbeeldingenviewer werkt

### KIconThemes

- Informeer QIconLoader ook wanneer het bureaubladpictogramthema wordt gewijzigd (bug 365363)

### KInit

- Neem de eigenschap X-KDE-RunOnDiscreteGpu in acht bij starten van toepassing met klauncher

### KIO

- KIO::iconNameForUrl zal nu speciale pictogrammen voor xdg-locaties teruggeven zoals de Afbeeldingsmap van de gebruiker
- ForwardingSlaveBase: doorgeven van overschrijfvlag aan kio_desktop (bug 360487)
- KPasswdServerClient klasse verbeteringen en exporteren, de client API voor kpasswdserver
- [KPropertiesDialog] Kill optie "Plaats in systeemvak"
- [KPropertiesDialog] "Naam" van "Koppeling" van .desktop bestanden als bestandsnaam alleen-lezen is
- [KFileWidget] urlFromString gebruiken in plaats van QUrl(QString) in setSelection (bug 369216)
- Optie toevoegen om een app uit te voeren op een discrete grafische kaart naar KPropertiesDialog
- DropJob op de juiste manier beëindigen na loslaten op een uitvoerbaar programma
- Gebruik van logging-categorie op Windows repareren
- DropJob: gestarte kopieertaak na aanmaken uitsturen
- Ondersteuning toevoegen voor aanroepen van suspend() op een CopyJob voordat deze wordt gestart
- Ondersteuning toevoegen voor onmiddellijk onderbreken van jobs, minstens voor SimpleJob en FileCopyJob

### KItemModels

- Proxies bijwerken voor recent gerealiseerde klasse van bugs (behandeling van layoutChanged)
- Maak het mogelijk voor KConcatenateRowsProxyModel om te werken in QML
- KExtraColumnsProxyModel: modelindexen laten bestaan na uitzenden van layoutChange, niet ervoor
- Toekennen repareren (in beginRemoveRows) bij deselectie van een lege dochter van een geselecteerde dochter in korganizer

### KJobWidgets

- Voortgangsvensters geen focus geven (bug 333934)

### KNewStuff

- [GHNS Button] verbergen wanneer KIOSK-beperkingen van toepassing zijn
- Opzetten van de ::Engine repareren wanneer een absoluut pad van een configuratiebestand is gemaakt

### KNotification

- Een handmatige test voor Unity-starters toevoegen
- [KNotificationRestrictions] laat gebruiker een tekst met reden voor beperking kunnen specificeren

### KPackage-framework

- [PackageLoader] geef geen toegang tot ongeldige KPluginMetadata (bug 374541)
- beschrijving voor optie -t in man-pagina repareren
- Foutmelding verbeteren
- Het help-bericht voor --type repareren
- Installatieproces van KPackage bundels verbeteren
- Een kpackage-generic.desktop bestand installeren
- qmlc-bestanden uitsluiten van installatie
- Afzonderlijke plasmoids uit metadata.desktop en .json niet in lijst vermelden
- Extra reparatie voor pakketten met verschillende typen maar dezelfde id's
- Mislukking met cmake repareren wanneer twee pakketten met verschillend type dezelfde id hebben

### KParts

- De nieuwe checkAmbiguousShortcuts() uit MainWindow::createShellGUI aanroepen

### KService

- KSycoca: geen symbolische koppeling naar mappen volgen, het creëert het risico van recursie

### KTextEditor

- Reparatie: naar voren slepen van tekst resulteert in verkeerde selectie (bug 374163)

### KWallet Framework

- Minimaal vereiste GpgME++ versie 1.7.0 specificeren
- Keer "Als Gpgmepp niet wordt gevonden, probeer KF5Gpgmepp te gebruiken" om

### KWidgetsAddons

- Python-bindings toevoegen
- KToolTipWidget toevoegen, een tekstballon die een ander widget bevat
- Controles voor geldig ingevoerde fatums in KDateComboBox repareren
- KMessageWidget: meer donkere rode kleur gebruiken wanneer type fout is (bug 357210)

### KXMLGUI

- Bij opstarten waarschuwen over dubbelzinnige sneltoetsen (met een uitzondering voor Shift+Delete)
- MSWin en Mac hebben gelijk gedrag voor automatisch opslaan van venstergrootte
- Versie van toepassing weergeven in kop van dialoog Over (bug 372367)

### Oxygen-pictogrammen

- synchroniseren met breeze pictogrammen

### Plasma Framework

- De eigenschappen van renderType repareren voor verschillende componenten
- [ToolTipDialog] KWindowSystem::isPlatformX11() gebruiken dat in de cache is
- [Icon Item] impliciete grootte repareren wanneer pictogramgroottes wijzigen
- [Dialog] setPosition / setSize gebruiken in plaats van alles individueel in te stellen
- [Units] Maak eigenschap iconSizes constant
- Er is nu een globaal exemplaar "Eenheden" die geheugengebruik en aanmaaktijd van SVG-items vermindert
- [Pictogram-item] niet-vierkante pictogrammen ondersteunen (bug 355592)
- zoeken/vervangen van oude hard gecodeerde typen uit plasmapkg2 (bug 374463)
- X-Plasma-Drop* typen repareren (bug 374418)
- [Plasma ScrollViewStyle] achtergrond van schuifbalk alleen bij er boven zweven tonen
- #374127 repareren fout plaatsen van pop-ups uit dock-wins
- Plasma::Package API in PluginLoader als verouderd aanmerken
- Opnieuw controleren welke representatie gebruikt moet worden in setPreferredRepresentation
- [declarativeimports] QtRendering gebruiken op telefoonapparaten
- een leeg paneel altijd met "applets geladen" beschouwen (bug 373836)
- toolTipMainTextChanged uitzenden als het wijzigt in antwoord op een wijziging in de titel
- [TextField] sta uitschakelen van knop wachtwoord onthullen toe via KIOSK-restrictie
- [AppletQuickItem] start foutmelding ondersteunen
- Logica voor behandeling van pijlen in RTL-locales repareren (bug 373749)
- TextFieldStyle: implicitHeight waarde repareren zodat de tekstcursor gecentreerd is

### Sonnet

- cmake: zoek ook naar hunspell-1.6

### Accentuering van syntaxis

- Accentuering van Makefile.inc repareren door prioriteit aan makefile.xml toe te voegen
- Accentueer SCXML-bestanden als XML
- makefile.xml: vele verbeteringen, te lang om hier te tonen
- python-syntaxis: f-literals en verbeterde behandeling van tekenreeksen toegevoegd

### Beveiligingsinformatie

De vrijgegeven code is ondertekend met GPG met de volgende sleutel: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Vingerafdruk van primaire sleutel: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB

U kunt discussiëren en ideeën delen over deze uitgave in de section voor commentaar van <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>het artikel in the dot</a>.
