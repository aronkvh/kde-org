---
aliases:
- ../announce-applications-16.12.2
changelog: true
date: 2017-02-09
description: KDE stelt KDE Applicaties 16.12.2 beschikbaar
layout: application
title: KDE stelt KDE Applicaties 16.12.2 beschikbaar
version: 16.12.2
---
9 februari 2017. Vandaag heeft KDE de tweede stabiele update vrijgegeven voor <a href='../16.12.0'>KDE Applicaties 16.12</a> Deze uitgave bevat alleen bugreparaties en updates van vertalingen, die een veilige en plezierige update voor iedereen levert.

Meer dan 20 aangegeven reparaties van bugs, inclusief verbeteringen aan kdepim, dolphin, kate, kdenlive, ktouch, okular, naast andere.

Deze uitgave bevat ook ondersteuning op de lange termijn versies van KDE Development Platform 4.14.29.
