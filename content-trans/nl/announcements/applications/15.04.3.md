---
aliases:
- ../announce-applications-15.04.3
changelog: true
date: 2015-07-01
description: KDE stelt KDE Applicaties 15.04.3 beschikbaar
layout: application
title: KDE stelt KDE Applicaties 15.04.3 beschikbaar
version: 15.04.3
---
1 juli 2015. Vandaag heeft KDE de derde update voor stabiliteit vrijgegeven voor <a href='../15.04.0'>KDE Applicaties 15.04</a> Deze uitgave bevat alleen reparaties van bugs en updates van vertalingen, die een veilige en plezierige update voor iedereen levert.

Meer dan 20 aangegeven bugreparaties inclusief verbeteringen aan kdenlive, kdepim, kopete, ktp-kontact-list, marble, okteta en umbrello.

Deze uitgave bevat ook ondersteuning op de lange termijn versies van Plasma Workspaces 4.11.21, KDE Development Platform 4.14.10 en de Kontact Suite 4.14.10.
