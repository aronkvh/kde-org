---
aliases:
- ../../kde-frameworks-5.24.0
date: 2016-07-09
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Üldised muutused

- The list of supported platforms for each framework is now more explicit. Android has been added to the list of supported platforms in all frameworks where this is the case.

### Baloo

- DocumentUrlDB::del eeldus ainult siis, kui kataloogi järglased on tõesti olemas
- Vigaste päringute eiramine, milles on binaaroperaator ilma esimese argumendita

### Breeze'i ikoonid

- Palju uusi või täiustatud ikoone
- Veateate 364931 parandus: kasutaja jõudeoleku ikoon ei olnud nähtav (veateade 364931)
- Programmi lisamine nimeviidafailide teisendamiseks qrc aliasteks

### CMake'i lisamoodulid

- Teekide suhteliste asukohtade lõimimine APK-ga
- Use "${BIN_INSTALL_DIR}/data" for DATAROOTDIR on Windows

### KArchive

- Ensure extracting an archive does not install files outside the extraction folder, for security reasons. Instead, extract such files to the root of the extraction folder.

### KBookmarks

- KBookmarkManagerList'i puhastamine enne qApp'i töö lõpetamist ummikute vältimiseks DBus'i lõimega

### KConfig

- authorizeKAction() mätkimine iganenuks ja authorizeAction() eelistamine
- Korratavuse parandus ehitamisel utf-8 kodeeringu tagamisega

### KConfigWidgets

- KStandardAction::showStatusbar: soovitud toimingu tagastamine

### KDeclarative

- epoxy ei ole enam kohustuslik

### KDED

- [OS X] kded5 muutmine agendiks ja ehitamine tavalise rakendusena

### KDELibs 4 toetus

- KDETranslator klassi eemaldamine, kdeqt.po'd enam ei ole
- Asenduse dokumenteerimine use12Clock() tarbeks

### KDesignerPlugin

- KNewPasswordWidget'i toetuse lisamine

### KDocTools

- KDocTools'il lubatakse alati tuvastada vähemalt enda paigaldatud kraami
- CMAKE_INSTALL_DATAROOTDIR'i kasutamine docbook'i, mitte share'i otsimiseks
- qt5options'i manuaalilehekülje uuendamine qt 5.4 peale
- kf5options manuaalilehekülje docbook'i uuendamine

### KEmoticons

- glass'i teema liigutamine kde-look'i alla

### KGlobalAccel

- QGuiApplication'i kasutamine QApplication'i asemel

### KHTML

- Kontuuri omaduse shorthand päritud väärtuse rakendamise parandus
- Algse ja päritud piirderaadiuse käitlemine
- Omaduse hülgamine, kui tabasime tausta suuruse vigase pikkuse|protsendi
- cssText peab nende omaduste korral väljastama komaga eraldatud väärtused
- Taustaklipi parsimine parandus shorthand'is
- Tausta suuruse parsimise teostus shorthand'is
- Mustrite kordumisel omaduste märkimine määratuks
- Tausta omaduste päriluse parandus
- Algse ja päritud tausta suuruse omaduse rakendamise parandus
- khtml kxmlgui-faili lisamine Qt ressursifaili

### KI18n

- Ka kataloogide läbiotsimine väärtuste kärbitud variantide leidmiseks keskkonnamuutujas LANGUAGE
- Keskkonnamuutuja parsimise modifikaatori ja kooditabeli suhtes parandus (seda tehti vales järjekorras)

### KIconThemes

- RCC-faili ikooniteema automaatse laadimise ja kasutamise toetuse lisamine
- Ikooniteema pakkumise dokumenteerimine MacOS'i ja Windows'i peal, vt  https://api.kde.org/ frameworks/kiconthemes/html/index.html

### KInit

- Ajaületuse lubamine reset_oom_protection'is SIGUSR1 järel oodates

### KIO

- KIO: SlaveBase::openPasswordDialogV2 lisamine parema veakontrolli huvides, palun portida oma KIO-moodulid selle peale
- Parandus: KUrlRequester avas failidialoogi vales kataloogis (veateade 364719)
- Ebaturvaliste KDirModelDirNode* määramiste parandus
- cmake'i valiku KIO_FORK_SLAVES lisamine vaikeväärtuse määramiseks
- ShortUri filter: mailto:kasutaja@masin filtreerimise parandus
- OpenFileManagerWindowJob'i lisamine faili esiletõstmiseks kataloogis
- KRun: meetodi runApplication lisamine
- soundcloud'i otsingupakkuja lisamine
- OS X omastiiliga "macintosh" joondamise probleemi parandus

### KItemModels

- KExtraColumnsProxyModel::removeExtraColumn'i lisamine, mida vajab StatisticsProxyModel

### KJS

- kjs/ConfigureChecks.cmake - HAVE_SYS_PARAM_H kohane määramine

### KNewStuff

- Tagamine, et meil on suurus välja pakkuda (veateade 364896)
- "Allalaadimisdialoog annab otsad, kui kõik kategooriad puuduvad" parandus

### KNotification

- Tegumiriba märguande parandus

### KNotifyConfig

- KNotifyConfigWidget: meetodi disableAllSounds() lisamine (veateade 157272)

### KParts

- Lüliti lisamine KParts'il aknatiitlite käitlemise keelamiseks
- Meie rakenduse abimenüüsse annetamiskirje lisamine

### Kross

- QDialogButtonBox'i loenduri "StandardButtons" nime parandus
- Teegi laadimise esimese katse eemaldamine, sest me proovime libraryPaths nagunii läbi
- Krahhi vältimine, kui Kross'ile avaldatud meetod tagastab QVariant'i mittekolitavate andmetega
- Ei kasutata C stiilis määramisi void* sisse (veateade 325055)

### KRunner

- [QueryMatch] iconName'i lisamine

### KTextEditor

- Kerimisriba teksti eelvaatluse näitamine 250 ms viivituse järel
- eelvaatluse ja kraami peitmine vaate sisu kerimisel
- eellase määramine + tööriistavaade, mida, ma usun, on tarvis tegumivahetaja kirje vältimiseks Win10 peal
- "KDE Standard" eemaldamine kodeeringukastist
- Eelvaatluse vaikimisi voltimine
- Kriipsallajoonimise vältimine eelvaatluses ja rea paigutuse puhvri mürgitamise vältimine
- Valik "Volditud teksti eelvaatluse näitamine" on alati lubatud
- TextPreview: grooveRect'i kõrguse kohandamine, kui scrollPastEnd on lubatud
- Kerimisriba eelvaatlus: gtooveRect'i kasutamine, kui kerimisriba ei kasuta kogu kõrgust
- KTE::MovingRange::numberOfLines() lisamine samamoodi nagu KTE::Range'l
- Koodi voltimise eelvaatlus: hüpiku kõrguse määramine selliseks, et kõik peidetud read ära mahuksid
- Valiku lisamine volditud teksti eelvaatluse keelamiseks
- Režiimirea "voltimise eelvaatluse" tüübi tõeväärtuse lisamine
- Vaate ConfigInterface: 'voltimise elvaatluse' tüübi tõeväärtuse lisamine
- Tõeväärtustega KateViewConfig::foldingPreview() ja setFoldingPreview(bool() lisamine
- Omadus: teksti eelvaatluse näitamine hiirekursorit volditud koodiploki kohale viies
- KateTextPreview: setShowFoldedLines() ja showFoldedLines() lisamine
- Režiimiridade 'scrollbar-minimap' [bool] ja 'scrollbar-preview' [bool] lisamine
- Minikaardi kerimisriba vaikimisi lubamine
- Uus omadus: teksti eelvaatluse näitamine hiirekursori kerimisriba kohale viies
- KateUndoGroup::editEnd(): KTE::Range'i edastamine konstandi referentsiga
- VIM-režiimi kiirklahvide käitlemise parandus pärast käitumise muutust Qt 5.5-s (veateade 353332)
- Autimaatsulud: sümbolit ' teksti ei lisata
- ConfigInterface: kerimisriba minikaardi seadistusvõtme lisamine kerimisriba minikaardi lubamiseks/keelamiseks
- KTE::View::cursorToCoordinate() parandus, kui näha on ülemine teatevidin
- Emuleeritud käsuriba koodi ümberkorraldamine
- Kujutamise artefaktide parandus kerimisel, kui märguanded on nähtaval (veateade 363220)

### KWayland

- Sündmuse parent_window lisamine Plasma Window liidesele
- Osutusseadme/klaviatuuri/puutepadja ressursi häivtamise kohane käitlemine
- [server] surnud koodi kustutamine: KeyboardInterface::Private::sendKeymap
- [server] lõikepuhvrivalikule DataDeviceInterface'i käsitsi määramise toetuse lisamine
- [server] tagamine, et Resource::Private::get tagastab nullptr, kui on edastatud nullptr
- [server] ressursikontrolli lisamine QtExtendedSurfaceInterface::close'is
- [server] SurfaceInterface'i viida määramise tühistamine viidatud objektides hävitamise korral
- [server] tõrketeate parandus QtSurfaceExtension'i liideses
- [server] Resource::unbound signaali väljastamine seondamata käitlejast
- [server] Ei eeldata, kui hävitatakse ikka veel viidatav BufferInterface
- Destruktorinõude lisamine org_kde_kwin_shadow'le ja org_kde_kwin_shadow_manager'ile

### KWidgetsAddons

- Unihan'i andmete lugemise parandus
- KNewPasswordDialog'i miinimumsuuruse parandus (veateade 342523)
- Mitmetähendusliku konstruktori parandus MSVC 2015 peal
- Joondamisprobleemi parandus OS X omastiili "macintosh" kasutamisel (veateade 296810)

### KXMLGUI

- KXMLGui: indeksite ühendamise parandus rühmadele rakendatud xmlgui klientide eemaldamisel (veateade 64754)
- Hoiatust "faili ei leitud ettenähtud asukohast" ei näidata, kui faili üldse ei leita
- Meie rakenduse abimenüüsse annetamiskirje lisamine

### NetworkManagerQt

- peap'i  pealdist ei määrata peap'i versiooni alusel
- Võrguhalduril lastakse käitusajal versiooni kontrollida vältimaks kompileerimis- ja käitusaja kokkupõrget (veateade 362736)

### Plasma raamistik

- [kalender] noolenuppude peegeldamine paremalt vasakule keelte korral
- Plasma::Service::operationDescription() peab tagastama QVariantMap'i
- Põimitud konteinereid ei lisata containmentAt(pos)-sse (veateade 361777)
- värviteema parandus süsteemi taaskäivitamise ikoonils sisselogimisekraanil (veateade 364454)
- tegumiriba pisipiltide keelamine llvmpipe'is (veateade 363371)
- kaitsmine vigaste aplettide eest (veateade 364281)
- PluginLoader::loadApplet: valesti paigaldatud aplettide ühilduvuse taastamine
- õige kataloog PLASMA_PLASMOIDS_PLUGINDIR jaoks
- PluginLoader: veateate täiustus plugina versiooni ühildumise kohta
- Kontrolli parandus hoidmaks QMenu ekraanil mitmeekraanipaigutuse korral
- Uus konteineritüüp süsteemisalvele

### Solid

- Protsessori kehtivuse kontrolli parandus
- /proc/cpuinfo lugemise käitlemine Arm-protsessoritel
- Protsessorite leidmine allsüsteemi, mitte draiveri järgi

### Sonnet

- Abiprogrammi exe märkimine mittegraafiliseks rakenduseks
- nsspellcheck'i vaikimisi kompleerimise lubamine mac'i peal

Väljalaske üle arutada ja mõtteid jagada saab <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>meie uudistelehekülje artikli</a> kommentaarides.
