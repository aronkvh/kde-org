---
aliases:
- ../announce-applications-17.08.2
changelog: true
date: 2017-10-12
description: KDE toob välja KDE rakendused 17.08.2
layout: application
title: KDE toob välja KDE rakendused 17.08.2
version: 17.08.2
---
October 12, 2017. Today KDE released the second stability update for <a href='../17.08.0'>KDE Applications 17.08</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

Rohkem kui 25 teadaoleva veaparanduse hulka kuuluvad Kontacti, Dolphini, Gwenview, Kdenlive'i, Marble'i, Okulari ja teiste rakenduste täiustused.

This release also includes Long Term Support version of KDE Development Platform 4.14.37.

Täiustused sisaldavad muu hulgas:

- Mäluleke ja krahh Plasma sündmuste plugina seadistustes parandati
- Loetud artikleid ei eemaldata otskohe Akregatori lugemata uudiste filtrist.
- Gwenview importija kasutab nüüd EXIF-i kuupäeva/kellaaega
