---
aliases:
- ../../kde-frameworks-5.8.0
date: '2015-03-13'
layout: framework
libCount: 60
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
Novas infraestruturas:

- KPeople, fornece acceso a todos os contactos e as persoas ás que corresponden
- KXmlRpcClient, interacción con servizos de XMLRPC

### Xeral

- Varias correccións de construción para compilar coa vindeira Qt 5.5

### KActivities

- Completouse o servizo de puntuación de recursos

### KArchive

- Deixar de fallar con ficheiros ZIP con descritores de datos redundantes

### KCMUtils

- Restaurar KCModule::setAuthAction

### KCoreAddons

- KPluginMetadata: engadir a posibilidade de usar a clave Hidden

### KDeclarative

- Preferir expoñer listas a QML con QJsonArray
- Xestionar devicePixelRatios non predeterminado en imaxes
- Expoñer hasUrls en DeclarativeMimeData
- Permitir aos usuarios configurar o número de liñas horizontais que se debuxan

### KDocTools

- Corrixir a construción en MacOSX ao usar Homebrew
- Mellor estilo de obxectos de son e vídeo (imaxes, …) na documentación
- Codificar os caracteres incorrectos das rutas que se usan nos DTD de XML para editar erros

### KGlobalAccel

- Marca de tempo de activación definida como propiedade dinámica nunha QAction causada.

### KIconThemes

- Corrixir que QIcon::fromTheme(xxx, someFallback) non devolvese a reserva

### KImageFormats

- Facer que o lector de imaxes PSD funcione sen importar o tipo de endian.

### KIO

- Marcar como obsoleto UDSEntry::listFields e engadir o método UDSEntry::fields que devolve un QVector sen unha conversión custosa.
- Só sincronizar bookmarkmanager se o cambio o fixo este proceso (fallo 343735)
- Corrixiuse o inicio do servizo de D-Bus kssld5
- Engadir compatibilidade con quota-used-bytes e quota-available-bytes, de RFC 4331, para permitir información de espazo libre no escravo de entrada e saída de HTTP.

### KNotifications

- Atrasar a inicialización do son ata que se necesite
- Corrixir que a configuración das notificacións non se aplicase de maneira instantánea
- Corrixir que as notificacións de son se detivesen tras reproducir o primeiro ficheiro

### KNotifyConfig

- Engadir unha dependencia opcional de QtSpeech para activar de novo as notificacións faladas.

### KService

- KPluginInfo: permitir listas de cadeas como propiedades

### KTextEditor

- Engadir estatísticas de número de palabras na barra de estado
- vimode: corrixir a quebra ao retirar a última liña no modo de liña visual

### KWidgetsAddons

- Facer que KRatingWidget xestione devicePixelRatio

### KWindowSystem

- KSelectionWatcher e KSelectionOwner poden usarse sen depender de QX11Info.
- KXMessages pode usarse sen depender de QX11Info

### NetworkManagerQt

- Engadir novas propiedades e métodos de NetworkManager 1.0.0

#### Infraestrutura de Plasma

- Corrixir plasmapkg2 para sistemas traducidos
- Mellorar a disposición dos consellos
- Make it possible to let plasmoids to load scripts outside the plasma package ...

### Cambios do sistema de construción (extra-cmake-modules)

- Estender o macro ecm_generate_headers para permitir tamén cabeceiras con MaiúsculasIniciais.h

Pode comentar e compartir ideas sobre esta versión na sección de comentarios do <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>artigo do Dot</a>.
