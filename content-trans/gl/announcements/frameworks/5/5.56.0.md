---
aliases:
- ../../kde-frameworks-5.56.0
date: 2019-03-09
layout: framework
libCount: 70
---
### Baloo

- Substituír varios Q_ASSERT por comprobacións axeitadas
- Comprobar a lonxitude das cadeas para evitar quebrar cos URL «tags:/»
- [tags_kio] Corrixir a etiquetaxe de ficheiros locais comprobando só nos URL tag: se hai barras dobres
- Definir manualmente o intervalo de actualización do tempo restante
- Corrixir unha regresión de coincidencia de cartafoles incluídos explicitamente
- Limpar as entradas idempotentes da táboa de asociacións de tipos MIME
- [baloo/KInotify] Notificar se o cartafol se moved dun lugar non vixiado (fallo 342224)
- Xestionar correctamente os cartafoles que coincidan con subcadeas de cartafoles incluídos e excluídos
- [balooctl] Normalizar as rutas de inclusión e exclusión antes de usalas para a configuración
- Optimizar o operador de asignar copia de Baloo::File, corrixir Baloo::File::load(url)
- Usar o contido para determinar o tipo MIME (fallo 403902)
- [Extractor] Excluír os datos cifrados de GPG da indexación (fallo 386791)
- [balooctl] Interromper de verdade as ordes incorrectas en vez de limitarse a dicilo
- [balooctl] Engadir a axuda que faltaba de «config set», normalizar a cadea
- Substituír o isDirHidden recursivo por un iterativo que permite un argumento constante
- Asegurarse de que ao vixiante de inotify só se engaden directorios

### Iconas de Breeze

- Engadir a icona code-oss
- [breeze-icons] Engadir iconas de cámaras de vídeo
- [breeze-icons] Usar as novas iconas de suspender, hibernar e cambiar de usuario do tema de iconas Breeze
- Engadir versións de 16 px e de 22 px da icona de mando de xogo a devices/
- Facer consistentes os textos das xanelas emerxentes do tema Breeze
- Engadir iconas de batería
- Cambiar o nome das iconas «visibility» e «hint» a «view-visible» e «view-hidden»
- [breeze-icons] Engadir iconas de tarxeta SD e tarxeta de memoria monocromas e máis pequenas (fallo 404231)
- Engadir iconas de dispositivo para drons
- Cambiar as iconas dos tipos MIME de cabeceira e fonte de C e C++ ao estilo de círculo e liña
- Corrixir as sombras que faltaban nas iconas de tipo MIME de cabeceira de C e C++ (fallo 401793)
- Retirar a icona monocroma de preferencias de fonte
- Mellorar a icona de selección de fonte
- Usar a nova icona de estilo campá para todos os usuarios de preferences-desktop-notification (fallo 404094)
- [breeze-icons] Engadir versións de 16 px de gnumeric-font.svg e ligar gnumeric-font.svg con font.svg
- Engadir ligazóns simbólicas de preferences-system-users que apuntan á icona yast-users
- Engadir a icona edit-none

### Módulos adicionais de CMake

- Corrixir a clonación de releaseme cando isto se inclúe nun subdirectorio
- Novo módulo de atopar para Canberra
- Actualizar os ficheiros da cadea de ferramentas de Android á realidade
- Engadir unha comprobación de compilación a FindEGL

### KActivities

- Usar ordenación natural en ActivityModel (fallo 404149)

### KArchive

- Evitar que se chame a KCompressionDevice::open cando non hai ningunha infraestrutura dispoñíbel (fallo 404240)

### KAuth

- Informar á xente de que deberían estar usando KF5::AuthCore principalmente
- Compilar o noso asistente contra AuthCore, non contra Auth
- Introducir KF5AuthCore

### KBookmarks

- Substituír a dependencia de KIconThemes polo uso equivalente de QIcon

### KCMUtils

- Usar o nome do KCM na cabeceira do KCM
- Engadir o ifndef KCONFIGWIDGETS_NO_KAUTH que faltaba
- Adaptar a cambios en kconfigwidgets
- Sincronizar o recheo do módulo de QML para reflectir as páxinas de configuración do sistema

### KCodecs

- Corrección para CVE-2013-0779
- QuotedPrintableDecoder::decode: devolver falso ante un erro en vez de aseverar
- Marcar que KCodecs::uuencode non fai nada
- nsEUCKRProber e nsGB18030Prober::HandleData non quebran se aLen é 0
- nsBig5Prober::HandleData: non quebrar se aLen é 0
- KCodecs::Codec::encode: non aseverar ou quebrar se makeEncoder devolve nulo
- nsEUCJPProbe::HandleData: non quebrar se aLen é 0

### KConfig

- Escribir caracteres UTF-8 válidos sen escapar (fallo 403557)
- KConfig: xestionar correctamente as ligazóns simbólicas de directorios

### KConfigWidgets

- Omitir a proba de rendemento se non se atopa ningún ficheiro de esquema
- Engadir unha nota para KF6 sobre usar a versión principal de KF5::Auth
- Gardar en caché a configuración predeterminada de KColorScheme

### KCoreAddons

- Crear ligazóns tel: para os números de teléfono

### KDeclarative

- usar KPackage::fileUrl para permitir paquetes rcc de KCM
- [GridDelegate] Corrixir que as etiquetas longas se sobrepuxesen (fallo 404389)
- [GridViewKCM] mellorar o contraste e a lexibilidade dos botóns de cubrir entre liñas dos delegados (fallo 395510)
- Corrixir a marca de aceptar do obxecto de evento en DragMove (fallo 396011)
- Usar unha icona de elemento «Ningún» distinta nos KCM de vista de grade

### KDESU

- kdesud: KAboutData::setupCommandLine() xa define a axuda e a versión

### KDocTools

- Mirar a posibilidade de compilación cruzada a KF5_HOST_TOOLING
- Só informar de que se atopou DocBookXML cando se atopase
- Actualizar as entidades españolas

### KFileMetaData

- [Extractor] Engadir metadatos aos extractores (fallo 404171)
- Engadir un extractor para ficheiros de AppImage
- Limpar o extractor de ffmpeg
- [ExternalExtractor] Fornecer unha saída máis útil cando o extractor falla
- Formatar os datos de flash das fotos de EXIF (fallo 343273)
- Evitar os efectos secundarios de valores obsoletos de número de erro
- Usar KFormat para as taxas de bits e de mostras
- Engadir unidades aos datos de taxa de fotogramas e de GPS
- Engadir unha función de formato de cadea á información das propiedades
- Evitar perder un QObject en ExternalExtractor
- Xestionar &lt;a&gt; como elemento contedor en SVG
- Comprobar Exiv2::ValueType::typeId antes de convertelo en racional

### KImageFormats

- ras: corrixir a quebra nos ficheiros rotos
- ras: protexer tamén o QVector de paleta
- ras: axustar a comprobación de ficheiros máximos
- xcf: corrixir o uso de memoria sen inicializar en documentos rotos
- engadir const, axuda a entender mellor a función
- ras: axustar o tamaño máximo que «cabe» nun QVector
- ras: non aseverar porque intentamos asignar un vector enorme
- ras: protexer dunha división por cero
- xcf: non dividir por 0
- tga: fallar elegantemente ante erros de readRawData
- ras: fallar elegantemente cando altura×anchura×bpp &gt; lonxitude

### KIO

- kioexec: KAboutData::setupCommandLine() xa define a axuda e a versión
- Corrixir a quebra en Dolphin ao soltar un ficheiro do lixo sobre o lixo (fallo 378051)
- Acurtar polo centro os nomes de ficheiro moi longos nas cadeas de erro (fallo 404232)
- Engadir compatibilidade con portais en KRun
- [KPropertiesDialog] Corrixir a caixa de alternativa de grupo (fallo 403074)
- Properly attempt to locate the kioslave bin in $libexec AND $libexec/kf5
- Usar AuthCore en vez de Auth
- Engadir o nome de icona aos fornecedores de servizo no ficheiro .desktop
- Ler a icona de buscador IKWS do ficheiro de escritorio
- [PreviewJob] Tamén indicar que somos o xerador de miniaturas ao obter os metadatos do ficheiro (fallo 234754)

### Kirigami

- retirar a xestión rota de contentY en refreshabeScrollView
- engadir OverlayDrawer ás cousas que Doxygen pode documentar
- asociar currentItem á vista
- cor axeitada para a icona de frecha cara abaixo
- SwipeListItem: facer espazo para as accións cando !supportsMouseEvents (fallo 404755)
- ColumnView e cambios internos parciais de C++ en PageRow
- podemos usar como moito os controis 2.3 por Qt 5.10
- corrixir a altura dos caixóns horizontais
- Mellorar ToolTip no compoñente ActionTextField
- Engadir un compoñente ActionTextField
- corrixir o espazado dos botóns (fallo 404716)
- corrixir o tamaño dos botóns (fallo 404715)
- GlobalDrawerActionItem: facer referencia correctamente á icona usando a propiedade de grupo
- mostrar o separador de a barra de ferramenta da cabeceira é invisíbel
- engadir un fondo de páxina predeterminado
- DelegateRecycler: corrixir que a tradución usase o dominio incorrecto
- Corrixir o aviso ao usar QQuickAction
- Retirar algunhas construcións de QString innecesarias
- Non mostrar a xanela emerxente cando se mostra o menú despregábel (fallo 404371)
- agochar as sombras cando se pecha
- engadir as propiedades necesarias para a cor alternativa
- reverter a meirande parte do cambio das heurísticas de coloración das iconas
- xestionar correctamente as propiedades agrupadas
- [PassiveNotification] Non iniciar o temporizador ata que a xanela teña o foco (fallo 403809)
- [SwipeListItem] Usar un botón de ferramenta real para mellorar a facilidade de uso (fallo 403641)
- permitir fondos opcionais alternantes (bug 395607)
- só mostrar asas cando hai accións visíbeis
- permitir iconas coloradas nos botóns de acción
- mostrar sempre o botón de volver nas capas
- Actualizar o documento de SwipeListItem a QQC2
- corrixir a lóxica de updateVisiblePAges
- expoñer as páxinas visíbeis en pagerow
- agochar o ronsel cando a páxina actual ten unha barra de ferramentas
- permitir sobrepoñer a páxina de toolbarstyle
- noca propiedade na páxina: titleDelegate para sobrepoñer o título nas barras de tarefas

### KItemModels

- KRearrangeColumnsProxyModel: publicar os dous métodos de asociación de columnas

### KNewStuff

- Retirar o contido incorrecto das listas
- Corrixir unha fuga de memoria atopada por asan

### KNotification

- migrar de ECM a findcanberra
- Listar Android como oficialmente compatíbel

### Infraestrutura KPackage

- retirar o aviso de obsolescencia de kpackage_install_package

### KParts

- modelos: KAboutData::setupCommandLine() xa define a axuda e a versión

### Kross

- Install Kross modules to ${KDE_INSTALL_QTPLUGINDIR}

### KService

- kbuildsycoca5: non hai que repetir o traballo de KAboutData::setupCommandLine()

### KTextEditor

- intentar mellorar a altura da debuxo das liñas de texto - fallo 403868 evitar cortar _ e outras partes aínda rotas: duplicar a altura de cousas como mestura de inglés e árabe, consultar o fallo 404713
- Usar QTextFormat::TextUnderlineStyle en vez de QTextFormat::FontUnderline (fallo 399278)
- Permitir mostrar todos os espazos do documento (fallo 342811)
- Non imprimir as liñas de sangrado
- KateSearchBar: mostrar tamén se a busca ten un consello axustado en nextMatchForSelection(), é dicir, Ctrl+H
- katetextbuffer: facer cambios internos en TextBuffer::save() para separar mellor as rutas de código
- Usar AuthCore en vez de Auth
- Facer cambios internos en KateViewInternal::mouseDoubleClickEvent(QMouseEvent *e)
- Melloras do completado
- Definir o esquema de cores como «Impresión» para a vista previa de impresión (fallo 391678)

### KWayland

- Só remitir XdgOutput::done se cambiou (fallo 400987)
- FakeInput: engadir a posibilidade de movemento de punteiro con coordenadas absolutas
- Engadir o XdgShellPopup::ackConfigure que faltaba
- [servidor] Engadir un mecanismo de proxy de datos de superficie
- [servidor] Engadir o sinal selectionChanged

### KWidgetsAddons

- Usar a icona correcta de «no» de KStandardGuiItem

### Infraestrutura de Plasma

- [Elemento de icona] Bloquear a seguinte animación tamén segundo a visibilidade da xanela
- Mostrar un aviso se un complemento require unha versión máis nova
- Aumentar as versións de tema porque as iconas cambiaron, para invalidas as cachés vellas
- [breeze-icons] Reformar system.svgz
- Facer consistentes os textos das xanelas emerxentes do tema Breeze
- Cambiar glowbar.svgz a un estilo máis suave (fallo 391343)
- Facer reserva de contraste de fondo en tempo de execución (fallo 401142)
- [tema de escritorio Breeze/diálogos] Engadir esquinas arredondadas aos diálogos

### Purpose

- pastebin: non mostrar as notificacións de progreso (fallo 404253)
- sharetool: Mostrar o URL compartido na parte superior
- Corrixir compartir ficheiros con espazos ou comiñas en nomes mediante Telegram
- Facer que ShareFileItemAction forneza unha saída ou un erro se se fornecen (fallo 397567)
- Activar a compartición de URL por correo electrónico

### QQC2StyleBridge

- Usar PointingHand ao cubrir ligazóns en Label
- Respectar a propiedade de visualización dos botóns
- premer zonas baleiras compórtase como RePáx e AvPáx (fallo 402578)
- Permitir iconas en ComboBox
- ofrecer unha API de colocación de texto
- Permitir iconas de ficheiros locais nos botóns
- Usar o cursor correcto ao cubrir unha parte editábel dun control numérico

### Solid

- Adaptar FindUDev.cmake aos estándares de ECM

### Sonnet

- Xestionar o caso de que createSpeller reciba un idioma non dispoñíbel

### Realce da sintaxe

- Corrixir o aviso de eliminación do repositorio
- MustacheJS: realzar tamén os ficheiros de modelos, corrixir a sintaxe e mellorar a compatibilidade con Handlebars
- facer os contextos non usados fatais para o indexador
- Actualizar example.rmd.fold e test.markdown.fold con novos números
- Instalar a cabeceira DefinitionDownloader
- Actualizar octave.xml a Octave 4.2.0
- Mellorar o realce de TypeScript (e React) e engadir máis probas para PHP
- Engadir máis realce para linguaxes aniñadas en Markdown
- Devolver as definicións ordenadas de nomes de ficheiro e tipos MIME
- engadir unha actualización de referencia que faltaba
- BrightScript: números unarios e hexadecimais , @atributo
- Evitar ficheiros *-php.xml duplicados en «data/CMakeLists.txt»
- Engadir funcións que devolven todas as definicións dun tipo MIME ou nome de ficheiro
- actualizar o tipo MIME de Haskell de literal
- evitar unha aserción ao cargar a expresión regular
- cmake.xml: actualizacións para a versión 3.14
- CubeScript: corrixe os escapes de continuación de liña nas cadeas
- engadir unhas instrucións básicas para engadir probas
- Markdown de R: mellorar a contracción de bloques
- HTML: realzar código JSX, TypeScript e MustacheJS na etiqueta &lt;script&gt; (fallo 369562)
- AsciiDoc: Engadir contracción de seccións
- Realce de sintaxe de esquemas de FlatBuffers
- Engadir algunhas constantes e funcións de Maxima

### Información de seguranza

The released code has been GPG-signed using the following key: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB
