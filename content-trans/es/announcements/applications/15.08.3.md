---
aliases:
- ../announce-applications-15.08.3
changelog: true
date: 2015-11-10
description: KDE lanza las Aplicaciones de KDE 15.08.3
layout: application
title: KDE lanza las Aplicaciones de KDE 15.08.3
version: 15.08.3
---
Hoy, 10 de noviembre de 2015, KDE ha lanzado la tercera actualización de estabilización para las <a href='../15.08.0'>Aplicaciones 15.08</a>. Esta versión solo contiene correcciones de errores y actualizaciones de traducciones, por lo que será una actualización agradable y segura para todo el mundo.

Entre las más de 20 correcciones de errores registradas, se incluyen mejoras en ark, dolphin, kdenlive, kdepim, kig, lokalize y umbrello.

También se incluye la versión de la Plataforma de desarrollo de KDE 4.14.14 que contará con asistencia a largo plazo.
