---
aliases:
- ../announce-applications-18.08.1
changelog: true
date: 2018-09-06
description: KDE lanza las Aplicaciones de KDE 18.08.1
layout: application
title: KDE lanza las Aplicaciones de KDE 18.08.1
version: 18.08.1
---
Hoy, 6 de septiembre de 2018, KDE ha lanzado la primera actualización de estabilización para las <a href='../18.08.0'>Aplicaciones de KDE 18.08</a>. Esta versión solo contiene correcciones de errores y actualizaciones de traducciones, por lo que será una actualización agradable y segura para todo el mundo.

Entre las más de 12 correcciones de errores registradas, se incluyen mejoras en Kontact, Cantor, Gwenview, Okular y Umbrello, entre otras aplicaciones.

Las mejoras incluyen:

- El componente KIO-MTP ya no falla cuando se está accediendo al dispositivo con otra aplicación distinta.
- El envío de mensajes de KMail usa ahora la contraseña cuando se indique mediante petición de contraseña.
- Okular recuerda ahora el modo de la barra lateral tras guardar documentos PDF.
