---
aliases:
- ../announce-applications-17.08.0
changelog: true
date: 2017-08-17
description: KDE lanza las Aplicaciones de KDE 17.08.0
layout: application
title: KDE lanza las Aplicaciones de KDE 17.08.0
version: 17.08.0
---
17 de agosto de 2017. Las Aplicaciones de KDE 17.08 están aquí. Hemos trabajado para hacer que tanto las aplicaciones como las bibliotecas en las que se apoyan sean más estables y más fáciles de usar. Planchando las arrugas y oyendo los comentarios recibidos, hemos hecho que las Aplicaciones de KDE sean menos propensas a sufrir fallos técnicos y mucho más amigables de usar. ¡Disfrute de las nuevas aplicaciones!

### Más migraciones a KDE Frameworks 5

Nos complace que las siguientes aplicaciones que estaban basadas en kdelibs4 estén ya basadas en KDE Frameworks 5: kmag, kmousetool, kgoldrunner, kigo, konquest, kreversi, ksnakeduel, kspaceduel, ksudoku, kubrick, lskat y umbrello. Gracias al esfuerzo de los desarrolladores que han dedicado tiempo y trabajo de forma voluntaria para hacerlo posible.

### Novedades de las Aplicaciones de KDE 17.08

#### Dolphin

Los desarrolladores de Dolphin comunican que Dolphin ya muestra la «Hora de borrado» en la papelera y que se muestra la «Hora de creación» si el sistema operativo lo permite en BSD.

#### KIO-Extras

Kio-Extras proporcionan ahora una mejor implementación de los recursos compartidos de Samba.

#### KAlgebra

Los desarrolladores de KAlgebra han trabajado en la mejora de la interfaz de Kirigami para el escritorio y han implementado la terminación automática de código.

#### Kontact

- En KMailtransport, los desarrolladores han reactivado el uso del transporte de Akonadi, han creado complementos para ello y han vuelto a crear la implementación de transporte de correo de «sendmail».
- En SieveEditor se han corregido y cerrado multitud de errores en los guiones de creación automática. Junto a esta corrección general de errores, se ha añadido un editor de líneas de expresiones regulares.
- En KMail se ha vuelto a crear como complemento la posibilidad de usar un editor externo.
- El asistente de importación de Akonadi usa ahora el conversor «convertir todo» como complemento, por lo que los desarrolladores pueden crear nuevos conversores de forma sencilla.
- Las aplicaciones dependen ahora de Qt 5.7. Los desarrolladores han corregido numerosos errores de compilación en Windows. Todavía no se puede compilar todo kdepim en Windows, aunque los desarrolladores han realizado un gran avance. Para empezar, han creado una receta para ello. Gran parte de las correcciones de errores se han dedicado a modernizar el código fuente (C++11). Uso de Wayland en Qt 5.9. La biblioteca en tiempo de ejecución de Kdepim añade un recurso para Facebook.

#### Kdenlive

En Kdenlive, el equipo ha corregido el «efecto de congelación», que no funcionaba correctamente. En versiones recientes, era imposible cambiar el fotograma congelado por el efecto congelado. Ahora se permite un acceso rápido de teclado para la función de extracción de fotograma. Ahora se pueden guardar capturas de pantalla de la línea de tiempo con un acceso rápido de teclado, para las que se sugiere un nombre en función del número de fotograma <a href='https://bugs.kde.org/show_bug.cgi?id=381325'>https://bugs.kde.org/show_bug.cgi?id=381325</a>. Se han corregido las lumas de transición descargadas que no aparecen en la interfaz: <a href='https://bugs.kde.org/show_bug.cgi?id=382451'>https://bugs.kde.org/show_bug.cgi?id=382451</a>. Se ha corregido un problema de clics de sonido (por ahora, se necesita compilar la dependencia de MLT desde git hasta que se publique MLT): <a href='https://bugs.kde.org/show_bug.cgi?id=371849'>https://bugs.kde.org/show_bug.cgi?id=371849</a>.

#### Krfb

Los desarrolladores han terminado de migrar el complemento X11 a Qt5, y krfb vuelve a funcionar de nuevo usando un motor de X11 que es mucho más rápido que el complemento de Qt. Hay una nueva página de configuración, que permite al usuario cambiar el complemento de «framebuffer» preferido.

#### Konsole

Konsole permite ahora un desplazamiento hacia atrás ilimitado para superar el límite de 2 GB (32 bits). Ahora Konsole permite al usuario introducir cualquier ubicación para almacenar archivos de contenido anterior de la pantalla. Además, se ha corregido una regresión: Konsole vuelve a permitir que KonsolePart llame al diálogo de gestión del perfil.

#### KAppTemplate

En KAppTemplate existe ahora una opción para instalar nuevas plantillas desde el sistema de archivos. Se han eliminado más plantillas de KAppTemplate que se han integrado en productos relacionados. Las plantillas del complemento «ktexteditor» y de «kpartsapp» (migradas ahora a Qt5/KF5) se han convertido en parte del framework de KDE KTextEditor y de KParts desde la versión 5.37.0. Estos cambios deberían simplificar la creación de plantillas en las aplicaciones de KDE.

### A la caza de errores

Se han resuelto más de 80 errores en aplicaciones, que incluyen la suite Kontact, Ark, Dolphin, K3b, Kdenlive, KGpg y Konsole, entre otras.

### Registro de cambios completo
