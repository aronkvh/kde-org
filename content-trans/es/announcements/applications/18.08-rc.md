---
aliases:
- ../announce-applications-18.08-rc
date: 2018-08-03
description: KDE lanza la versión candidata de las Aplicaciones 18.08.
layout: application
release: applications-18.07.90
title: KDE publica la versión candidata de las Aplicaciones de KDE 18.08
---
3 de agosto de 2018. KDE ha publicado hoy la versión candidata de las nuevas Aplicaciones de KDE. Una vez congeladas las dependencias y las funcionalidades, el equipo de KDE se ha centrado en corregir errores y en pulir aún más la versión.

Consulte las <a href='https://community.kde.org/Applications/18.08_Release_Notes'>notas de lanzamiento de la comunidad</a> para obtener información sobre los paquetes y sobre los problemas conocidos. Se realizará un anuncio más completo para la versión final.

Los lanzamientos de las Aplicaciones de KDE 18.08 necesitan una prueba exhaustiva con el fin de mantener y mejorar la calidad y la experiencia del usuario. Los usuarios reales son críticos para mantener la alta calidad de KDE porque los desarrolladores no pueden probar todas las configuraciones posibles. Contamos con ustedes para ayudar a encontrar errores de manera temprana de forma que se puedan corregir antes de la versión final. Considere la idea de unirse al equipo instalando la versión candidata <a href='https://bugs.kde.org/'>e informando de cualquier error que encuentre</a>.
