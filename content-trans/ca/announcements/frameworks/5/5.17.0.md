---
aliases:
- ../../kde-frameworks-5.17.0
date: 2015-12-12
layout: framework
libCount: 60
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Baloo

- Esmenar el filtre de data usat per timeline://
- Balooctl: Retorn després de les ordres
- Neteja i protecció de Baloo::Database::open(), gestionar més condicions de fallada
- Afegir comprovacions a Database::open(OpenDatabase) per fallar si la base de dades no existeix

### Icones Brisa

- Diverses icones afegides o millorades
- Usar fulls d'estil en les icones del Brisa (error 126166)
- Error: 355902 Esmenar i canviar el sistema de bloqueig de pantalla
- Afegir un diàleg d'informació de 24px per les aplicacions GTK (error 355204)

### Mòduls extres del CMake

- No avisar quan les icones SVG(Z) es proporcionen amb diverses mides/nivells de detall
- Assegura que es carreguen les traduccions en el fil principal (error 346188)
- Redissenyar el sistema de construcció ECM.
- Possibilitar l'activació del Clazy en qualsevol projecte KDE
- Per defecte, no cercar la biblioteca XINPUT del XCB.
- Netejar el directori d'exportació abans de tornar a generar una APK
- Usar «quickgit» com a URL del repositori Git.

### Integració del marc de treball

- Afegir un plasmoide de fallada d'instal·lació al plasma_workspace.notifyrc

### KActivities

- Esmenar un bloqueig en el primer inici del dimoni
- Moure la creació de QAction al fil principal (error 351485)
- A vegades el format «clang» pren alguna decisió dolenta (error 355495)
- Eliminació de problemes potencials de sincronització
- Usar org.qtproject en lloc de com.trolltech
- Eliminar l'ús de «libkactivities» des dels connectors
- Eliminar la configuració de KAStats de l'API
- Afegir l'enllaçat i el desenllaçat a ResultModel

### Eines de Doxygen del KDE

- Fer més robust «kgenframeworksapidox».

### KArchive

- Esmenar KCompressionDevice::seek(), cridat en crear un KTar a sobre d'un KCompressionDevice.

### KCoreAddons

- KAboutData: Permetre https:// i altres esquemes d'URL en la pàgina principal (error 355508)
- Reparar la propietat MimeType en usar kcoreaddons_desktop_to_json()

### KDeclarative

- Adaptar KDeclarative per a usar directament KI18n
- «delegateImage» de «DragArea» ara pot ser una cadena per la qual es crea automàticament una icona
- Afegir una biblioteca CalendarEvents nova

### KDED

- Eliminar la variable d'entorn SESSION_MANAGER en lloc de deixar-la buida

### Compatibilitat amb les KDELibs 4

- Esmenar diverses crides i18n.

### KFileMetaData

- Marcar els «m4a» com a llegibles per la «taglib»

### KIO

- Diàleg de galeta: fer que funcioni com està destinat
- Esmenar el suggeriment de nom de fitxer a quelcom aleatori en canviar el tipus MIME en el «desa com a».
- Registrar el nom D-Bus per al kioexec (error 353037)
- Actualitzar el KProtocolManager després d'un canvi de configuració.

### KItemModels

- Esmenar l'ús de KSelectionProxyModel a la QTableView (error 352369)
- Esmenar el restabliment o el canvi del model origen d'un KRecursiveFilterProxyModel.

### KNewStuff

- «registerServicesByGroupingNames» pot definir més elements per defecte
- Fer menys estricte KMoreToolsMenuFactory::createMenuFromGroupingNames

### KTextEditor

- Afegir ressaltat de sintaxi per a TaskJuggler i PL/I
- Possibilitar la desactivació de compleció de paraula clau via la interfície de configuració.
- Redimensionar l'arbre quan el model de compleció es reinicia.

### Framework del KWallet

- Gestionar correctament el cas quan l'usuari ens desactiva

### KWidgetsAddons

- Esmenar un defecte petit del KRatingWidget en ppp alt.
- Refactoritzar i esmenar la funcionalitat introduïda a l'error 171343

### KXMLGUI

- No cridar QCoreApplication::setQuitLockEnabled(true) en iniciar.

### Frameworks del Plasma

- Afegir un plasmoide bàsic com a exemple per a la guia del desenvolupador
- Afegir un parell de plantilles de plasmoide per a kapptemplate/kdevelop
- [calendari] Retardar el reinici del model fins que la vista estigui preparada (error 355943)
- No reposicionar mentre està ocult (error 354352)
- [IconItem] No fallar en un tema del KIconLoader nul (error 355577)
- En deixar anar fitxers d'imatge en un plafó ja no s'oferirà definir-los com a fons de pantalla per al plafó
- En deixar anar un fitxer .plasmoid en un plafó o l'escriptori, s'instal·larà i s'afegirà
- Eliminar el mòdul kded «platformstatus» que ja no s'usa (error 348840)
- Permetre enganxar en camps de contrasenya
- Esmenar el posicionament del menú d'edició, afegir un botó per a seleccionar
- [calendari] Usar l'idioma de la IU per obtenir el nom del mes (error 353715)
- [calendari] Ordenar els esdeveniments també pel seu tipus
- [calendari] Moure la biblioteca de connectors a KDeclarative
- [calendari] «qmlRegisterUncreatableType» necessita més arguments
- Permetre afegir dinàmicament categories de configuració
- [calendari] Moure la gestió de connectors a una classe separada
- Permetre als connectors que subministrin esdeveniments de dates a la miniaplicació de Calendari (error 349676)
- Verificar l'existència d'un sòcol abans de connectar o desconnectar (error 354751)
- [plasmaquick] No enllaçar l'OpenGL explícitament
- [plasmaquick] Eliminar les dependències XCB::COMPOSITE i DAMAGE

Podeu debatre i compartir idees quant a aquest llançament en la secció de comentaris en <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>l'article del Dot</a>.
