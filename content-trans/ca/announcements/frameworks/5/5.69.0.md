---
aliases:
- ../../kde-frameworks-5.69.0
date: 2020-04-05
layout: framework
libCount: 70
---
### Baloo

- [SearchStore] Usa un enregistrament categoritzat
- [QueryParser] Corregeix una detecció trencada de cometes finals
- [EngineQuery] Proporciona la sobrecàrrega de «toString(Term)» per a QTest
- [EngineQuery] Elimina un membre de posició no usat, amplia les proves
- [SearchStore] Evita les línies llargues i la imbricació de funcions
- [baloosearch] Surt molt aviat si la carpeta especificada no és vàlida
- [MTimeDB] Consolida el codi de gestió de l'interval de temps
- [AdvancedQueryParser] Prova si les frases citades es passen correctament
- [Term] Proporciona la sobrecàrrega de «toString(Term)» per a QTest
- [ResultIterator] Elimina una declaració «forward» sense ús de SearchStore
- [QueryTest] Fa control per dades i amplia el cas de prova de les frases
- [Inotify] Inicia el temporitzador de venciment per a «MoveFrom» com a màxim una vegada per lot d'«inotify»
- [UnindexedFileIndexer] Marca el fitxer per indexar el contingut només quan sigui necessari
- [Inotify] Només crida QFile::decode des d'un únic lloc
- [QML] Controla correctament «unregistration»
- [FileIndexScheduler] Actualitza més sovint el progrés d'indexació del contingut
- [FileIndexerConfig] Substitueix el parell «QString,bool» de configuració per una classe dedicada
- [QML] Estableix de manera més fiable el temps romanent al monitor
- [TimeEstimator] Corregeix la mida del lot, elimina la referència de la configuració
- [FileIndexScheduler] Emet un canvi a l'estat LowPowerIdle
- [Debug] Millora la intel·ligibilitat del format de depuració «positioninfo»
- [Debug] Corregeix la sortida de *::toTestMap(), silencia el no error
- [WriteTransactionTest] Prova l'eliminació de només les posicions
- [WriteTransaction] Amplia el cas de prova de les posicions
- [WriteTransaction] Evita el creixement de «m_pendingOperations» per duplicat en substituir
- [FileIndexScheduler] Neteja la gestió de «firstRun»
- [StorageDevices] Corregeix l'ordre de les notificacions de connexió i inicialització
- [Config] Elimina/fa obsolet «disableInitialUpdate»

### Icones Brisa

- Corregeix els enllaços simbòlics trencats
- Mou el plegat de la cantonada a la part superior dreta a 24 icones
- Fa que «find-location» mostri una lupa a un mapa, per a diferenciar de «mark-location» (error 407061)
- Afegeix les icones del LibreOffice de 16px
- Corregeix la configuració quan «xmllint» no és present
- Corregeix l'enllaçat del full d'estil a 8 icones
- Corregeix els colors de diversos fulls d'estil a 3 fitxers d'icones
- Corregeix els enllaços simbòlics a les mides d'icona incorrectes
- Afegeix «input-dialpad» i «call-voicemail»
- Afegeix la icona «buho»
- Afegeix la icona «calindori» a l'estil «pm» nou
- Afegeix la icona «nota»
- [breeze-icons] Corregeix l'ombra a diverses icones d'usuari (applets/128)
- Afegeix «call-incoming/missed/outgoing»
- Gestiona el «sed» de BusyBox com el «sed» de GNU
- Afegeix «transmission-tray-icon»
- Millora l'alineació a píxel i els marges de les icones de la safata del sistema del «keepassxc»
- Reverteix «[breeze-icons] Afegeix les icones «telegram-desktop» de la safata»
- Afegeix icones petites per al KeePassXC
- [breeze-icons] Afegeix icones de la safata del TeamViewer
- Afegeix «edit-reset»
- Canvia l'estil de «document-revert» perquè sigui més semblant a «edit-undo»
- Icones per a les categories dels «emoji»
- Afegeix les icones de la safata del «flameshot»

### KAuth

- Corregeix el requisit de tipus d'espai de nom

### KBookmarks

- Desacobla KBookmarksMenu de KActionCollection

### KCalendarCore

- Corregeix la càrrega de l'alternativa de reserva vCalendar en fallar la càrrega de l'iCalendar

### KCMUtils

- Escolta a «passiveNotificationRequested»
- Solució temporal per fer que «applicationitem» mai es redimensioni a si mateix

### KConfig

- [KConfigGui] Comprova l'estil del tipus de lletra en netejar la propietat «styleName»
- KconfigXT: afegeix un atribut de valor als camps «choices» de les enumeracions

### KCoreAddons

- kdirwatch: corregeix una fallada introduïda recentment (error 419428)
- KPluginMetaData: gestiona els tipus MIME no vàlids a «supportsMimeType»

### KCrash

- Mou la definició de «setErrorMessage» fora de l'«ifdef» del Linux
- Permet proporcionar un missatge d'error des de l'aplicació (error 375913)

### KDBusAddons

- Comprova si és el fitxer correcte per a la detecció de l'entorn protegit

### KDeclarative

- Presenta una API per a notificacions passives
- [KCM Controls GridDelegate] Usa <code>ShadowedRectangle</code>
- [kcmcontrols] Respecta la visibilitat de la capçalera/peu de pàgina

### KDocTools

- Usa negreta cursiva al 100%f per als títols «sect4», i negreta al 100%f per als títols «sect5» (error 419256)
- Actualitza la llista d'entitats italianes
- Usa el mateix estil a «informaltable» que per a «table» (error 418696)

### KIdleTime

- Corregeix una funció recursiva infinita al connector de l'«xscreensaver»

### KImageFormats

- Adapta el connector HDR de «sscanf()» a QRegularExpression. Corregeix el FreeBSD

### KIO

- Classe nova KIO::CommandLauncherJob al KIOGui per substituir KRun::runCommand
- Classe nova KIO::ApplicationLauncherJob al KIOGui per substituir KRun::run
- File ioslave: usa una definició millor a la crida de sistema «sendfile» (error 402276)
- FileWidgets: ignora els esdeveniments de retorn del KDirOperator (error 412737)
- [DirectorySizeJob] Corregeix el comptador de subdirectoris en resoldre enllaços simbòlics a directoris
- Marca els muntatges de KIOFuse com a «Probably slow»
- kio_file: respecta KIO::StatResolveSymlink per a UDS_DEVICE_ID i UDS_INODE
- [KNewFileMenu] Afegeix una extensió al nom de fitxer proposat (error 61669)
- [KOpenWithDialog] Afegeix un nom genèric a partir dels fitxers «.desktop» com un consell d'eina (error 109016)
- KDirModel: implementa la visualització d'un node arrel per a l'URL sol·licitat
- Registra les aplicacions generades a un «cgroups» independent
- Afegeix el prefix «Stat» a les entrades de l'enumeració StatDetails
- Windows: afegeix la implementació per a la data de creació del fitxer
- KAbstractFileItemActionPlugin: afegeix les cometes que manquen al codi d'exemple
- Evita la recuperació doble i la codificació hex temporal per als atributs NTFS
- KMountPoint: omet la memòria d'intercanvi
- Assigna una icona als submenús d'acció
- [DesktopExecParser] Obre els URL {ssh,telnet,rlogin}:// amb el «ktelnetservice» (error 418258)
- Corregeix el codi de sortida del «kioexec» quan no existeix l'executable (i s'ha definit --tempfiles)
- [KPasswdServer] Substitueix un «foreach» per un «for» basat en «range/index»
- KRun's KProcessRunner: finalitza la notificació d'inici també en error
- [http_cache_cleaner] Substitueix l'ús de «foreach» per QDir::removeRecursively()
- [StatJob] Usa una QFlag per a especificar els detalls retornats per StatJob

### Kirigami

- Revisió per a la D28468 per corregir referències a variables trencades
- Desfer-se de l'incubador
- Desactiva completament la roda del ratolí fora «flickable»
- Afegeix la implementació de l'inicialitzador de propietats al PagePool
- Refactoritza OverlaySheet
- Afegeix els elements ShadowedImage i ShadowedTexture
- [controls/formlayout] No intenta reiniciar «implicitWidth»
- Afegeix consells útils dels mètodes d'entrada al camp de contrasenya de manera predeterminada
- [FormLayout] Estableix l'interval del temporitzador de compressió a 0
- [UrlButton] Desactiva quan no hi ha cap URL
- Simplifica la redimensió de la capçalera (error 419124)
- Elimina la capçalera d'exportació de la instal·lació estàtica
- Corregeix la pàgina «quant a» amb les Qt 5.15
- Corregeix els camins trencats a «kirigami.qrc.in»
- Afegeix la durada d'animació «veryLongDuration»
- Corregeix les notificacions multifila
- No depèn de la finestra activa per al temporitzador
- Implementa les notificacions passives múltiples apilades
- Corregeix l'activació de la vora per al ShadowedRectangle a la creació de l'element
- Comprova l'existència de la finestra
- Afegeix els tipus que manquen al «qrc»
- Corregeix una verificació no definida al mode de menú de calaix global (error 417956)
- Un rectangle senzill com a alternativa de reserva en usar la renderització per programari
- Corregeix el «premultiply» del color i la barreja alfa
- [FormLayout] Propaga FormData.enabled també a l'etiqueta
- Afegeix un element ShadowedRectangle
- Propietat «alwaysVisibleActions»
- No crea instàncies quan l'aplicació està sortint
- No emet canvis de paleta si la paleta no ha canviat

### KItemModels

- [KSortFilterProxyModel QML] Fa públic «invalidateFilter»

### KNewStuff

- Corregeix la disposició a DownloadItemsSheet (error 419535)
- [QtQuick dialog] S'ha adaptat per usar UrlButton i l'oculta quan no hi ha cap URL
- Commuta per usar ShadowedRectangle del Kirigami
- Corregeix l'actualització dels escenaris sense un «downloadlink» explícitament seleccionat (error 417510)

### KNotification

- Classe nova KNotificationJobUiDelegate

### KNotifyConfig

- Usa la «libcanberra» com a mètode primari de previsualitzar el so (error 418975)

### KParts

- Classe nova PartLoader com a substitució de KMimeTypeTrader per a les parts

### KService

- KAutostart: afegeix un mètode estàtic per comprovar la condició d'inici
- KServiceAction: emmagatzema el servei pare
- Llegeix adequadament la llista de cadenes X-Flatpak-RenamedFrom dels fitxers «desktop»

### KTextEditor

- Fa que compili amb les Qt 5.15
- Corregeix una fallada del plegat en plegar plecs d'una línia (error 417890)
- [VIM Mode] Afegeix les ordres g&lt;up&gt; g&lt;down&gt; (error 418486)
- Afegeix MarkInterfaceV2, a s/QPixmap/QIcon/g per als símbols de marques
- Dibuixa «inlineNotes» després de dibuixar el marcador d'ajust de paraules

### KWayland

- [xdgoutput] Només envia el nom inicial i la descripció si s'ha establert
- Afegeix XdgOutputV1 versió 2
- Difusió del menú d'aplicació als recursos quan es registrin
- Proporciona una implementació per a la interfície de tauleta
- [server] No fa suposicions quant a l'ordre de les sol·licituds de «damage_buffer» i «attach»
- Passa un «fd» dedicat a cada teclat per al mapa de teclat del «xkb» (error 381674)
- [server] Presenta SurfaceInterface::boundingRect()

### KWidgetsAddons

- Classe nova KFontChooserDialog (basada en el KFontDialog de KDELibs4Support)
- [KCharSelect] No simplifica els caràcters únics a la cerca (error 418461)
- Si es llegeixen elements primer cal netejar-ho. Altrament es veuran duplicats a la llista
- Actualitza les «kcharselect-data» a Unicode 13.0

### KWindowSystem

- Corregeix l'EWMH que no compleix amb NET::{OnScreenDisplay,CriticalNotification}
- KWindowSystem: fa obsolet KStartupInfoData::launchedBy, sense ús
- Exposa el menú d'aplicació via KWindowInfo

### Frameworks del Plasma

- S'ha afegit un element Page
- [pc3/busyindicator] S'oculta quan no està executant-se
- Actualitza «window-pin», afegeix més mides, elimina «edit-delete» redundant
- Crea un element nou TopArea usant els «svg» «widgets/toparea»
- Afegeix un «svg» de capçalera de plasmoide
- Fa que la propietat ressaltada funcioni per als «roundbutton»

### Prison

- Exposa la mida mínima real també al QML
- Afegeix un conjunt nou de funcions de mida de codis de barres
- Simplifica la gestió de la mida mínima
- Mou la lògica d'escalat de la imatge de codi de barres a AbstractBarcode
- Afegeix una API per comprovar si un codi de barres és d'una o dues dimensions

### QQC2StyleBridge

- [Dialog] Usa <code>ShadowedRectangle</code>
- Corregeix la mida de CheckBox i RadioButton (error 418447)
- Usa <code>ShadowedRectangle</code>

### Solid

- [Fstab] Assegura la unicitat per a tots els tipus de sistema de fitxers
- Samba: assegura que es diferencien els muntatges que comparteixen el mateix origen (error 418906)
- Hardware tool: defineix la sintaxi via arguments de sintaxi

### Sonnet

- Corregeix la detecció automàtica del Sonnet que fallava per als idiomes de la India
- Crea ConfigView, un ConfigWidget sense gestió

### Ressaltat de la sintaxi

- LaTeX: corregeix els parèntesis matemàtics a les etiquetes opcionals (error 418979)
- Afegeix la sintaxi de l'Inno Setup, incloent-hi la creació de scripts incrustats del Pascal
- Lua: afegeix # com a deliminator addicional per activar la compleció automàtica amb <code>#quelcom</code>
- C: elimina ' com a separador de dígits
- Afegeix un comentari quant al tema de «skipOffset»
- Optimitza la coincidència de les expressions regulars dinàmiques (error 418778)
- Corregeix les regles de les expressions regulars marcades erròniament com a dinàmiques
- Amplia l'indexador per detectar «dynamic=true regexes» que no tenen variables de substitució per adaptar
- Afegeix el ressaltat de l'Overpass QL
- Agda: paraules clau actualitzades a la 2.6.0 i correcció de les comes flotants

### Informació de seguretat

El codi publicat s'ha signat amb GPG usant la clau següent: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Empremta digital de la clau primària: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB
