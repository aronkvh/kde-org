---
aliases:
- ../../kde-frameworks-5.65.0
date: 2019-12-14
layout: framework
libCount: 70
qtversion: 5.12
---
Mòdul nou: KQuickCharts -- un mòdul del QtQuick que proporciona diagrames d'alt rendiment.

### Icones Brisa

- Alinea a píxel el selector de color
- Afegeix icones noves del Baloo
- Afegeix icones noves de preferències de cerca
- Usa un comptagotes per a les icones de selector de color (error 403924)
- Afegeix la icona de categoria «all applications»

### Mòduls extres del CMake

- Neteja del transport «extra-cmake-modules» de l'EBN
- ECMGenerateExportHeader: afegeix l'indicador NO_BUILD_SET_DEPRECATED_WARNINGS_SINCE
- Ús explícit de «lib» per als directoris del Systemd
- Afegeix un directori d'instal·lació per a les unitats del Systemd
- KDEFrameworkCompilerSettings: activa tots els avisos d'obsolescència de les Qt i els KF

### Integració del marc de treball

- Defineix condicionalment SH_ScrollBar_LeftClickAbsolutePosition en funció de l'opció de «kdeglobals» (error 379498)
- Defineix el nom de l'aplicació i la versió a l'eina «knshandler»

### Eines de Doxygen del KDE

- Corregeix les importacions de mòdul amb el Python3

### KAuth

- Instal·la el fitxer «.pri» per al KAuthCore

### KBookmarks

- Fa obsolet KonqBookmarkMenu i KonqBookmarkContextMenu
- Mou les classes només usades pel «KonqBookmarkMenu», cap a ell

### KCalendarCore

- Usa com a alternativa de reserva la zona horària del sistema en crear un calendari quan n'hi ha una de no vàlida
- Memory Calendar: evita la duplicació de codi
- Usa QDate com a clau a «mIncidencesForDate» del MemoryCalendar
- Gestiona les incidències en zones horàries diferents al MemoryCalendar

### KCMUtils

- [KCMultiDialog] Elimina la gestió més especial dels marges; ara es fa al KPageDialog
- KPluginSelector: usa el KAboutPluginDialog nou
- Afegeix una protecció quan manca el Kirigami (error 405023)
- Desactiva el botó per restaurar els valors predeterminats si el KCModule ho diu
- Fa que el «KCModuleProxy» tingui cura de l'estat predeterminat
- Fa que el «KCModuleQml» estigui conforme amb el senyal «defaulted()»

### KCompletion

- Refactoritza KHistoryComboBox::insertItems

### KConfig

- Documenta l'opció Notifiers
- Només crea una sessió de configuració quan realment es restaura una sessió
- kwriteconfig: afegeix l'opció de suprimir
- Afegeix KPropertySkeletonItem
- Prepara el KConfigSkeletonItem per permetre heretar la seva classe privada

### KConfigWidgets

- [KColorScheme] Fa que l'ordre dels colors de decoració coincideixi amb l'enumeració del DecorationRole
- [KColorScheme] Corregeix un error al comentari de NShadeRoles
- [KColorScheme/KStatefulBrush] Canvia els nombres al codi font per elements enumerats
- [KColorScheme] Afegeix elements a les enumeracions de ColorSet i Role per al nombre total d'elements
- Registra KKeySequenceWidget al KConfigDialogManager
- Ajusta el «KCModule» amb la informació de canal també quant als valors predeterminats

### KCoreAddons

- Fa obsolet KAboutData::fromPluginMetaData, ara hi ha KAboutPluginDialog
- Afegeix un avís descriptiu quan «inotify_add_watch» retorna ENOSPC (error 387663)
- Afegeix una prova per a l'error «bug-414360» que no és un error del «ktexttohtml» (error 414360)

### KDBusAddons

- Inclou l'API per implementar genèricament els arguments «--replace»

### KDeclarative

- Neteja del protocol de transferència del «kdeclarative» de l'EBN
- Adapta per canviar el KConfigCompiler
- Fa visibles la capçalera i el peu quan tinguin contingut
- Admet «qqmlfileselectors»
- Permet desactivar el comportament del desament automàtic a ConfigPropertyMap

### KDED

- Elimina la dependència de «kdeinit» del «kded»

### Compatibilitat amb les KDELibs 4

- Elimina «kgesturemap» sense ús de «kaction»

### KDocTools

- Treballs al català: afegeix les entitats que manquen

### KI18n

- Desfà l'obsolescència d'I18N_NOOP2

### KIconThemes

- Fa obsolet el mètode UserIcon de nivell superior, ja no s'empra

### KIO

- Afegeix un protocol nou per als arxius 7z
- [CopyJob] En enllaçar, també considera el «https» per a la icona «text-html»
- [KFileWidget] Evita cridar «slotOk» just després de canviar l'URL (error 412737)
- [KFileWidget] Carrega les icones pel nom
- KRun: no sobreescriu l'aplicació preferida per l'usuari en obrir fitxers *.*html i co. locals (error 399020)
- Repara les consultes FTP/HTTP al servidor intermediari per al cas que no n'hi hagi
- Ftp ioslave: corregeix el passi del paràmetre ProxyUrls
- [KPropertiesDialog] Proporciona una manera de mostrar el destí d'un enllaç simbòlic (error 413002)
- [Remote ioslave] Afegeix Display Name a «remote:/» (error 414345)
- Corregeix la configuració del servidor intermediari HTTP (error 414346)
- [KDirOperator] Afegeix una drecera Retrocés per l'acció enrere
- Quan el «kioslave5» no s'ha pogut trobar a les ubicacions relatives a «libexec» intenta $PATH
- [Samba] Millora el missatge d'avís quant al nom «netbios»
- [DeleteJob] Usa un fil de procés de treball separat per executar l'operació d'E/S real (error 390748)
- [KPropertiesDialog] Fa que la cadena de data de creació també sigui seleccionable amb el ratolí (error 413902)

Són obsolets:

- Fa obsoletes les classes KTcpSocket i KSsl*
- Elimina els darrers rastres de KSslError a TCPSlaveBase
- Adaptacions de les metadades de «ssl_cert_errors» des del KSslError al QSslError
- Fa obsoleta la sobrecàrrega de KTcpSocket del «ctor» KSslErrorUiData
- [http kio slave] Usa QSslSocket en lloc de KTcpSocket (obsolet)
- [TcpSlaveBase] Adaptació des de KTcpSocket (obsolet) a QSslSocket

### Kirigami

- Corregeix els marges de ToolBarHeader
- No falla quan l'origen de la icona està buit
- MenuIcon: corregeix els avisos quan el calaix no està inicialitzat
- Compte per a una etiqueta mnemònica per tornar enrere a «»
- Corregeix les accions InlineMessage situant-les sempre al menú de desbordament
- Corregeix el fons predeterminat de la targeta (error 414329)
- Icon: soluciona un problema amb el fil que l'origen és HTTP
- Correccions a la navegació amb el teclat
- i18n: també extreu els missatges des del codi font en C++
- Corregeix la posició de l'ordre del projecte CMake
- Fa una instància per cada motor a QmlComponentsPool (error 414003)
- Canvia ToolBarPageHeader per usar el comportament de reducció de les icones des d'ActionToolBar
- ActionToolBar: canvia automàticament a només-icona per a les accions marcades KeepVisible
- Afegeix una propietat «displayHint» a Action
- Afegeix la interfície de D-Bus a la versió estàtica
- Reverteix «Té en compte la velocitat d'arrossegament quan acaba un lliscament»
- FormLayout: corregeix l'alçada de l'etiqueta si el mode ample és fals
- No mostra la maneta de manera predeterminada quan no és modal
- Reverteix «Assegura que el «topContent» del GlobalDrawer sempre està a la part superior»
- Usa una RowLayout per disposar la ToolBarPageHeader
- Centra verticalment les accions a l'esquerra a ActionTextField (error 413769)
- Control dinàmic «irestore» del mode tauleta
- Substitueix SwipeListItem
- Permet la propietat «actionsVisible»
- Comença l'adaptació de SwipeListItem a SwipeDelegate

### KItemModels

- Fa obsolet KRecursiveFilterProxyModel
- KNumberModel: gestiona adequadament una «stepSize» de 0
- Exposa KNumberModel al QML
- Afegeix la classe nova KNumberModel que és un model de nombres entre dos valors
- Exposa KDescendantsProxyModel al QML
- Afegeix la importació del QML per a KItemModels

### KNewStuff

- Afegeix diversos enllaços amistosos a «informeu els errors aquí»
- Corregeix la sintaxi «i18n» per evitar errors en temps d'execució (error 414498)
- Converteix el KNewStuffQuick::CommentsModel en un SortFilterProxy per a revisions
- Defineix correctament els arguments d'«i18n» en un pas (error 414060)
- Aquestes funcions són des de la 5.65, no la 5.64
- Afegeix OBS als enregistradors de pantalla (error 412320)
- Corregeix un parell d'enllaços trencats, actualitza els enllaços a https://kde.org/applications/
- Corregeix les traduccions de $GenericName
- Mostra un indicador d'ocupat «S'està carregant més...» en carregar les dades de la vista
- Dona més retroacció a NewStuff::Page mentre s'està carregant l'Engine (error 413439)
- Afegeix un component de disposició per a la retroacció d'activitat de l'element (error 413441)
- Només mostra DownloadItemsSheet si hi ha més d'un element de baixada (error 413437)
- Usa el cursor de mà apuntant per als delegats de clic simple (error 413435)
- Corregeix les disposicions de la capçalera per als components EntryDetails i Page (error 413440)

### KNotification

- Fa que els documents reflecteixin que es prefereixi «setIconName» per sobre de «setPixmap» quan sigui possible
- Documenta el camí del fitxer de configuració a l'Android

### KParts

- Marca adequadament BrowserRun::simpleSave com a obsolet
- BrowserOpenOrSaveQuestion: mou l'enumeració d'AskEmbedOrSaveFlags des de BrowserRun

### KPeople

- Permet activar l'ordenació des del QML

### KQuickCharts

Mòdul nou. El mòdul Quick Charts proporciona un conjunt de diagrames que es poden usar des de les aplicacions QtQuick. Està pensat per a la visualització de dades senzilles, així com la visualització contínua de dades de gran volum (sovint anomenats traçadors). Els diagrames usen un sistema anomenat camps de distància per a la seva renderització accelerada, que proporciona formes d'usar la GPU per a renderitzar formes en 2D sense pèrdua de qualitat.

### KTextEditor

- KateModeManager::updateFileType(): modes de validació i recàrrega del menú de la barra d'estat
- Verifica els modes del fitxer de configuració de la sessió
- LGPLv2+ després de l'acord del Svyatoslav Kuzmich
- Restaura el preformat dels fitxers

### KTextWidgets

- Fa obsolet «kregexpeditorinterface»
- [kfinddialog] Elimina l'ús del sistema de connectors del «kregexpeditor»

### KWayland

- [server] No s'apropia de la implementació del «dmabuf»
- [server] Fa les propietats amb memòria intermèdia doble al «xdg-shell» «double-buffered»

### KWidgetsAddons

- [KSqueezedTextLabel] Afegeix una icona per a l'acció «Copia el text sencer»
- Unifica la gestió del marge del KPageDialog al mateix KPageDialog (error 413181)

### KWindowSystem

- Ajusta el comptador després de l'addició de _GTK_FRAME_EXTENTS
- Afegeix la implementació per _GTK_FRAME_EXTENTS

### KXMLGUI

- Elimina la implementació del KGesture trencat sense ús
- Afegeix KAboutPluginDialog, per a ser usat amb KPluginMetaData
- També permet invocar la lògica de la restauració de sessió quan les aplicacions es llancen manualment (error 413564)
- Afegeix una propietat que manca a KKeySequenceWidget

### Icones de l'Oxygen

- Enllaç simbòlic de «microphone» a «audio-input-microphone» per a totes les mides (error 398160)

### Frameworks del Plasma

- Mou la gestió de «backgroundhints» a Applet
- Usa el selector de fitxers a l'interceptor
- Més ús de ColorScope
- També controla els canvis de finestra
- Admet que l'usuari elimini el fons i l'ombra automàtica
- Admet els selectors de fitxer
- Admet els selectors de fitxer QML
- Elimina el codi «qgraphicsview» de la safata
- No elimina i torna a crear la «wallpaperinterface» si no cal
- MobileTextActionsToolBar comprova si «controlRoot» està no indefinit abans d'usar-lo
- Afegeix «hideOnWindowDeactivate» a PlasmaComponents.Dialog

### Purpose

- Inclou l'ordre CMake que està a punt d'usar-se

### QQC2StyleBridge

- [TabBar] Usa el color de la finestra en lloc de color del botó (error 413311)
- Vincula les propietats activades a la vista activada
- [ToolTip] Basa el temps de venciment en la longitud del text
- [ComboBox] No enfosqueix el Popup
- [ComboBox] No indica el focus quan el missatge emergent és obert
- [ComboBox] Segueix la «focusPolicy»

### Solid

- [udisks2] Corregeix la detecció de canvi de suports per a les unitats òptiques externes (error 394348)

### Sonnet

- Desactiva el dorsal «ispell» amb el «mingw»
- Implementa el dorsal «ISpellChecker» per al Windows &gt;= 8
- Implementació de compilació creuada bàsica per «parsetrigrams»
- Incrusta «trigrams.map» a la biblioteca compartida

### Sindicació

- Corregeix l'error 383381 - L'obtenció de l'URL d'alimentació d'un canal de Youtube ja no funciona (error 383381)
- Extreu el codi de manera que es pugui corregir l'anàlisi del codi (error 383381)
- Atom té el funcionament amb icones (de manera que es pot usar una icona específica a l'Akregator)
- Converteix com una aplicació real «qtest»

### Ressaltat de la sintaxi

- Actualitzacions de la publicació final del CMake 3.16
- reStructuredText: corregeix els caràcters que precedeixen al ressaltat dels literals en línia
- rst: afegeix la implementació per als enllaços autònoms
- JavaScript: mou les paraules clau des del TypeScript i altres millores
- JavaScript/TypeScript React: reanomena les definicions de la sintaxi
- LaTeX: corregeix el delimitador de barra inversa a diverses paraules clau (error 413493)

### ThreadWeaver

- Usa els URL amb encriptatge del transport

### Informació de seguretat

El codi publicat s'ha signat amb GPG usant la clau següent: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Empremta digital de la clau primària: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB
