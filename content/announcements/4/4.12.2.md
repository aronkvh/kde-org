---
aliases:
- ../announce-4.12.2
date: 2014-02-04
description: KDE Ships Applications and Platform 4.12.2.
title: KDE Ships February Updates to Applications, Platform and Plasma Workspaces
---

February 4, 2014. Today KDE released updates for its Applications and Development Platform, the second in a series of monthly stabilization updates to the 4.12 series. This release also includes an updated Plasma Workspaces 4.11.6. This release contains only bugfixes and translation updates; it will be a safe and pleasant update for everyone.

More than 20 recorded bugfixes include improvements to the personal information management suite Kontact, the UML tool Umbrello, the document viewer Okular, the web browser Konqueror, the file manager Dolphin, and others.

A more complete <a href="https://bugs.kde.org/buglist.cgi?query_format=advanced&amp;short_desc_type=allwordssubstr&amp;short_desc=&amp;long_desc_type=substring&amp;long_desc=&amp;bug_file_loc_type=allwordssubstr&amp;bug_file_loc=&amp;keywords_type=allwords&amp;keywords=&amp;bug_status=RESOLVED&amp;bug_status=VERIFIED&amp;bug_status=CLOSED&amp;emailtype1=substring&amp;email1=&amp;emailassigned_to2=1&amp;emailreporter2=1&amp;emailcc2=1&amp;emailtype2=substring&amp;email2=&amp;bugidtype=include&amp;bug_id=&amp;votes=&amp;chfieldfrom=2013-06-01&amp;chfieldto=Now&amp;chfield=cf_versionfixedin&amp;chfieldvalue=4.12.2&amp;cmdtype=doit&amp;order=Bug+Number&amp;field0-0-0=noop&amp;type0-0-0=noop&amp;value0-0-0=">list of changes</a> can be found in KDE's issue tracker. Browse the Git logs for a detailed list of changes in 4.12.2.

To download source code or packages to install go to the <a href="/info/4/4.12.2">4.12.2 Info Page</a>. If you want to find out more about the 4.12 versions of KDE Applications and Development Platform, please refer to the <a href="/announcements/4.12/">4.12 release notes</a>.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.12.0/screenshots/dolphin.png" width="600px">}}

KDE software, including all libraries and applications, is available for free under Open Source licenses. KDE's software can be obtained as source code and various binary formats from <a href="http://download.kde.org/stable/4.12.2/">download.kde.org</a> or from any of the <a href="/distributions">major GNU/Linux and UNIX systems</a> shipping today.

#### Installing 4.12.2 Binary Packages

<em>Packages</em>.
Some Linux/UNIX OS vendors have kindly provided binary packages of 4.12.2 for some versions of their distribution, and in other cases community volunteers have done so. Additional binary packages, as well as updates to the packages now available, may become available over the coming weeks.

<em>Package Locations</em>.
For a current list of available binary packages of which the KDE Project has been informed, please visit the <a href="/info/4/4.12.2#binary">4.12.2 Info Page</a>.

#### Compiling 4.12.2

The complete source code for 4.12.2 may be <a href="http://download.kde.org/stable/4.12.2/src/">freely downloaded</a>. Instructions on compiling and installing 4.12.2 are available from the <a href="/info/4/4.12.2">4.12.2 Info Page</a>.

#### Supporting KDE

KDE is a <a href="http://www.gnu.org/philosophy/free-sw.html">Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href="/community/donations/">Supporting KDE page</a> for further information or become a KDE e.V. supporting member through our new <a href="http://jointhegame.kde.org/">Join the Game</a> initiative.



