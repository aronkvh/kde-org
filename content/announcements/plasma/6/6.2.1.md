---
date: 2024-10-15
changelog: 6.2.0-6.2.1
layout: plasma
video: false
asBugfix: true
draft: false
---

+ Discover Snap: Don't crash when a null channel is returned. [Commit.](http://commits.kde.org/discover/f05da2cb1bd6050e9c6c89905f3387e450de8cac) Fixes bug [#492657](https://bugs.kde.org/492657)
+ Kcms/wallpaper: fix crash when wallpaper config has invalid values. [Commit.](http://commits.kde.org/plasma-workspace/f06d43868e6c53326c98b79f32ba0cc8bd5108eb) 
+ Spacebar: Fixup SMS sending. [Commit.](http://commits.kde.org/spacebar/1057d392b822dd140731246badcf9c4db24fa4ee) 
