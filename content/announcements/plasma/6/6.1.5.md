---
date: 2024-09-10
changelog: 6.1.4-6.1.5
layout: plasma
video: false
asBugfix: true
draft: false
---

+ Screenedge: allow activating clients in drag and drop. [Commit.](http://commits.kde.org/kwin/d85dd623d7315f2e620c2c37e635f2fd3737a684) Fixes bug [#450579](https://bugs.kde.org/450579)
+ Applets/kickoff: Fix keyboard navigation getting stuck inside gridviews. [Commit.](http://commits.kde.org/plasma-desktop/2a62f024b8fdd3d301536f2f2ac757e68806764d) Fixes bug [#489867](https://bugs.kde.org/489867)
+ Klipper: fix copying cells when images are ignored. [Commit.](http://commits.kde.org/plasma-workspace/fead4fa8174aeb6e61354390e9ddee4627b31166) Fixes bug [#491488](https://bugs.kde.org/491488)
