---
date: 2022-01-04
changelog: 5.23.4-5.23.5
layout: plasma
video: false
asBugfix: true
draft: false
---

{{< video src-webm="https://cdn.kde.org/promo/Announcements/Plasma/5.23/Plasma_25AE_Final.webm" src="https://cdn.kde.org/promo/Announcements/Plasma/5.23/Plasma_25AE_Final.mp4" poster="https://kde.org/announcements/plasma/5/5.23.0/Plasma25AE_b.png" >}}

+ Save bluetooth status on teardown. [Commit.](http://commits.kde.org/bluedevil/daaa18920ae93b482585eb3df790a6d5a647b6db) Fixes bug [#445376](https://bugs.kde.org/445376)
+ System Monitor: Handle process parent changes in ProcessDataModel. [Commit.](http://commits.kde.org/libksysguard/311faef0ef0e5f60eebed2a5a00c43f5cb60aab1) Fixes bug [#446534](https://bugs.kde.org/446534)
+ Fix Klipper Actions content truncation. [Commit.](http://commits.kde.org/plasma-workspace/beb34e3d9781c7728260d486a835c52be21a6d83) Fixes bug [#444365](https://bugs.kde.org/444365)
