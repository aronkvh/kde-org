---
date: 2023-09-12
changelog: 5.27.7-5.27.8
layout: plasma
video: false
asBugfix: true
draft: false
---

+ KRunner: make debut on X11 faster. [Commit.](http://commits.kde.org/plasma-workspace/433a4365b77a9400280effcd3cce3702bc2f577e)
+ Powerdevil: don't automatically suspend by default if running in a virtual machine. [Commit.](http://commits.kde.org/powerdevil/cccb8a70a35cd8dd74ccb153b00438f44484ef01)
+ System Settings: Remove soft hyphens in keywords used for searching. [Commit.](http://commits.kde.org/systemsettings/054713702216bf54bd0420c6f742aa031ed48ab6)
