---
qtversion: 5.15.2
date: 2022-11-14
layout: framework
libCount: 83
---


### Baloo

* [TermGeneratorTest] Reduce code duplication
* Correct and simplify OrpostingIterator constructor
* Convert kded metadata to JSON

### Breeze Icons

* Add accessories-dictionary-symbolic symlink to a book-looking icon (bug 461033)
* Add new icons for Fortran source code
* Add 64px audio-headset, fix issues with 64px audio-headphones (bug 447536)
* Add data-success icon
* Add icon for OpenOffice extensions

### Extra CMake Modules

* Make the expiry of these artifacts more reasonable, 3 months is a bit excessive
* avoid lib64 directory on NixOS
* Add quotes, otherwise if is malformed when envvar not defined
* Allow KF_IGNORE_PLATFORM_CHECK to be set through an environment variable

### KDE Doxygen Tools

* remove link to Other KDE Software

### KArchive

* Make error messages translatable

### KAuth

* Config.cmake.in: declare static dependencies

### KBookmarks

* Don't use KXmlGui when building without deprecated stuff

### KCMUtils

* Remove space
* Kauth doesn't build on windows
* KCModuleQML: Make sizeHint() check more robust

### KCodecs

* Prepare for 5.15.7: adapt test

### KConfig

* kconfig_compiler: switch away from bit mask for signal enum values
* kconfig_compiler: fix generation of bit masks enum items for signals
* kconfig_compiler: perform signals count check earlier
* Fix KConfigGui initialization in case of static builds (bug 459337)

### KConfigWidgets

* avoid stating files during restore of recent files (bug 460868)
* Ensure icon is always set for recent files actions
* Add file icons to open recent menu
* Replace custom color preference code with KColorSchemeWatcher
* Intialize default decoration colors
* [KCommandBar] Fix shortcut background

### KContacts

* Add setAddresses

### KCoreAddons

* Add missing . in error string
* KPluginMetaData: Check applicationDirPath first when resolving plugin paths (bug 459830)
* Fix static build of downstream consumers
* KFileSystem: add Fuse Type; use libudev to probe underlying fuseblk type

### KDeclarative

* Make QQmlContext's contextObject outlive the QQmlContext
* Completely deprecate KDeclarative class
* Port manual test app away from deprecated KDeclarative API

### KDELibs 4 Support

* Add CMake option to build WITH_X11

### KDesignerPlugin

* add win ci, for kdelibs4support

### KDESU

* Add missing deprecation wrapper for KDEsuClient::isServerSGID impl

### KFileMetaData

* OdfExtractorTest: Add test cases for Flat XML documents
* OdfExtractor: Only go looking for textual content in office:body
* OdfExtractor: Support "Flat XML" variants
* PngExtractor: Only extract metadata when it was asked for

### KDE GUI Addons

* Add misisng find_dependency's for static builds
* systemclipboard: Don't signals data source cancellation (bug 460248)
* Guard the global was actually intialised
* Implement destuctor for keystate protocol implementation
* kcolorschemewatcher: make changing colour schemes work as expected on macOS
* [kcolorschemewatcher] Default to light mode, where AppsUseLightTheme isn't set (notably Windows 8.1)
* enable automatic dark-mode switching on macOS
* Add API for system color preference reading

### KI18n

* Try fixing build on Windows mingw
* Add missing <cerrno> include

### KImageFormats

* Don't install desktop files for image formats when building against Qt6
* raw: Don't seek back if we were asked to read too much
* jxl: indicate when all frames have been read
* avif: indicate when all frames have been read
* avif: always indicate endless loop (bug 460085)
* avif: return `false` in `canRead()` when `imageIndex >= imageCount` (bug 460085)
* Auto-rotate input images in readtest
* jxl: remove C-style casts
* avif: Use reinterpret_cast instead C cast
* heif: replace C cast with static_cast
* heif: use heif_init/heif_deinit with libheif 1.13.0+
* FindLibRaw: fix include dir, should not contain prefix libraw/ (bug 460105)
* Fix duplicated tests
* ANI partial test and PIC test added
* PSD: impreved support to sequential access device
* Fix messages
* CMakeLists: enable EXR test
* Added EXR test image
* Fixes for sequential devices

### KIO

* Add new base class for thumbnail plugins
* Fix string formatting in "Do you really want to delete it?" message
* [kdiroperator] Don't add actions to actioncollection when building without deprecated API
* Deprecate KDirOperator::actionCollection
* [kfilewidget] Don't remove mkdir action from the action collection
* Fix deprecation of virtual functions
* Deprecate KDirOperator::setView(QAbstractItemView *)
* [kcoredirlister] Deprecate setMimeExcludeFilter
* [kcoredirlister] Rename showingDotFiles to showHiddenFiles
* [kdiroperator] Rename setView to setViewMode
* [kcoredirlister] Deprecate itemsFilteredByMime
* [kdiroperator] Remove usage of itemsFilteredByMime
* [knewfilemenu] Decouple from KActionCollection
* KPropertiesDialog: Add tooltip for magic mime type, too
* kurlnavigatortest: add test for absolute path in relative path handling
* KUrlNavigator: fix handling absolute paths
* kurlnavigatortest: add test case for relative hidden directory
* Remove KCrash dependency on Android
* KFilePlacesModel: Move open files error on teardown to KIO
* Add a CMake option, off by default, to control ftpd and wsgidav usage
* Conditionally chow hidden folders in the location bar's dropdown menu
* fix dav overwrite
* don't disable tests
* Warning dialogs: Change button text from "Delete" to "Delete Permanently"
* AskUserActionInterface: add DeleteInsteadOfTrash deletion type (bug 431351)
* WidgetsAskUserActionHandler: fix showing file name for single url
* Message box methods: add API using "action" terms instead of "Yes" & "No"
* Deprecate defaulting to "Yes" & "No" message box buttons
* Add messagebox: KIO worker for testing messageBox() calls from worker
* Remove service type definition for urifilter plugins
* DeleteOrTrashJob: it's included in KF 5.100
* [ioslaves/remote] Convert kded metadata to JSON
* Fix missing details in message dialogs from worker
* UserNotificationHandler: fix messagebox type mismatches
* [kdiroperator] Allow accessing actions without KActionCollection
* KDirOperator: use KIO::DeleteOrTrashJob
* Add DeleteOrTrashJob
* [kopenwithdialog] Handle absence of a mime comment gracefully
* Port Windows-only code away from deprecated KIO API
* KFileItemListProperties: Better respect ioworker metadata
* KIO HTTP: Store error string for deferred result
* KFilePlacesView: Show busy indicator while (un)mounting a device
* KFilePlacesModel: Change teardown action to indicate teardown in progress
* KFilePlacesModel: Add DeviceAccessibilityRole
* KFilePlacesItem: Allow passing roles for dataChanged

### Kirigami

* icon: `itemSize` should be `size`
* FormLayout: remove top margin for TextArea
* BannerImage: fix title background not aligning to vertical center
* TabletModeWatcher: reduce the amount of blocking calls we do
* SingletonCreation: Make sure singletons are reused with the engine
* ToolBarLayout: Do not crash when a null action is passed
* set a11y properties on swipelistitem actions buttons
* Set is so that if there is at least 1 action in the CategorizedSettings pagestack that the current index is set to it on open
* DefaultCardBackground: Use simple drop shadow and less shadow
* FormLayout: Use const instead of let where possible
* Examples + tests: Update usage of actions in Page components
* Improve appearance of ListSectionHeader and remove list item separators by default
* GlobalDrawerActionItem: Simplify spacing
* GlobalDrawerActionItem: Use consistent spacing between icon and label, and left margin
* GlobalDrawerActionItem: Make icon size readonly
* GlobalDrawerActionItem: Don't increase icon size for mobile
* Fix verticalAlignment bindingloop in PlaceholderMessage
* WheelHandler: Fix memory leak
* ActionToolBar: let keyword not used
* ListSectionHeader: Elide when text overflows
* FormLayout: remove hack
* Remove cases where hoverEnabled is used in place of supportMouseEvents
* SearchField: Check for acceptableInput before firing an event
* GlobalToolBar: Expose the title padding property
* Check preventStealing earlier in ColumnView (bug 460345)
* Don't load page header components asynchronously by default
* SwipeListItem: Make sure we only show the aggressive warning when it's due (bug 455789)
* imagecolors: don't early return when there is no window for QQuickItem
* Avatar: fix blurry avatar when using Qt scaling (bug 356446)
* Fix ginormous FABs
* PC3/TextField,SearchField: Fix mirroring detection
* SearchField: Align search icon vertically
* SearchField,ActionTextField: Use non-destructive clear() method
* Doc: ActionTextField: Use StandardKey instead of hardcoded Ctrl+F string
* SearchField: Bump QML imports and fix code style
* BasicListItem: Make text more readable when using fadeContent property
* FormLayout: Fix nullable items
* Remove renderType workaround that was fixed in Qt 5.14.0 RC1
* Replace sizeForLabels with Units.iconSizes.medium/large where relevant
* AboutItem: make license text selectable
* make example build on qt6
* Remove unnecessary break statement

### KItemModels

* Also fix kconcatenaterows_qml test for static builds
* Fix ksortfilterproxymodel_qml test for static builds (bug 459321)

### KItemViews

* KCategoryDrawer: Update design to match Kirigami list categories

### KNewStuff

* KF5NewStuffCoreConfig.cmake: fix resolving QT_MAJOR_VERSION at generation
* [kmoretools] Replace QDesktopServices::openUrl with KIO::OpenUrlJob
* Add qtdesigner plugin for KNewStuffWidgets

### KParts

* [partloader] Remove references to KPluginLoader from docs
* Don't install service type definitions when building without deprecated stuff
* Add new functions to instantiate parts

### KRunner

* runnermanager: only emit `queryFinished` when previous jobs are still running

### KService

* Do not warn if KService("") is instantiated

### KTextEditor

* Set default mark type only if control is pressed
* Don't install desktop file for katepart when building against Qt6
* CamelCursor: Improve complex unicode handling (bug 461239)
* we want to shift unsigned ints
* auto completion got me
* remove file name for saveas only for remote files (bug 461161)
* Simplify and optimize KateScriptDocument::anchorInternal
* Indenter: Dart and Js fixes
* Fix js/dart indent in array
* Make Tab/Shift+Tab completion work
* Bring Shell-like tab completion back, and make it work
* Remove select all and deselect from context menu
* Remove double message about lack of access rights
* KateCompletionModel: Dont sort Group::prefilter
* KateCompletionModel: Simplify changeCompletions()
* Avoid detaching currentCompletion
* KateCompletionModel: Remove not needed setters
* Remove KateCompletionModel config from KateCompletionWidget
* Avoid excessive filtering and calls to modelContentChanged
* Completion: Avoid extra model reset
* Remove "Preview:" label in themes tab
* Fix fallback logic
* Fix tab order in search (bug 440888)
* fix clipboard selection update on shift mouse click (bug 443642)
* Ensure to set highlight on reload when file type was set by user (bug 454715)

### KUnitConversion

* Add missing forms for mass units: tonnes, ct, lbs
* add singular abbreviation forms as match strings for teaspoon and tablespoon

### KWallet Framework

* mark some binaries as non-gui
* Add support for plain transfer algorithm to Secret Service API (bug 458341)
* Change naming and order of FreedesktopSecret members to match the spec

### KWayland

* [registry] Bump PlasmaWindowManagement version to 16

### KWidgetsAddons

* avoid stating files during restore of recent files (bug 460868)
* Add a method to remove all actions in one go
* KTitleWidget: Constraint the frame size so it properly aligns (bug 460542)
* KPageDialog: Collapse margins also for flat list
* KToolBarPopupAction: Apply popupMode to existing widgets
* Deprecate KStandardGuiItem::yes() KStandardGuiItem::no()
* KMessageDialog: add API using "action" terms instead of "Yes" & "No"
* KMessageBox: add API using "action" terms instead of "Yes" & "No"
* Fix potential crash in fix for 458335

### NetworkManagerQt

* DeviceStatistics: Replace setRefreshRateMs call with manual DBus call
* Remove dead CMakeLists code

### Plasma Framework

* PC3/BusyIndicator: Disable hover, drop unused id, and shorten a comment
* Never time out tooltip while its control is still hovered (bug 397336)
* windowthumbnail 'count > 0' is always true
* Dialog: Avoid using plasmashell protocol when running in kwin
* Make KPackage an implementation detail of Plasma Applets
* Deprecate public includes for KPackage
* templates/cpp-plasmoid: Load icon as svg rather than svgz
* templates/cpp-plasmoid: Remove unneeded EnabledByDefault and X-Plasma-MainScript keys
* templates/cpp-plasmoid: Consistently require ECM, KF and Qt dependencies
* templates/cpp-plasmoid: Port to newer KPackageStructure key instead of ServiceTypes
* templates/cpp-plasmoid: Do not embed json file in plugin
* Create SVGZ files without embedded filename & timestamp -> reproducibility
* Oxygen: Drastically improve buttons
* Oxygen: Invert input box color and increase size
* Oxygen: Update Inkscape Metadata
* Oxygen: Fix tab bars not rendering, add N/E/S/W sides
* Oxygen: Fix active button size
* ExpandableListItem: Port to explicit signal handler parameter names
* Center the AppletPopup dialogs when possible
* framesvgitem: avoid unnecessary type conversion in `resizeFrame`
* framesvgitem: use `QQuickItem::size()` directly in `resizeFrame`
* framesvgitem: port two qMax to std::max
* framesvgitem: remove one unnecessary maxvalue comparison
* PC3/BusyIndicator: Revert RotationAnimator back to property interceptor syntax
* PC3/BusyIndicator: Fix hang & rapid jump on start
* PC3/BusyIndicator: Don't do extra work when animation starts
* PC3/BusyIndicator: Port opacity animation to Easing.OutCubic
* PC3/BusyIndicator: Center the active/rotating part of the control
* PC3/BusyIndicator: Fix QML/JS code style, bump imports
* fix refcount of dialog instances
* Fix name of margin hints of task manager (bug 456076)
* PC3/TextField,SearchField: Fix mirroring detection
* SearchField: Align search icon vertically
* SearchField,ActionTextField: Use non-destructive clear() method
* PC3/Slider: Fix tick marks direction for vertical orientation
* Manage the case someone is asking for the attached of an applet
* Fix scrollbar visibility when contentWidth or contentHeight is 0
* Drop deprecated KDeclarative::setupEngine calls
* iconitem: add test for devicePixelRatio
* svgitem: fix blurry icon on HiDPI screens
* iconitem: fix blurry icon on HiDPI screens
* Revert "Install a plugin for org.kde.plasma.plasmoid"
* Add 5G network icons
* ExpandableListItem: Simplify Accessible.description expression
* ExpandableListItem: Use uniform standard margins for actions list
* Perform initialization in initialization list
* IconItem: Use standard Units.longDuration for cross-fade animation

### Prison

* Convert code128 data table to binary literals
* Simplify QR reference PNG images

### QQC2StyleBridge

* Never time out tooltip while its control is still hovered
* TextField: Use effectiveHorizontalAlignment without extra mirroring for placeholder

### Syntax Highlighting

* Jira: improve highlighting
* Jira: fix Bold and Stroked Out highlight at start of line (bug 431268)
* CMake: fix nested parentheses highlinthing (bug 461151)
* systemd_unit: update to systemd v252
* Common List: optimize (~40% faster)
* Common Lisp: highlight numbers preceded by -
* Common Lisp: highlint 'symbol and #'symbol
* Common Lisp: add operators
* Common Lisp: add .el extension (emacs script)
* powershell.xml: Recognize variable substitutions in double quoted strings
* powershell.xml: Recognize single quoted strings
* debchangelog: add Forky
* debchangelog: add Lunar Lobster
* CMake: return() as Control Flow
* CMake: add Control Flow style for if, else, while, etc (bug 460596)
* Fix name (for consistency with CartoCSS MML)
* Better highlighting for some GIS-related file formats
* VHDL: support of VHDL-2008
* Nix: fix decimal numbers format and some performance changes
* Alerts: add NO-BREAK SPACE (nbsp) as keyword deliminator (bug 459972)
* Git Rebase: add missing commands
* Git Ignore: more highlight
* cmake.xml: New features for CMake 3.25
* Bash/Zsh: fix Parameter Expansion style for } in ${...}
* Zsh: add folding for group command
* Bash/Zsh: fix group command with coproc statement (bug 460301)
* use a constant container with range-based for loop to avoid copies
* Ansi/HtmlHighlighter: prefer QTextStream::readLineInto() to QTextStream::readLine() to avoid allocation
* fix AnsiHighlighter::highlightData() with valid definition, but without any context
* increment version number after keywords change
* cpp.xml: add missing QStringTokenizer class for highlighting
* Add folding for maps, structs and lists in elixir
* Add opendocument "flat XML" file types
* Add do: to tests
* Undo unintended line formating
* Remove any reference to fn being a definition
* Adapt tests to new output
* Add folding to elixir code

### Security information

The released code has been GPG-signed using the following key:
pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure <faure@kde.org>
Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB
