---
qtversion: 5.15.2
date: 2022-04-09
layout: framework
libCount: 83
---


### Breeze Icons

* Add some more symlinks for zoom icons (bug 450870)
* Add input-tvremote

### Extra CMake Modules

* Fix finding qmake
* ECMQueryQt: don't fail when TRY is used
* ECMQueryQt: Add fallbacks
* ECMAddQch: Make it work with Qt6::qhelpgenerator too
* Deprecate ECMQueryQmake, superseded by ECMQueryQt
* KDEInstallDirs6: port from custom logic to ecm_query_qt
* Port from ECMQueryQmake to ECMQueryQt
* Android: use the current cmake executable
* Add ECMQueryQt module, which encompases both Qt5 Qmake and Qt6 qtpaths
* generate-fastlane-metadata: Don't fail if source URL can not be found
* Fix checking the repository name for detached remotes
* Android: Set CMAKE_TRY_COMPILE_PLATFORM_VARIABLES
* Check that you're building the framework in a supported platform
* Android: autodetect the use of llvm

### KDE Doxygen Tools

* add cmake file for standardised build

### KAuth

* add chrono overload for setTimeout

### KBookmarks

* Add Android to supported platforms in repo metadata

### KCalendarCore

* Notify on notebook change for exceptions
* Note use of low-level libical function
* Print more information about errors when loading vCalendars (bug 328976)
* Implement notebook association when parsing iCal data
* Fix storing of times with UTC offsets
* Compare QDateTime times, timespecs, and timezones

### KCMUtils

* Fix KPluginSelector KCMs missing their metadata on plugin loading (bug 444378)
* Mark Windows and macOS as supported

### KConfig

* Disable DBus support on Windows by default
* Make KConfig::mainConfigName() public
* kconf_update: Fix checking for changes of update files

### KConfigWidgets

* Merge KColorSchemePrivate::init and constructor
* Unify both KColorSchemePrivate constructors
* Default to light mode, where AppsUseLightTheme isn't set (notably Windows 8.1)
* Move KStatefulBrush to its own dedicated header
* Deprecate KColorScheme::contrast
* Fix automatic color scheme on Windows
* khamburgermenu: Ensure menu is polished before creating window
* Fix blame ignore list
* [kcolorschememanager] Initialize selected scheme from config
* Add Android to supported platforms in the repo metadata

### KContacts

* Addressee: make birthday writable from QML (bug 446665)
* Fix bug 451962:  append Instant Messaging service list (bug 451962)

### KCoreAddons

* Add default argument for KPluginFactory::create where no keyword and args have to be specified
* Also wrap Kdelibs4ConfigMigrator in a QT_VERSION check for Qt6
* Optimize KPluginMetaData::findPluginById by using QPluginLoader directly
* Remove *.doc pattern for text/plain
* KPluginMetaData: find plugins in the directory of the executable, too

### KCrash

* metainfo.yaml - add macOS as a supported platform

### KDeclarative

* Deprecate KQuickAddons::QtQuickSettings::init

### KDELibs 4 Support

* Use KDE_INSTALL_FULL_* variables where needed
* Don't assume libexec is a LIB_INSTALL_DIR subfolder

### KFileMetaData

* exiv2extractor: add support for avif, heif, jpeg xl

### KGlobalAccel

* Add macOS to supported platforms list

### KDE GUI Addons

* Gracefully ignore SIGPIPE
* Use nativeInterface to access the x11 display

### KHolidays #

* All values of category get read from holiday file
* Unreliable print of solstice in holidays
* Remove double entries and correct indian- (bug 441275)

### KHTML

* Build without kglobalaccel on Windows and mac

### KIdleTime

* Fixed crash during KIdleTime::timeoutReached() (bug 451946)
* Use nativeInterface to access the x11 display

### KImageFormats

* Fix XCF parasites metadata in QImage and support to ICC profile
* avif: encoder speed 7->6
* avif: fix jumpToImage
* avif: warn about non-recommended libavif configuration

### KIO

* Fix KRecentDocumentTest
* fix reading wrong integer type from config (bug 452048)
* Add Yandex search keywords
* [KFilePlacesView] Don't append "(hidden)" to disappearing groups
* Don't use KCrash on Android, currently not used there
* KOpenWithDialog: alternative fix for the sort filter model regex
* Introduce KCoreUrlNavigator
* filewidgets: Auto-select filename before extension in filename dialog
* Add support in krecentdocument to add to recently-used.xbel
* [KFilePlacesView] Drop now unused placeEntered/Left handling
* [KFilePlacesView] Show free space as permanent thin line
* include QStandardPaths
* KRecentDocuments: don't save history for hidden files by default
* KUrlNavigator: don't force LtR (bug 428567)
* [KPropertiesDialog] Use PlainText format for most labels
* KFileItem: protect againt ' ' passed mimeType
* KFileItem: use passed mimeType to determine isDir (bug 401579)
* KOpenWithDialog: Include arguments in name when writing a new desktop file
* Add Android to the list of supported platforms
* [KPropertiesDialog] Make read-only filename label selectable by mouse
* [KFilePlacesModel] Improve outcome of dropped places
* [ftp] Simplify code

### Kirigami

* InlineMessage: Do not layout for actions at bottom when there are none
* only change height if visible
* -warning: Shortcut.sequence is an Array
* Units: Delete QML FontMetrics object when Units is destructed
* ListItemDragHandle: Emit moveRequest when cursor enters a new index (bug 451014)
* BasicListItem: tighten up spacing for text+subtitle labels
* ListSectionHeader: vertically align default text item
* PlaceholderMessage: compare string length rather than content
* Render Page's default heading as PlainText
* Page: Don't load global toolbar async so that the header looks less stuttery
* Fix warning on the org.kde.desktop Theme
* controls/SwipeListItem: handle RtL properly (bug 441156)

### KItemViews

* KExtendableItemDelegate: Fix HiDPI positioning of indicator arrows (bug 414904)

### KNewStuff

* Fix problems renaming a temporary file on Windows
* Installation: don't let QTemporaryFile delete the file before using it
* Do not set user-visible name as additional agent information when copying provider (bug 451165)
* Do not set user-visible name as additional agent information (bug 451165)
* Engine: Default to autoconfig.kde.org for reading ProvidersUrl
* Port to ECMQmlModule
* Fix close button for NewStuff.Dialog not existing on X11 (bug 437653)
* Fix certain tars not being recognized as valid archives (bug 450662)

### KPackage Framework

* PackageJobThread: Also check if metadata.json file exists
* Explicitly call KPluginMetaData::fromJsonFile when constructing metadata objects
* Fix desktoptojson invocation when cross-compiling

### KParts

* Mark copy job when opening remote URLs as transient
* Mark Android as supported

### KPty

* Add macOS to supported platforms
* add setWinSize() overload which also takes pixels

### KQuickCharts

* Add Android, Windows and MacOS to supported platforms
* Correct install location for ecm_finalize_qml_module()
* cmake: Use the right path to find ECMFindQmlModule.cmake
* Don't crash if we have no qmlContext for a deprecated object
* Use the right path when including ECMFindQmlModule in CMake package config
* Fix punctuation/whitespace of runtime rate-limited deprecation warnings
* Don't add module-uri based path to finalize_qml_module
* Don't use CMake exports for things that aren't really exported
* Port controls module to use ECMQmlModule
* Port main module to use ECMQmlModule

### KRunner

* Mark qrc file as non-copyrightable
* Remove upper-limits for RunnerManagerTest's timeouts

### KTextEditor

* simplify + optimize uniq command
* Fix switching back to normal mode from VI
* quickfix crash on Qt 6
* remove shortcut conflicting with KDevelop ATM
* fix wrap of search for slash (bug 451893)
* Fix multicursor anchor in findNext
* Fix multicursor pasting when cursors don't match copied text
* Fix ordering of selectionText
* Warn + assert if multicursorClipboard.size == 1
* Implement multicursor copy/paste
* Hide statusbar with permanent widget
* Improve cursor movement with RTL text in doc (bug 451789)
* Add support for custom line height (bug 328837)
* Fix cursor in RTL text with dyn wrap on (bug 385694 368182)
* Don't try to convert non-char key codes to strings
* port to QScreen & fix position on Wayland
* Move print and export actions into submenu
* Add actions to allow adding a new line above/below
* multicursor: Use same code path as primary for cursor placing
* Fix multicursor position after selection removal
* use special value to indicate never for auto save
* Allow auto saving document
* Take wrapCursor into account when creating cursor using keybaord
* Improve autocompletion handling
* Add support for auto wrap selection with chars + test
* Add support for auto brackets + fix indentation + test
* Fix crash with stale indexes (bug 451593)
* Rewrite merge selections to fix all known cases
* multicursors: combine cursor and selection into one type
* fix config dialog sizing
* Disable multicursor in block selection mode (bug 451542)
* Remove shortcut for duplicateLinesUp
* Clear previous selections when finding AllOcurrences
* Ensure to provide a proper cursor position
* Use moving cursors directly in typeChars
* Use doc->lineLength
* Avoid tagging lines that are not in view
* Fix toggle comment with space at the start (bug 451471)
* Improve performance with multiple cursors
* Make the multicursor modifier configurable
* keep bom action in tools
* Move some editing related actions to Edit menu from Tools
* Fix incorrect select anchor after undo
* Fix multicursor undo selection restoration
* Allow toggling camel cursor movement
* Clear highlights when multiselecting next occurence
* Find All occurences and select them
* Find next occurence and select it
* Make multicursor shortcuts actions
* multicursor: Introduce setSecondaryCursors
* multicursors: Support tab indent
* Undo: Clear cursors when undo doesn't have secondary cursors..
* Don't allow to place cursor on top of primary
* New shortcuts for duplicate-line-up/down
* Read script action shortcuts as portable text instead of native text
* Remove stray include
* Fix undo doesn't restore cursors
* new method to add a bunch of cursors at once
* Fix backspace cursor duplication
* Completely disable cursors for vimode
* Try to add new cursor in the same column as primary cursor
* clear multicursors on mouse move
* Fix word jumping
* Restore old Ctrl+H behaviour
* Make cursor painting instant
* Make paintCursor a bit more efficient
* ensure to update cursors immediately
* Fix Ctrl+H, use only primary selection text as pattern
* Multicursors undo/redo support
* Allow creating cursors from selection
* Slightly improve Alt+Ctrl+Up/Down cursor creation
* Fix selection merging for cursorUp/Down
* Fix home and end actions
* Ensure unique cursors after deleting
* Remove multicursors on doc reload
* move caret drawing to separate function
* Fix all cursors get hidden with cursorUp
* Remove selections when adding new cursor
* Fix cursor still visible when toggling
* Disable multicursors for overwrite mode
* Clear multicursors in block select mode
* Multicursor text transforms (capitalize, lowercase, uppercase)
* multicursor transpose chars
* Multicursor newline support
* Multi cursor auto completion support
* Ctrl + Alt Up/Down to create new cursors
* Remove unused method
* multicursor copy support
* Add incremental multicursor search
* Fix selection removal with multicursors
* Multicursor home/end support
* Mutlicurosr delete key
* Disable multicursor for overwrite/vi mode
* Backspace for multiple cursors
* multicursor selection removal
* Multicursor kill lines
* Improve cursorUp and handle selection removal
* Remove multicursors on big movements
* WIP: Basic multicursor support
* Don't build KAuth integration on Windows
* vimode: fix extra newline when using the "Find" action (bug 451076)
* fixed loop break condition
* ensure correct highlighting after text change
* fix highlighting while typing in search bar (/)
* fix config dialog sizes for KTextEditor (bug 445541)
* simplify check for empty last line
* add new line on save inside the buffer (bug 450919)
* Implement TextBuffer::saveBuffer without QTextStream
* tidy: make trivially destructible

### KWayland

* Install Client headers in a dirs hierarchy matching the C++ namespaces
* We don't actually need Qt6WaylandCompositor

### KWidgetsAddons

* Align buttons (and icon) on top, also when word wrap is off, but text is pre-formatted to span multiple lines
* avoid quadratic search of children widgets

### KXMLGUI

* KShortcutsEditorDelegate: Fix HiDPI rendering of indicator arrows (bug 414904)
* Tests: Enable HiDPI pixmap rendering
* Add Android to supported platforms in repo metadata
* Fix broken "Add to Toolbar" action

### ModemManagerQt

* Check DBus signal connection
* Include milliseconds when formatting SMS timestamps

### NetworkManagerQt

* Connect in the correct order everywhere
* Remove workaround for properties changing
* Do not create devices to just find out the type

### Plasma Framework

* Make WindowThumbnail build against Qt6 as well
* Highlight: change hover: property to hovered:
* Don't install the heading SVG in breeze-light and breeze-dark Plasma themes
* AppletInterface: Add `self` property which just returns `this`
* Allow applets to have empty json metadata & use KPackage's metadata instead
* Do not compute sizes within dialogs when they're not visible
* Port dateengine away from inperformant KPluginInfo hack
* Factor out QML module URI string
* Adapt to the deprecation of qmlRegisterInterface in Qt6
* Add default switch statement
* Add power profile icons (bug 449475)
* Fix Plasmoid attached property look-up on Config objects (bug 451482)
* Fix potential null parent access
* Break recursion when checking compact representation
* Use generic breeze-styled arrow in Plasma style
* Port to PlasmaExtras version of Highlight
* Hardcode keyboard shortcut in SearchField for now
* Add ActionTextField, SearchField, and PasswordField
* PC3: Remove unreachable code in Svg item from DefaultListItemBackground
* AppletQuickItem: Ensure we provide the attached property when we have it
* Port from using plasmoid to Plasmoid
* Calendar: Draw highlight below the text (bug 451209)
* Use PACKAGE_CMAKE_INSTALL_PREFIX before any find_dependency() calls
* Move Highlight from PlasmaComponents 2.0 to PlasmaExtras
* Introduce PLASMA_NO_CONTEXTPROPERTIES to disable Plasma QML context properties

### QQC2StyleBridge

* Do not make space for invisible items in menus
* TextFieldContextMenu: Make menu separator invisible when its preceding items are too
* KQuickStyleItem: use control mirrored property if available

### Solid

* Recognize fuse.gocryptfs mounts as devices like we do for cryfs and encfs (bug 452070)

### Sonnet

* Enable Windows spellchecker with mingw
* Add Android to supported platforms in repo metadata
* hunspell: resolve symlinks with canonicalFilePath()

### Syntax Highlighting

* Go syntax - "go" -> ControlFlow
* Go syntax - Separate ControlFlow from Keywords
* Bash/Zsh: fix expression closing parenthesis in regex (bug 450478)

### Security information

The released code has been GPG-signed using the following key:
pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure <faure@kde.org>
Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB
