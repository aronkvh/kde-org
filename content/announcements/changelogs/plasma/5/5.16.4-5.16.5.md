---
aliases:
- /announcements/plasma-5.16.4-5.16.5-changelog
hidden: true
plasma: true
title: Plasma 5.16.5 Complete Changelog
type: fulllog
version: 5.16.5
---

### <a name='breeze' href='https://commits.kde.org/breeze'>Breeze</a>

- [SplitterProxy] Don't manually mapToGlobal. <a href='https://commits.kde.org/breeze/f16961c3eb4ce32d4cb124674d97756eadbcb19e'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D22851'>D22851</a>

### <a name='discover' href='https://commits.kde.org/discover'>Discover</a>

- Odrs: fix fetching reviews. <a href='https://commits.kde.org/discover/5e994dca11f4f877489caf6e8c57ede89e416d14'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/411034'>#411034</a>
- Odrs: don't leak qnam instances. <a href='https://commits.kde.org/discover/55bd6a787bdbf2e8d339ffad0d6d939adfd9fef2'>Commit.</a>

### <a name='drkonqi' href='https://commits.kde.org/drkonqi'>Dr Konqi</a>

- Make fixture file name windows-safe. <a href='https://commits.kde.org/drkonqi/64bfaf75cd217fe416426ddec31ef3113eee65f3'>Commit.</a>

### <a name='kde-gtk-config' href='https://commits.kde.org/kde-gtk-config'>KDE GTK Config</a>

- Fix build with pango 1.44. <a href='https://commits.kde.org/kde-gtk-config/cf8db4ebe5ef5e54517604cd9330535983e4dff0'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D22832'>D22832</a>

### <a name='kdeplasma-addons' href='https://commits.kde.org/kdeplasma-addons'>Plasma Addons</a>

- Fix build. <a href='https://commits.kde.org/kdeplasma-addons/c389606dc950ed66d26eb6f2c7b2a2346103fbb0'>Commit.</a>
- Fix #410744: Duplicate results when a 2nd unit is partially written in krunner. <a href='https://commits.kde.org/kdeplasma-addons/239f574c5b2fe1367406f7ccecd31f491ce46a5e'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/410744'>#410744</a>. Phabricator Code review <a href='https://phabricator.kde.org/D23064'>D23064</a>
- Fixed dictionary runner not finding any definitions. <a href='https://commits.kde.org/kdeplasma-addons/eac0dbd5f1c0f1ba54bd9c9682da096d9c78db2a'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/376905'>#376905</a>. Phabricator Code review <a href='https://phabricator.kde.org/D22814'>D22814</a>

### <a name='kscreen' href='https://commits.kde.org/kscreen'>KScreen</a>

- Add missing includes. <a href='https://commits.kde.org/kscreen/0c157186eb4020f520e88bf43378754ca9710cd7'>Commit.</a>

### <a name='kscreenlocker' href='https://commits.kde.org/kscreenlocker'>KScreenlocker</a>

- Port away from deprecated KWindowSystem API. <a href='https://commits.kde.org/kscreenlocker/3edc9f8efc9a40fe1ce2b030a40c288ae9ec4a41'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D23228'>D23228</a>

### <a name='kwayland-integration' href='https://commits.kde.org/kwayland-integration'>KWayland-integration</a>

- Remove slideWindow(QWidget\*) overload with recent KWindowSystem. <a href='https://commits.kde.org/kwayland-integration/143a325ad1f4c57ef9f64e57f009e7e9085a8e90'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D23334'>D23334</a>
- Fix build with recent frameworks and Qt 5.13. <a href='https://commits.kde.org/kwayland-integration/13d87f47920b82679ad31468a62cec951768772d'>Commit.</a>

### <a name='kwin' href='https://commits.kde.org/kwin'>KWin</a>

- [effects/desktopgrid] Don't change activities. <a href='https://commits.kde.org/kwin/e918cb5d2d3de033635d0cd7463de25fd6312b24'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/301447'>#301447</a>. Phabricator Code review <a href='https://phabricator.kde.org/D14046'>D14046</a>
- Remove slideWindow(QWidget\*) overload with recent KWindowSystem. <a href='https://commits.kde.org/kwin/ba5432fd52ef69b906c665fb39c8332baf2b78d6'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D23227'>D23227</a>

### <a name='oxygen' href='https://commits.kde.org/oxygen'>Oxygen</a>

- [SplitterProxy] Don't manually mapToGlobal. <a href='https://commits.kde.org/oxygen/47459c65df41a0a7de9de3a1b7da4d97c01cc4a8'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D22899'>D22899</a>

### <a name='plasma-nm' href='https://commits.kde.org/plasma-nm'>Plasma Networkmanager (plasma-nm)</a>

- Add some missing includes. <a href='https://commits.kde.org/plasma-nm/33ab48684dee54b26e0916ba320e59b113d7a3af'>Commit.</a>
- Make "Network Disconnected" notification low priority. <a href='https://commits.kde.org/plasma-nm/b8e7d4c03b393c1d91a0343e7a517b92e7f6496f'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D23248'>D23248</a>

### <a name='plasma-pa' href='https://commits.kde.org/plasma-pa'>Plasma Audio Volume Control</a>

- Fix speaker test not showing sinks/buttons. <a href='https://commits.kde.org/plasma-pa/097879580833b745bae0dc663df692d573cf6808'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D23620'>D23620</a>
- [Microphone Indicator] Don't show if there are no microphones. <a href='https://commits.kde.org/plasma-pa/e4cd6f30eb4ffe86147b2058a12aeabc36a181ba'>Commit.</a> See bug <a href='https://bugs.kde.org/410637'>#410637</a>. Phabricator Code review <a href='https://phabricator.kde.org/D22961'>D22961</a>

### <a name='plasma-sdk' href='https://commits.kde.org/plasma-sdk'>Plasma SDK</a>

- [Cuttlefish] Use only a single AppStream ID. <a href='https://commits.kde.org/plasma-sdk/06912303ea4bfbf5625e00a5b03f4fd72415b8b7'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/411112'>#411112</a>. Phabricator Code review <a href='https://phabricator.kde.org/D23301'>D23301</a>

### <a name='plasma-workspace' href='https://commits.kde.org/plasma-workspace'>Plasma Workspace</a>

- [Notifications] Elide application label on the left. <a href='https://commits.kde.org/plasma-workspace/6a0df006ec4992f8e29d5e168ab1fcea3293f9d9'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D23690'>D23690</a>
- [Notifications] Group only same origin and show it in heading. <a href='https://commits.kde.org/plasma-workspace/ee787241bfff581c851777340c1afdc0e46f7812'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D23583'>D23583</a>
- [Notifications] Implement JobViewV3. <a href='https://commits.kde.org/plasma-workspace/6c728dd175492b5fa0d9d66a90ca52d1c46327a9'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D23293'>D23293</a>
- Replicate Shift+Enter for action invocation for history as well. <a href='https://commits.kde.org/plasma-workspace/6f0d0a22366b9bd007c8d8f1d71f5bddc3e9a98b'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/411350'>#411350</a>. Phabricator Code review <a href='https://phabricator.kde.org/D23494'>D23494</a>
- Libtaskmanager: fix launch url for startup info with pathless desktop file. <a href='https://commits.kde.org/plasma-workspace/4f309328ee00eac1d4c58340138881058603f8bd'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D23417'>D23417</a>
- [weather][envcan] Add additional current condition icon mappings. <a href='https://commits.kde.org/plasma-workspace/3b61c8c689fe2e656e25f927d956a6d8c558b836'>Commit.</a>
- [weather][envcan] Thunderstorm is a current condition, the XML also seems to have whitespaces so strip those off. <a href='https://commits.kde.org/plasma-workspace/9b3c4d1986f37543b9cb8b826715ac68bd17a288'>Commit.</a>
- Tell AppStream generators not to index Klipper .desktop files. <a href='https://commits.kde.org/plasma-workspace/d753fa01679dea32ee8526585651be62a4b83621'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D23320'>D23320</a>
- [Notifications] Bring back dismissed progress in case of crash. <a href='https://commits.kde.org/plasma-workspace/5806f4969604117f7bc70e660f659bcd3bb043ab'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D23294'>D23294</a>
- Add missing include (QJsonArray) to fix compilation with Qt 5.13. <a href='https://commits.kde.org/plasma-workspace/eaa5e74c04c56d035cfb307fc292f585ad530275'>Commit.</a>
- [lockscreen] Fit album art to defined geometry. <a href='https://commits.kde.org/plasma-workspace/953f72e42f6c30ff951570ca53bc3c7a859aa5bc'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D23063'>D23063</a>
- Fix compilation. <a href='https://commits.kde.org/plasma-workspace/1fcbf707b41dcc7048c9091135d88edd2b5bbf41'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D22976'>D22976</a>
- Add missing include. <a href='https://commits.kde.org/plasma-workspace/e34389839a815b3f29ccff1fbbbafa7221453051'>Commit.</a>
- [notifications] Change default icon to notification-inactive. <a href='https://commits.kde.org/plasma-workspace/4c11f4cd2e9208f6e0409d5abf64671f1b7aa49a'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D23033'>D23033</a>
- Players not compliant to MPRIS specification will no longer work with Media Controller applet. <a href='https://commits.kde.org/plasma-workspace/8eec2cfd36c4ecf9e2b13f4226ed49495271a1e2'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D22596'>D22596</a>
- [applets/notifications] Try to not have the notification count numbers overflow the icon. <a href='https://commits.kde.org/plasma-workspace/cd027ce844edbfd248e10cb5a5d5552ca1405366'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D22878'>D22878</a>
- [Notifications] Workaround NativeRendering with non-integer scaling. <a href='https://commits.kde.org/plasma-workspace/e3035bbcebbfa715dafa0cd262af3d21dcf3d7c8'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D22858'>D22858</a>
- Bind SQL parameters in firefox bookmarksrunner support. <a href='https://commits.kde.org/plasma-workspace/7fdc614f4226cb5075b61d4a76a9204a71374838'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D22623'>D22623</a>

### <a name='systemsettings' href='https://commits.kde.org/systemsettings'>System Settings</a>

- Exclude other desktop file from AppStream metadata generation. <a href='https://commits.kde.org/systemsettings/7f8d58958df9d56a5937b15d6b1c83d57c0ac09e'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D23328'>D23328</a>
- Use non-deprecated .metainfo.xml filename extension. <a href='https://commits.kde.org/systemsettings/16a43f64532b021ebbe043dbf9e164d3f901ee8e'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D23325'>D23325</a>
- Fix XML syntax. <a href='https://commits.kde.org/systemsettings/b01fc96fb9326f72e16f2a5a2da1363dc9167569'>Commit.</a>
- Add AppStream metadata file. <a href='https://commits.kde.org/systemsettings/b249a3d4e76e58e8ce7c1396b9f62783d4bc5ac5'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D23306'>D23306</a>