---
aliases:
- ../../fulllog_releases-22.08.1
title: KDE Gear 22.08.1 Full Log Page
type: fulllog
gear: true
hidden: true
---
{{< details id="akonadi" title="akonadi" link="https://commits.kde.org/akonadi" >}}
+ Fix Bug 458202: Search for messages in kmail does not work anymore... no results at all. [Commit.](http://commits.kde.org/akonadi/2acf92c81c4317950760e22c4fc1c5e216de6012) Fixes bug [#458202](https://bugs.kde.org/458202). Fixes bug [#458245](https://bugs.kde.org/458245)
{{< /details >}}
{{< details id="akonadiconsole" title="akonadiconsole" link="https://commits.kde.org/akonadiconsole" >}}
+ Add clear button. [Commit.](http://commits.kde.org/akonadiconsole/9784c64096c8eccf19c2324a25cc1588f0528d54) 
{{< /details >}}
{{< details id="ark" title="ark" link="https://commits.kde.org/ark" >}}
+ Don't set permissions of extracted zip files when there are none. [Commit.](http://commits.kde.org/ark/64650246a64569832ca9edfd2bb7c0f39814a8a6) 
{{< /details >}}
{{< details id="bomber" title="bomber" link="https://commits.kde.org/bomber" >}}
+ Fix 32px icon. [Commit.](http://commits.kde.org/bomber/363c46564c15997b0066ac3d737b6adc958e6d1e) 
{{< /details >}}
{{< details id="cervisia" title="cervisia" link="https://commits.kde.org/cervisia" >}}
+ Drop redundant QCommandLineParser calls. [Commit.](http://commits.kde.org/cervisia/3878724f31af329b81de6b07ea3dc3b5ceda7280) 
{{< /details >}}
{{< details id="dolphin" title="dolphin" link="https://commits.kde.org/dolphin" >}}
+ Don't show error message on ERR_USER_CANCELED. [Commit.](http://commits.kde.org/dolphin/573fc54be3aae345bc5925ac5f31f1550d93a683) 
{{< /details >}}
{{< details id="elisa" title="elisa" link="https://commits.kde.org/elisa" >}}
+ Also consider the .jpeg file extension for cover art. [Commit.](http://commits.kde.org/elisa/f71454f3e9802c3a3fa1bc38700767358f4e7714) Fixes bug [#458526](https://bugs.kde.org/458526)
+ Remove obsolete workaround for QML combobox width bug. [Commit.](http://commits.kde.org/elisa/24fd3ec47ae2794e6c2139b77cf127e0c3e98cf5) Fixes bug [#458215](https://bugs.kde.org/458215)
+ Fix condition for Breeze style. [Commit.](http://commits.kde.org/elisa/07e35a1ae99b7a95b6893e10239c420650e7c187) 
{{< /details >}}
{{< details id="eventviews" title="eventviews" link="https://commits.kde.org/eventviews" >}}
+ Allow drag-and-drop from the Month View. [Commit.](http://commits.kde.org/eventviews/f734e33a0c88c6462a36793ac64b4fc435a48cf0) See bug [#182867](https://bugs.kde.org/182867)
{{< /details >}}
{{< details id="filelight" title="filelight" link="https://commits.kde.org/filelight" >}}
+ Set antializing on the item itself as well. [Commit.](http://commits.kde.org/filelight/5509157bf29cd3fe09022171b4739615e051ebcb) Fixes bug [#458181](https://bugs.kde.org/458181)
+ Make sure we get palette updates. [Commit.](http://commits.kde.org/filelight/7d631935e4763d2cf8010d3f75e42dc579f05732) 
+ Explicitly set pen color before painting text. [Commit.](http://commits.kde.org/filelight/d83ba0cf2020a4ad324a0945a211ef07c25dc917) Fixes bug [#458274](https://bugs.kde.org/458274)
{{< /details >}}
{{< details id="grantleetheme" title="grantleetheme" link="https://commits.kde.org/grantleetheme" >}}
+ KF5GrantleeThemeConfig: Use QT_MAJOR_VERSION from build time. [Commit.](http://commits.kde.org/grantleetheme/bb017ba37bfddcb8e5ff02584a6dc45e6d601095) 
{{< /details >}}
{{< details id="incidenceeditor" title="incidenceeditor" link="https://commits.kde.org/incidenceeditor" >}}
+ Create new to-dos in the system time zone, not "floating". [Commit.](http://commits.kde.org/incidenceeditor/c8e44605d2768d3f5c4a9b9cfcd3d623bc88a171) Fixes bug [#457241](https://bugs.kde.org/457241)
{{< /details >}}
{{< details id="itinerary" title="itinerary" link="https://commits.kde.org/itinerary" >}}
+ Add 22.08.1 release notes. [Commit.](http://commits.kde.org/itinerary/f14cf98d1263c95ef8b55b529eab9c52868b9866) 
+ Improve width and positioning of map overlay sheets. [Commit.](http://commits.kde.org/itinerary/69084649cce962fb428cdecb1b4c21572f7fd870) Fixes bug [#457770](https://bugs.kde.org/457770)
+ Add 22.08.0 release notes. [Commit.](http://commits.kde.org/itinerary/28cfc48c162273bd6b9601b0604f09c579312468) 
{{< /details >}}
{{< details id="kalarm" title="kalarm" link="https://commits.kde.org/kalarm" >}}
+ Fix regression in 1eb97bd5a9eb87ee8c6ed9f09c0f4942764ab7cd. [Commit.](http://commits.kde.org/kalarm/6f73019965b26c61be2e3c5e561b751446ba5a4c) 
{{< /details >}}
{{< details id="kate" title="kate" link="https://commits.kde.org/kate" >}}
+ Fix code actions. [Commit.](http://commits.kde.org/kate/d754541824c6ca402c7d912299caef5012be2140) Fixes bug [#458554](https://bugs.kde.org/458554)
+ Add missing license text. [Commit.](http://commits.kde.org/kate/392e193bdee79805e3673f81d4faed731534472e) 
+ Fix crash when there are no search results. [Commit.](http://commits.kde.org/kate/6b29b6ba03b9e01b03c5af8dd71c36a62bbb3344) 
+ Don't search for KF5Activities on Windows/Mac. [Commit.](http://commits.kde.org/kate/0b7011c6a6066cc5b03ff49f22b7e8111a1a7c20) 
+ Show project tool view only on manual project open. [Commit.](http://commits.kde.org/kate/c6b57c141af5661e14bd101cc523956055862103) 
+ Move KF5DBusAddons dependency into kate/kwrite. [Commit.](http://commits.kde.org/kate/5f812612d58f8ebcab46964bf205b56bb03eb7bd) 
+ Re-add KF5TextWidgets dependency used by lib. [Commit.](http://commits.kde.org/kate/6c27dfeeca85b83b27b3761674567559def7f1c0) 
+ QuickDialog: removeEventFilters. [Commit.](http://commits.kde.org/kate/f5d5353f050435f1eb5b42c9fb30f7eab0a10338) Fixes bug [#457670](https://bugs.kde.org/457670)
+ ExternalTools: JsonFormat read input from stdin. [Commit.](http://commits.kde.org/kate/414d7002f9aeda7c57e6c3279f184e21916a9afe) Fixes bug [#455078](https://bugs.kde.org/455078)
+ ExternalTools: Dont try to saved files with no url. [Commit.](http://commits.kde.org/kate/aaa1bdbe6604e54dc453a0d91870c40771af1e28) 
{{< /details >}}
{{< details id="kdenlive" title="kdenlive" link="https://commits.kde.org/kdenlive" >}}
+ Fix crash when clip is modified by external app. [Commit.](http://commits.kde.org/kdenlive/edea6a279b07c058b41cf20eadeb5b8fcddb37cb) 
+ Fix paste clip broken until close/repoen app if trying to paste an invalid clip (missing of playlist with different fps). [Commit.](http://commits.kde.org/kdenlive/6c52d311559b033fa47600a56280d0f911d6323b) 
+ Fix double clicking mixed clip start corrupting mix. [Commit.](http://commits.kde.org/kdenlive/fd5b4436e85f0ac307baa49a6bd9941c277f43b3) 
+ Fix incorrect mutex unlock in thumbs cache. [Commit.](http://commits.kde.org/kdenlive/43cba9cc57144e331206aae85b218e27b24bf665) 
+ Ensure tasks are properly terminated on close, fix incorrect mutex in thumbnailcache causing corruption. [Commit.](http://commits.kde.org/kdenlive/bb8e6ffef87bbbdaf1808a07d35675a174c296c3) 
+ Ensure queued tasks are not started on project or test close. [Commit.](http://commits.kde.org/kdenlive/3db8a140c41774ff5045ed7ef70e9df563447fbf) 
+ Don't remove consecutive spaces in SRT subtitles. [Commit.](http://commits.kde.org/kdenlive/6e6d06dd2d010ea88160783bc89f751c25a868cd) Fixes bug [#457878](https://bugs.kde.org/457878)
+ Fix archiving when a clip is added twice in a project. [Commit.](http://commits.kde.org/kdenlive/234358f5c19d9b9bcb768e8e99ff039f91ba86cb) 
+ [Mix Stack] Fix wrongly reversed position slider. [Commit.](http://commits.kde.org/kdenlive/0b650bcf069d1b08b9e3d8a270163e63ba15b231) 
{{< /details >}}
{{< details id="kdepim-addons" title="kdepim-addons" link="https://commits.kde.org/kdepim-addons" >}}
+ Avoid to call refresh list each time that we open composer. [Commit.](http://commits.kde.org/kdepim-addons/46411fe454b97ab2b1b06558c5f2b193ca93f89c) 
+ Add comment. [Commit.](http://commits.kde.org/kdepim-addons/7796678e8602595435ce42507ce1b105664faf9f) 
+ Fix bug 458635 :  Language Tool set to "français", in the composer LT is set to "anglais". [Commit.](http://commits.kde.org/kdepim-addons/35dfdc8846cd6758db2c866542edea35d8581847) Fixes bug [#458635](https://bugs.kde.org/458635)
+ Fix icon size. [Commit.](http://commits.kde.org/kdepim-addons/241955907cb020538f3e2b3f6e5a3e2548a9148f) 
{{< /details >}}
{{< details id="kdevelop" title="kdevelop" link="https://commits.kde.org/kdevelop" >}}
+ Disable "News" display on starting page. [Commit.](http://commits.kde.org/kdevelop/c6665446dd8cd752fcfda1e96819718e2d8af7c8) 
{{< /details >}}
{{< details id="khangman" title="khangman" link="https://commits.kde.org/khangman" >}}
+ Put OK and Cancel buttons in a RowLyout. [Commit.](http://commits.kde.org/khangman/56240925bd631772bb232505397cf6e3bb644cf9) 
{{< /details >}}
{{< details id="kig" title="kig" link="https://commits.kde.org/kig" >}}
+ Add 3.10 to the list of python versions. [Commit.](http://commits.kde.org/kig/5f0a7ac7e71bf4491b116cebee8179e7a1006eea) 
{{< /details >}}
{{< details id="kitinerary" title="kitinerary" link="https://commits.kde.org/kitinerary" >}}
+ Support another (newer?) format variation of Amtrak PDF tickets. [Commit.](http://commits.kde.org/kitinerary/865dc0cb9d14a9a405c7f66eafc18ea970141d85) Fixes bug [#458514](https://bugs.kde.org/458514)
+ Add Vistara boarding pass extractor script. [Commit.](http://commits.kde.org/kitinerary/b90bd6ec7b950de87f535f93e47eccc12f1065c0) 
+ Handle more variants of the "Tx" terminal notation. [Commit.](http://commits.kde.org/kitinerary/12e9c8781519b14ff6926490ebd232e7aa0526d7) 
+ Fix the plausible gate logic in the pkpass extractor. [Commit.](http://commits.kde.org/kitinerary/5342768cfa759f395331e2738b39ca3ae751421a) 
+ Fix poppler variable name. [Commit.](http://commits.kde.org/kitinerary/06be6fc80c000520ad0f62ec63abbddd1a77d7a4) 
+ Add Flixbus PDF ticket extractor. [Commit.](http://commits.kde.org/kitinerary/d9a98cd4d2183622ab258423a93d270f47350cfc) 
+ Fix Flixbus ticket tokens extracted from JSON-LD in more cases. [Commit.](http://commits.kde.org/kitinerary/4d6c72cdc7f0af5d80d9ae191b70ffe0c8378b42) 
+ Perform airport terminal detection in the generic boarding pass extractor. [Commit.](http://commits.kde.org/kitinerary/295b576dac8fa1588636012766190500d98607af) Fixes bug [#457702](https://bugs.kde.org/457702)
+ Extract the airport terminal detection logic into its own class. [Commit.](http://commits.kde.org/kitinerary/4e0f446faad61d3f797b3362adc3ad4acc46de51) 
{{< /details >}}
{{< details id="kleopatra" title="kleopatra" link="https://commits.kde.org/kleopatra" >}}
+ Fix building against QGpgME 1.18.0. [Commit.](http://commits.kde.org/kleopatra/a3684049dd4f0d2d47dcc6605ff31ce753254ef9) Fixes bug [#458044](https://bugs.kde.org/458044)
{{< /details >}}
{{< details id="kmail" title="kmail" link="https://commits.kde.org/kmail" >}}
+ Fix i18n. [Commit.](http://commits.kde.org/kmail/50114d7bea8d52f69165aff8dcb817ab748807d6) 
+ Fix logic. We will not add item to list + remove it in some case. [Commit.](http://commits.kde.org/kmail/46ef64c323d379ccbbdd19cf25df99a7886cf9de) See bug [#458618](https://bugs.kde.org/458618)
{{< /details >}}
{{< details id="konquest" title="konquest" link="https://commits.kde.org/konquest" >}}
+ Fix text color potentially not having enough contrast with background. [Commit.](http://commits.kde.org/konquest/32f9105616250e173b21e038a100612ccb700a44) Fixes bug [#458482](https://bugs.kde.org/458482)
{{< /details >}}
{{< details id="konsole" title="konsole" link="https://commits.kde.org/konsole" >}}
+ Move shell argument manipulation to session. [Commit.](http://commits.kde.org/konsole/73b219879602a8ad0d61ce88aaf4f82cc33197b7) 
+ Prevent _replModeStart line number from becoming negative. [Commit.](http://commits.kde.org/konsole/97171b59e694893b61a1f70c2998d6967070187a) Fixes bug [#457920](https://bugs.kde.org/457920). Fixes bug [#458015](https://bugs.kde.org/458015)
+ Initialize sixel mode even when missing Raster attributes Ph and Pv. [Commit.](http://commits.kde.org/konsole/256daa374ff7b574d0e8b4865cf4212b461a1f64) 
{{< /details >}}
{{< details id="krdc" title="krdc" link="https://commits.kde.org/krdc" >}}
+ Fix scale computation when fitting framebuffer to local view. [Commit.](http://commits.kde.org/krdc/312c42507e94072ee2793825d72cde16aaa6222a) 
+ Fix access dates display. [Commit.](http://commits.kde.org/krdc/f4824d49759ba1cf1711e432ea4fe42a7359d380) Fixes bug [#458587](https://bugs.kde.org/458587)
+ Use QWidget::screen instead of screenAt. [Commit.](http://commits.kde.org/krdc/fdec6d33003230bfca0c08e148f7253709f246cf) 
{{< /details >}}
{{< details id="kwalletmanager" title="kwalletmanager" link="https://commits.kde.org/kwalletmanager" >}}
+ Move desktop files to src/manager. [Commit.](http://commits.kde.org/kwalletmanager/2092c150aae555bb0bbd13611dda4ecfb0c40709) 
+ Add option to not build KCM & KAuth support. [Commit.](http://commits.kde.org/kwalletmanager/685bf87578877900bf6244426850baa1612f0c1b) 
{{< /details >}}
{{< details id="libkdegames" title="libkdegames" link="https://commits.kde.org/libkdegames" >}}
+ KgThemeProvider: fix generating broken full path theme id due to fs symlinks. [Commit.](http://commits.kde.org/libkdegames/91e246f2b17c899c1d5a9940d7b6f7d9893e2319) Fixes bug [#457951](https://bugs.kde.org/457951)
{{< /details >}}
{{< details id="libkleo" title="libkleo" link="https://commits.kde.org/libkleo" >}}
+ KF5LibkleoConfig: Use QT_MAJOR_VERSION from build time. [Commit.](http://commits.kde.org/libkleo/28f25d9b650b6235553519844d8c2ec6b563e605) 
{{< /details >}}
{{< details id="messagelib" title="messagelib" link="https://commits.kde.org/messagelib" >}}
+ Close dialog when we close on "X" in developper page. [Commit.](http://commits.kde.org/messagelib/eeebc0bbd9e7180d9871679a2251d2e5a862e8b8) 
+ Minor optimization. [Commit.](http://commits.kde.org/messagelib/e010edcf5d1bdfb7131c8c2cbc2ed68106b176d1) 
+ Start to implement ed25519 support for dkim. [Commit.](http://commits.kde.org/messagelib/3ac94d38c9851c62174f80070430dcc44f69bd91) 
{{< /details >}}
{{< details id="okular" title="okular" link="https://commits.kde.org/okular" >}}
+ Fix saving files on Windows. [Commit.](http://commits.kde.org/okular/8e93995e3caa49bcd7c2c862a66f959160194fda) Fixes bug [#458574](https://bugs.kde.org/458574)
+ ToggleActions: Obey "don't show text" for a particular action toolbar setting. [Commit.](http://commits.kde.org/okular/3304038d1de0bead5e67275a7a6a9885afe0332d) Fixes bug [#457322](https://bugs.kde.org/457322)
+ Even more tweaks to opening "text" files. [Commit.](http://commits.kde.org/okular/01871fdfff9363af3a90959650ef20ab9276c464) Fixes bug [#430538](https://bugs.kde.org/430538). Fixes bug [#456434](https://bugs.kde.org/456434)
+ Epub: Improve TableOfContents for some files. [Commit.](http://commits.kde.org/okular/656587ca6393663c8d652a63df7d8393b4adaac7) Fixes bug [#458289](https://bugs.kde.org/458289)
{{< /details >}}
{{< details id="palapeli" title="palapeli" link="https://commits.kde.org/palapeli" >}}
+ Delay quitting import until notification was shown. [Commit.](http://commits.kde.org/palapeli/22bb5fdb78f259c04cf9e19f0ce0fb683584f656) 
+ Fix importing file. [Commit.](http://commits.kde.org/palapeli/4d412cc3cbb7f980f380d24cb190ab473c24d7e0) 
+ Fix opening file from arguments. [Commit.](http://commits.kde.org/palapeli/2e966ae9b31120b27bae4330e0a72ae7d16c8ab6) 
{{< /details >}}
{{< details id="umbrello" title="umbrello" link="https://commits.kde.org/umbrello" >}}
+ Unittests/testbase.cpp followup to commit e4d79cfe : In function initTestCase call UMLApp::setup() on `app`. [Commit.](http://commits.kde.org/umbrello/2b725d04809920c87862cd65b2b0502f568b29fe) 
{{< /details >}}
{{< details id="yakuake" title="yakuake" link="https://commits.kde.org/yakuake" >}}
+ Fix highlight not disappearing when focussing terminal. [Commit.](http://commits.kde.org/yakuake/f12226e08780f983ef5964678c8047f27ac605b4) Fixes bug [#441013](https://bugs.kde.org/441013)
{{< /details >}}
