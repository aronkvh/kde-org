------------------------------------------------------------------------
r1016725 | aseigo | 2009-08-28 17:29:16 +0000 (Fri, 28 Aug 2009) | 3 lines

only reset the sizes when we have no tabs
BUG:205457

------------------------------------------------------------------------
r1016813 | mart | 2009-08-29 00:07:22 +0000 (Sat, 29 Aug 2009) | 2 lines

backport possible fix to 201045

------------------------------------------------------------------------
r1016830 | aseigo | 2009-08-29 00:34:22 +0000 (Sat, 29 Aug 2009) | 3 lines

prevent duplicate actions in the toolbox
CCBUG:200981

------------------------------------------------------------------------
r1016960 | mart | 2009-08-29 13:03:47 +0000 (Sat, 29 Aug 2009) | 2 lines

backport pixmap fix

------------------------------------------------------------------------
r1016999 | mart | 2009-08-29 14:26:15 +0000 (Sat, 29 Aug 2009) | 2 lines

compile++

------------------------------------------------------------------------
r1017044 | grossard | 2009-08-29 16:32:06 +0000 (Sat, 29 Aug 2009) | 2 lines

added a translator's entity

------------------------------------------------------------------------
r1018143 | aseigo | 2009-09-01 11:06:05 +0000 (Tue, 01 Sep 2009) | 3 lines

don't repaint the entire widget when we're setting the same content
CCBUG:204491

------------------------------------------------------------------------
r1018178 | ruphy | 2009-09-01 12:04:51 +0000 (Tue, 01 Sep 2009) | 3 lines

fix a huge leak :-) (backport)

Signed-off-by: Riccardo Iaconelli <riccardo@kde.org>
------------------------------------------------------------------------
r1018463 | dfaure | 2009-09-01 18:11:55 +0000 (Tue, 01 Sep 2009) | 5 lines

I don't think we want to hide errors like
  uic: Error in line 88, column 21 : Unexpected element customwidgets
It makes the debugging of .ui syntax errors pretty difficult.
CCMAIL: neundorf@kde.org

------------------------------------------------------------------------
r1018467 | dfaure | 2009-09-01 18:15:17 +0000 (Tue, 01 Sep 2009) | 3 lines

Don't generate a file with just two includes in them, if uic failed.
This way typing "make" again, shows the real error from uic, rather than the "class not found"...

------------------------------------------------------------------------
r1018481 | mrybczyn | 2009-09-01 18:35:41 +0000 (Tue, 01 Sep 2009) | 2 lines

Corrections for systemsettings

------------------------------------------------------------------------
r1018490 | dfaure | 2009-09-01 18:49:53 +0000 (Tue, 01 Sep 2009) | 2 lines

backport syntax fix

------------------------------------------------------------------------
r1018521 | mrybczyn | 2009-09-01 19:32:08 +0000 (Tue, 01 Sep 2009) | 2 lines

Entities for Plasma

------------------------------------------------------------------------
r1018526 | mrybczyn | 2009-09-01 19:38:53 +0000 (Tue, 01 Sep 2009) | 2 lines

Minor correction

------------------------------------------------------------------------
r1018532 | beatwolf | 2009-09-01 19:50:23 +0000 (Tue, 01 Sep 2009) | 2 lines

fix double delete bug

------------------------------------------------------------------------
r1018542 | mrybczyn | 2009-09-01 20:09:21 +0000 (Tue, 01 Sep 2009) | 2 lines

kcontrolcenter entity

------------------------------------------------------------------------
r1018605 | lunakl | 2009-09-01 22:43:27 +0000 (Tue, 01 Sep 2009) | 4 lines

the configuration UI for noproxy says that URLs are allowed, so actually
allow them


------------------------------------------------------------------------
r1018611 | lunakl | 2009-09-01 22:51:19 +0000 (Tue, 01 Sep 2009) | 3 lines

fix last commit


------------------------------------------------------------------------
r1019298 | mart | 2009-09-03 10:01:26 +0000 (Thu, 03 Sep 2009) | 2 lines

backport fix to 205879

------------------------------------------------------------------------
r1019499 | mart | 2009-09-03 18:48:40 +0000 (Thu, 03 Sep 2009) | 2 lines

backport of fix of bug 194570

------------------------------------------------------------------------
r1019508 | adawit | 2009-09-03 18:55:20 +0000 (Thu, 03 Sep 2009) | 1 line

Backport r1018292 from trunk.
------------------------------------------------------------------------
r1019620 | scripty | 2009-09-04 03:27:20 +0000 (Fri, 04 Sep 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1020226 | annma | 2009-09-05 17:30:14 +0000 (Sat, 05 Sep 2009) | 3 lines

backport richtext fix. Thanks to Maksim that should not make strings fuzzy.
CCMAIL=kde-i18n-doc@kde.org

------------------------------------------------------------------------
r1020459 | mkoller | 2009-09-06 11:29:42 +0000 (Sun, 06 Sep 2009) | 7 lines

Backport r1016875 by mkoller from trunk to the 4.3 branch:

CCBUG: 184273

Stop the internal scrolling timer when scrolling via the scrollBy() API method


------------------------------------------------------------------------
r1020584 | neundorf | 2009-09-06 17:12:54 +0000 (Sun, 06 Sep 2009) | 5 lines

-document that since KDE 4.2 RUN_UNINSTALLED is not necessary anymore and has no effect

Alex


------------------------------------------------------------------------
r1020622 | annma | 2009-09-06 19:27:48 +0000 (Sun, 06 Sep 2009) | 6 lines

Sorry about the error.
This fixes some richtext tags being shown in QWhatsThis.
This will make a few strings fuzzy.
CCMAIL=kde-i18n-doc@kde.org


------------------------------------------------------------------------
r1020701 | alvarenga | 2009-09-07 05:15:56 +0000 (Mon, 07 Sep 2009) | 1 line

[KDE-pt_BR]
------------------------------------------------------------------------
r1020853 | mart | 2009-09-07 11:25:29 +0000 (Mon, 07 Sep 2009) | 2 lines

backport config overlay look fox

------------------------------------------------------------------------
r1020859 | dfaure | 2009-09-07 11:38:44 +0000 (Mon, 07 Sep 2009) | 6 lines

What's wrong with this code?
  Q_FOREACH(sub_entry, e->m_entries)
    if (sub_entry->path == e->path + '/' + path) break;
Answer: if not found, sub_entry will point to the last entry in the list.
This made kdirwatch remove+re-add the last entry when an unrelated notification happened.

------------------------------------------------------------------------
r1020866 | mart | 2009-09-07 11:59:58 +0000 (Mon, 07 Sep 2009) | 2 lines

backport of the config overlay placement in the popup

------------------------------------------------------------------------
r1020884 | dfaure | 2009-09-07 13:22:53 +0000 (Mon, 07 Sep 2009) | 5 lines

Backport r1020883
Add support for konqueror haters :)
 (I mean, for people who choose another browser *and* don't even install kdebase-apps)
(BUG 206581)

------------------------------------------------------------------------
r1020919 | ogoffart | 2009-09-07 15:28:09 +0000 (Mon, 07 Sep 2009) | 13 lines

Backport 1020918

Optimize the drawning of blur text.

Drawing the blur text takes 65% of the time while changing desktop on my desktop
(40 opened windows)

QImage::width() and QImage::height() are not inline, do not call them on a tight
loop or they consume 10% of the time.

Reviewed by Alexis    


------------------------------------------------------------------------
r1021385 | scripty | 2009-09-09 03:35:53 +0000 (Wed, 09 Sep 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1022110 | mart | 2009-09-10 20:24:57 +0000 (Thu, 10 Sep 2009) | 2 lines

backport the actual toolbox close

------------------------------------------------------------------------
r1022112 | mart | 2009-09-10 20:33:29 +0000 (Thu, 10 Sep 2009) | 2 lines

toggle toolbox only if visible

------------------------------------------------------------------------
r1022113 | dfaure | 2009-09-10 20:38:15 +0000 (Thu, 10 Sep 2009) | 2 lines

Committing on behalf of Maskim: comment out assert which can happen with e.g. getPropertyCSSValue: Unhandled property: "filter"

------------------------------------------------------------------------
r1022120 | ppenz | 2009-09-10 20:47:34 +0000 (Thu, 10 Sep 2009) | 8 lines

Backport of SVN commit 1022119: When KFilePreviewGenerator::Private::killPreviewJobs() is invoked, all timers
must get stopped to. Otherwise it is possible that a timer like m_changedItemsTimer will invoke orderItems() on an already invalid URL which
will result in a crash.

Thanks a lot to Frank Reininghaus for the analyses and Dario Andres & FiNeX for taking care to recognize the huge number of duplicates.

CCBUG: 200125

------------------------------------------------------------------------
r1022533 | zwabel | 2009-09-11 23:45:08 +0000 (Fri, 11 Sep 2009) | 3 lines

Backport r1022512:
Lock the smart-mutex before creating the smart-range. This should prevent a possible crash.

------------------------------------------------------------------------
r1022951 | mkoller | 2009-09-13 16:07:23 +0000 (Sun, 13 Sep 2009) | 8 lines

Backport r1022949 by mkoller from trunk to the 4.3 branch:

CCBUG: 185321

make sure that a timeout on connect returns the correct errorString
instead of "Unknown Error"


------------------------------------------------------------------------
r1023086 | mrybczyn | 2009-09-13 21:12:20 +0000 (Sun, 13 Sep 2009) | 2 lines

New entries for kmenuedit

------------------------------------------------------------------------
r1023243 | dfaure | 2009-09-14 10:20:22 +0000 (Mon, 14 Sep 2009) | 2 lines

Add unittest for <DefineGroup> in part merging.

------------------------------------------------------------------------
r1023249 | dfaure | 2009-09-14 10:31:10 +0000 (Mon, 14 Sep 2009) | 2 lines

"what happens then when the host doesn't provide a group with that name"

------------------------------------------------------------------------
r1023464 | mleupold | 2009-09-14 19:17:58 +0000 (Mon, 14 Sep 2009) | 2 lines

Backport of r1021489:
Fix cancelling KPasswdServer requests. Thanks to David Faure for the patch.
------------------------------------------------------------------------
r1023473 | mkoller | 2009-09-14 19:48:40 +0000 (Mon, 14 Sep 2009) | 7 lines

Backport r1023472 by mkoller from trunk to the 4.3 branch:

CCBUG: 95628

Hide the current tooltip when showing a new page


------------------------------------------------------------------------
r1023550 | dhaumann | 2009-09-14 22:20:21 +0000 (Mon, 14 Sep 2009) | 1 line

backport r1023071: Fix broken pointer in KateCmdLineEdit: parentWidget() != KateView...
------------------------------------------------------------------------
r1023688 | dfaure | 2009-09-15 09:03:58 +0000 (Tue, 15 Sep 2009) | 2 lines

Backport r1023683, fix for .desktop files in BrowserApplication

------------------------------------------------------------------------
r1023701 | dfaure | 2009-09-15 09:23:54 +0000 (Tue, 15 Sep 2009) | 2 lines

Remove wrong line, no %u wanted/needed.

------------------------------------------------------------------------
r1023728 | ilic | 2009-09-15 09:58:45 +0000 (Tue, 15 Sep 2009) | 1 line

i18n fix: proper output of version string (bport: 1023725).
------------------------------------------------------------------------
r1023830 | dfaure | 2009-09-15 11:29:06 +0000 (Tue, 15 Sep 2009) | 6 lines

In case of "disk full", return an error instead of removing the existing config file.
Testcase:
 dd if=/dev/zero of=image count=200 ; mkfs.ext2 image ; mkdir mntpt ; sudo mount image mntpt -o loop -t ext2
 cd mntpt ; sudo chown -R $UID . ; dd if=/dev/zero of=make_it_full
 kwriteconfig --file $PWD/outfile --key MyKey TheValue2

------------------------------------------------------------------------
r1023881 | pino | 2009-09-15 13:06:22 +0000 (Tue, 15 Sep 2009) | 6 lines

Backport SVN commit 1006058 by pino:

Activate the reentrancy features for lex/yacc, and the bison bridge for flex.
This, together with the use of a scanner object, allows the resulting ktrader parser to be reentrant, thus to be used safely in multithreading contexts.
CCBUG: 192536

------------------------------------------------------------------------
r1023884 | pino | 2009-09-15 13:12:06 +0000 (Tue, 15 Sep 2009) | 6 lines

backport SVN commit 1006063 by pino:

ktrader parser invocation: instead of using a static variable for current resulting tree and data being parsed, put them in a thread local storage
this avoids them being mutually rewritten by racing threads, and the result should guarantee reentrancy
CCBUG: 192536

------------------------------------------------------------------------
r1024387 | trueg | 2009-09-16 14:00:07 +0000 (Wed, 16 Sep 2009) | 1 line

Backport: Add the type of the resource and default to rdfs:Resource if no type is given
------------------------------------------------------------------------
r1024413 | pino | 2009-09-16 15:18:00 +0000 (Wed, 16 Sep 2009) | 3 lines

remove the application/x-openraster
Cyrille says it is old and not used, so just kill it

------------------------------------------------------------------------
r1024436 | scripty | 2009-09-16 16:06:59 +0000 (Wed, 16 Sep 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1024519 | ecuadra | 2009-09-16 18:48:10 +0000 (Wed, 16 Sep 2009) | 1 line

SVN_SILENT Fix some typos in entities for spanish language
------------------------------------------------------------------------
r1024892 | gkiagia | 2009-09-17 13:41:25 +0000 (Thu, 17 Sep 2009) | 6 lines

Backport of r1024876.
Logout from utmp when the KPtyProcess finishes.
This fixes the bug where konsole and kwrited leave
invalid entries in utmp after they have quit.
Reviewed by ossi.

------------------------------------------------------------------------
r1025059 | dfaure | 2009-09-17 22:26:43 +0000 (Thu, 17 Sep 2009) | 2 lines

Remove dead KICONLOADER_CHECKS code, wasn't used anymore, just slowing things down.

------------------------------------------------------------------------
r1025126 | scripty | 2009-09-18 03:06:57 +0000 (Fri, 18 Sep 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1025416 | dfaure | 2009-09-18 17:37:59 +0000 (Fri, 18 Sep 2009) | 2 lines

hmm, seems I still have -pedantic set.

------------------------------------------------------------------------
r1025758 | mdawson | 2009-09-19 15:57:00 +0000 (Sat, 19 Sep 2009) | 3 lines

Fix kio_http to be able to authenticate against a http server that returns no body with the 401 error code.
This patch fixes KDE's ability to talk to eGroupware using groupdav.

------------------------------------------------------------------------
r1025761 | mart | 2009-09-19 16:00:06 +0000 (Sat, 19 Sep 2009) | 2 lines

backport buttin icon alignment

------------------------------------------------------------------------
r1025797 | orlovich | 2009-09-19 17:59:57 +0000 (Sat, 19 Sep 2009) | 5 lines

Fix crash in runAdFilter --- take in account that when we're removing a node its kids 
will be removed as well...

BUG: 192308

------------------------------------------------------------------------
r1025800 | orlovich | 2009-09-19 18:22:14 +0000 (Sat, 19 Sep 2009) | 2 lines

Fix show()/hide()

------------------------------------------------------------------------
r1025823 | orlovich | 2009-09-19 19:56:58 +0000 (Sat, 19 Sep 2009) | 3 lines

Quieter...


------------------------------------------------------------------------
r1025829 | orlovich | 2009-09-19 20:12:42 +0000 (Sat, 19 Sep 2009) | 5 lines

Handle namespaced attributes when generating contexts;
and more modular namespace handling in general.
(Now if I only remembered what OpenSolaris compile fix 
made me find this issue..)

------------------------------------------------------------------------
r1025861 | orlovich | 2009-09-19 22:20:51 +0000 (Sat, 19 Sep 2009) | 6 lines

Make SVG text drawing a bit more reliable:
1. Remove some non-working layout optimizations that made the text boxes
not get computed right
2. In the simple case, set up the pen propertly.
actually, in the non-simple case, we shouldn't be using drawText at all...

------------------------------------------------------------------------
r1026027 | sengels | 2009-09-20 14:36:29 +0000 (Sun, 20 Sep 2009) | 2 lines

backport r1026019
CCBUG:207826
------------------------------------------------------------------------
r1026751 | mart | 2009-09-22 13:54:58 +0000 (Tue, 22 Sep 2009) | 2 lines

backport the fix of a off by one derived crash

------------------------------------------------------------------------
r1026754 | mart | 2009-09-22 14:05:18 +0000 (Tue, 22 Sep 2009) | 2 lines

backport current index update when inserting tabs

------------------------------------------------------------------------
r1027071 | lueck | 2009-09-23 09:55:18 +0000 (Wed, 23 Sep 2009) | 1 line

add missing i18n call to make it translated
------------------------------------------------------------------------
r1027439 | trueg | 2009-09-24 08:32:41 +0000 (Thu, 24 Sep 2009) | 1 line

backported api doc improvements
------------------------------------------------------------------------
r1027445 | trueg | 2009-09-24 08:34:14 +0000 (Thu, 24 Sep 2009) | 1 line

backport: do check for the existence of the nepomuk storage service before connecting to it
------------------------------------------------------------------------
r1027646 | teve | 2009-09-24 12:43:35 +0000 (Thu, 24 Sep 2009) | 3 lines

Closing old bug, fix will be in KDE 4.3.2 
BUG:103424

------------------------------------------------------------------------
r1027715 | adawit | 2009-09-24 15:25:31 +0000 (Thu, 24 Sep 2009) | 4 lines

Send the correct 'Content-Type' header when submitting forms.

BUG:207438

------------------------------------------------------------------------
r1027745 | dfaure | 2009-09-24 16:48:23 +0000 (Thu, 24 Sep 2009) | 2 lines

crash less

------------------------------------------------------------------------
r1027765 | mart | 2009-09-24 18:01:09 +0000 (Thu, 24 Sep 2009) | 2 lines

backport of crash fix when setting an index < 0

------------------------------------------------------------------------
r1027768 | mart | 2009-09-24 18:27:46 +0000 (Thu, 24 Sep 2009) | 2 lines

backport of the real fix to the issue addressed by the previous commit

------------------------------------------------------------------------
r1027781 | mart | 2009-09-24 18:53:37 +0000 (Thu, 24 Sep 2009) | 2 lines

another null pointer check

------------------------------------------------------------------------
r1027857 | adawit | 2009-09-24 23:08:46 +0000 (Thu, 24 Sep 2009) | 4 lines

- Added attribute enumerations to make it easier to identify KDE/KIO specific ones.
- Propagate error codes that cannot mapped into QNetworkReply::NetworkError through 
  QNetworkRequest's attribute system.

------------------------------------------------------------------------
r1027871 | dhaumann | 2009-09-25 00:01:26 +0000 (Fri, 25 Sep 2009) | 6 lines

backport SVN commit 1027817 by pletourn:

Fix line numbers drawing

CCBUG:179018

------------------------------------------------------------------------
r1028166 | scripty | 2009-09-26 03:10:22 +0000 (Sat, 26 Sep 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1028243 | ecuadra | 2009-09-26 11:19:20 +0000 (Sat, 26 Sep 2009) | 2 lines

SVN_SILENT Update entities for Spanish language
CC_MAIL: jaime@robles.es
------------------------------------------------------------------------
r1028402 | orlovich | 2009-09-27 00:13:24 +0000 (Sun, 27 Sep 2009) | 12 lines

Merged revision:r1028401 | orlovich | 2009-09-26 20:12:19 -0400 (Sat, 26 Sep 2009) | 11 lines

Handle reentry of setFocusNode via blur event.
Fixes crashes on bahn.de
BUG:201159
BUG:207952
BUG:201208
BUG:204050
BUG:195710
BUG:177689
BUG:205967
BUG:186539
------------------------------------------------------------------------
r1028403 | adawit | 2009-09-27 00:13:24 +0000 (Sun, 27 Sep 2009) | 1 line

Disable SSL warnings when leaving ssl sites as result user entering new address or selecting a bookmarked one
------------------------------------------------------------------------
r1028619 | lueck | 2009-09-27 18:48:05 +0000 (Sun, 27 Sep 2009) | 1 line

enable the localised stylesheets for translations
------------------------------------------------------------------------
r1028823 | trueg | 2009-09-28 09:41:04 +0000 (Mon, 28 Sep 2009) | 1 line

Backport: fixed handling of local file paths and symlinks
------------------------------------------------------------------------
r1028915 | sengels | 2009-09-28 13:15:40 +0000 (Mon, 28 Sep 2009) | 1 line

fix linker error
------------------------------------------------------------------------
r1028997 | rkcosta | 2009-09-28 18:21:43 +0000 (Mon, 28 Sep 2009) | 15 lines

Backport r1028996.

Commit patch by Sverre Froyen and submitted to the kde-freebsd mailing list by Douglas Berry.

According to Sverre, he was unable to connect to his Yahoo! IM account with Kopete on KDE4 because he always got a "There was an error while connecting <user name> to the Yahoo server. Error message: 17 remote host closed connection" error message.

Patch ack'ed by Thiago Macieira, although even he doesn't remember the code very well anymore. So let's hope for the best!

Relevant discussions:

 * http://lists.kde.org/?l=kopete-devel&m=125349905124616&w=2
 * http://mail.kde.org/pipermail/kde-freebsd/2009-September/006583.html

CCMAIL: sverre@viewmark.com

------------------------------------------------------------------------
r1029138 | bks | 2009-09-29 03:56:02 +0000 (Tue, 29 Sep 2009) | 2 lines

backport r1023999 so that I can run 4.3.x with Qt 4.6 on 16-bit displays

------------------------------------------------------------------------
r1029209 | dfaure | 2009-09-29 09:27:19 +0000 (Tue, 29 Sep 2009) | 3 lines

Backport: Don't crash if dataChanged(root, root) is emitted, e.g. when changing permissions for the current dir.
BUG: 193466

------------------------------------------------------------------------
r1029629 | jlayt | 2009-09-30 12:19:50 +0000 (Wed, 30 Sep 2009) | 7 lines

Fix CUPS detection routine, only add CUPS tabs to print dialog if CUPS service
is on port 631.

This code is copied form Okular FilePrinter class where it has been working
since 4.2, I just forgot to do it here as well. A little simplistic, but
better than just looking for config files.

------------------------------------------------------------------------
r1029735 | vtokarev | 2009-09-30 16:16:43 +0000 (Wed, 30 Sep 2009) | 1 line

backport r1029732
------------------------------------------------------------------------
r1029754 | orlovich | 2009-09-30 17:03:04 +0000 (Wed, 30 Sep 2009) | 7 lines

Don't set m_render before computing style for HTMLFrameSetElementImpl
as that can crash if we need to determine isContentEditable.
Also fixup missing && childAllowed() I noticed while looking at this.

BUG:206038
BUG:202682

------------------------------------------------------------------------
r1029833 | orlovich | 2009-09-30 20:58:03 +0000 (Wed, 30 Sep 2009) | 13 lines

Merged revision:r1029831 | orlovich | 2009-09-30 16:39:30 -0400 (Wed, 30 Sep 2009) | 13 lines

Prevent double-dispatch of events file upload lineedit does not accept.
(One directly to lineedit, one via DOM on the containing upload widget).

Fixes double-upload-dialogs-on-enter bug. I think in general the bubbling 
behavior of Qt when it comes to events is problematic --- we should 
probably always accept events at original level and route them manually
to avoid issues. (This requires moving top-level shortcut handling 
from KHTMLView to ElementImpl::defaultEventHandler with a check 
for being the documentElement)

BUG: 192755
------------------------------------------------------------------------
r1029879 | dfaure | 2009-09-30 23:44:02 +0000 (Wed, 30 Sep 2009) | 5 lines

Backport 1029862+1029870+1029871:
protect access to the list of catalogs (and other KLocalePrivate members) using a mutex.
Fix will be in KDE-4.3.2.
CCBUG: 208178

------------------------------------------------------------------------
r1029929 | orlovich | 2009-10-01 04:14:24 +0000 (Thu, 01 Oct 2009) | 7 lines

Merged revision:r1029928 | orlovich | 2009-10-01 00:13:37 -0400 (Thu, 01 Oct 2009) | 7 lines

Add some robustness vs. 194667/202699; should hopefully
avoid crashes, but needs further investigation

CCBUG:202699
CCBUG:194667
------------------------------------------------------------------------
r1030000 | dfaure | 2009-10-01 11:16:43 +0000 (Thu, 01 Oct 2009) | 4 lines

Add fromMimeData overload that takes a QDomDocument, otherwise we get elements with a dangling doc.
<thiago> a node/element only exists inside a document
CCBUG: 160679

------------------------------------------------------------------------
r1030139 | jacopods | 2009-10-01 17:02:43 +0000 (Thu, 01 Oct 2009) | 2 lines

Workaround the (n+1)st qt clipping bug; the bug has been reported and appears to be fixed in the 4.6 branch

------------------------------------------------------------------------
r1030167 | dfaure | 2009-10-01 19:04:07 +0000 (Thu, 01 Oct 2009) | 2 lines

Backport race-condition fix

------------------------------------------------------------------------
