2006-06-04 18:27 +0000 [r548146]  rdale

	* branches/KDE/3.5/kdebindings/qtruby/rubylib/qtruby/Qt.cpp,
	  branches/KDE/3.5/kdebindings/qtruby/ChangeLog: * For Qt::QObject
	  classes which are immediate subclasses of Qt::Base, don't add C
	  metaObject() and qt_invoke() methods as they aren't needed. This
	  means that a QMetaObject is no longer constructed for these
	  classes, and the one that the corresponding C++ class has is
	  returned instead.

2006-06-05 09:48 +0000 [r548316-548315]  rdale

	* branches/KDE/3.5/kdebindings/qtruby/rubylib/qtruby/lib/Qt/qtruby.rb,
	  branches/KDE/3.5/kdebindings/qtruby/rubylib/qtruby/Qt.cpp,
	  branches/KDE/3.5/kdebindings/qtruby/ChangeLog: * The metaObject
	  methods for slots and signals are no longer added when a
	  Qt::Object is constructed, but when slots or signals are added to
	  a class. This means that signals as well as slots can be added to
	  an existing instance.

	* branches/KDE/3.5/kdebindings/korundum/rubylib/korundum/Korundum.cpp,
	  branches/KDE/3.5/kdebindings/korundum/ChangeLog: * The metaObject
	  methods for slots and signals are no longer added when a
	  Qt::Object is constructed, but when slots or signals are added to
	  a class. This means that signals as well as slots can be added to
	  an existing instance.

2006-06-16 17:13 +0000 [r552120]  rdale

	* branches/KDE/3.5/kdebindings/qtruby/rubylib/qtruby/Qt.cpp: * Only
	  put classes starting with 'Ko' into a Ko:: module namespace if
	  they don't already contain a '::' scope operator

2006-07-12 09:46 +0000 [r561509]  rdale

	* branches/KDE/3.5/kdebindings/qtruby/rubylib/qtruby/handlers.cpp,
	  branches/KDE/3.5/kdebindings/qtruby/ChangeLog: * The Ruby String
	  to 'char *' and String to 'unsigned char *' were using the
	  pointer within the Ruby String directly which meant they were
	  deleted when the Ruby String was gc'd. So they are copied with
	  strdup () instead.

2006-07-23 14:01 +0000 [r565467]  coolo

	* branches/KDE/3.5/kdebindings/kdebindings.lsm: preparing KDE 3.5.4

