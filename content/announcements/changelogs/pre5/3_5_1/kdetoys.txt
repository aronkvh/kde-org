2005-11-29 23:38 +0000 [r484255]  mpyne

	* branches/KDE/3.5/kdetoys/amor/amor.cpp,
	  branches/KDE/3.5/kdetoys/amor/amordialog.cpp: Fix bug 117312
	  (Amor settings can't be transferred due to absolute path names)
	  in KDE 3.5. Leaving open for now because I will try to also fix
	  in /trunk. CCBUG:117312

2005-12-14 15:43 +0000 [r488461]  dfaure

	* branches/KDE/3.5/kdetoys/kweather/kweather.cpp,
	  branches/KDE/3.5/kdetoys/kweather/dockwidget.cpp: very verbose
	  output commented out

2005-12-19 06:03 +0000 [r489571]  hindenburg

	* branches/KDE/3.5/kdetoys/kworldwatch/zone.tab: Update zone.tab
	  from glibc-2.3.5 (v1.30). Brian Beck: The format of this file is
	  taken from glibc; it would be unwise IMHO to change it. BUG:
	  97192

2006-01-02 01:06 +0000 [r493275]  hindenburg

	* branches/KDE/3.5/kdetoys/kworldwatch/astro.c,
	  branches/KDE/3.5/kdetoys/kworldwatch/sunclock.c,
	  branches/KDE/3.5/kdetoys/kworldwatch/maploader.cpp,
	  branches/KDE/3.5/kdetoys/kworldwatch/sunclock.h: fix compiler
	  warnings

