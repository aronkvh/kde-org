---
date: 2024-10-10
appCount: 180
layout: gear
---
+ dolphin: Ignore trailing slashes when comparing place URLs ([Commit](http://commits.kde.org/dolphin/1d6ee22576fdb4472723a40a3fa77bcc0b8013a9))
+ kate: Fix session restore of tabs/views of untitled documents ([Commit](http://commits.kde.org/kate/be856dbb927f2c4082218bc4e0360eb677c9c44f), fixes bug [#464703](https://bugs.kde.org/464703), bug [#462112](https://bugs.kde.org/462112) and bug [#462523](https://bugs.kde.org/462523))
+ konsole: Fix a crash when sending OSC 4 (RGB) color outside the 256 range ([Commit](http://commits.kde.org/konsole/0f1738548fb7ec8e7db92a425181e3880271f43b), fixes bug [#494205](https://bugs.kde.org/494205))
