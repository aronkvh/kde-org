---
title: "Old Supporting Member System Retired"
---

Thank you very much for your support of KDE.

The supporting member program previously run on relate.kde.org has been migrated to a new system.

Due to the nature of the new system it was not possible to transfer memberships from the old program to the new system.
We encourage members to re-enroll in the <a href="https://kde.org/community/donations/">new supporting members program system</a>.

For any queries regarding payments processed by the previous system or queries on the new system please <a href="mailto:kde-ev-campaign@kde.org">contact us</a>.
