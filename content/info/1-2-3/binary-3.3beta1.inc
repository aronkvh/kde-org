<ul>

<!--   SUSE LINUX -->
<li>
  <a href="http://www.suse.com/">SuSE Linux</a>
<!--  (<a href="http://download.kde.org/binarydownload.html?url=/unstable/3.2.91/SuSE/README">README</a>) -->
  :
  <ul type="disc">
    <li>
        <a href="http://download.kde.org/binarydownload.html?url=/unstable/3.2.91/SuSE/noarch/">Language
        packages</a> (all versions and architectures)</li>
    </li>
    <li>
      9.1:
      <a href="http://download.kde.org/binarydownload.html?url=/unstable/3.2.91/SuSE/ix86/9.1/">Intel i586</a> and
      <a href="http://download.kde.org/binarydownload.html?url=/unstable/3.2.91/SuSE/x86_64/9.1/">AMD x86-64</a>
    </li>
  </ul>
  <p />
</li>

<!-- YOPER LINUX -->
<li>
  <a href="http://www.yoper.com/">Yoper</a>:
  <ul type="disc">
    <li>
      1.0:
      <a href="http://download.kde.org/binarydownload.html?url=/unstable/3.2.91/yoper/">Intel i686 rpm</a>
    </li>
  </ul>
  <p />
</li>

</ul>
