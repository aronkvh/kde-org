<ul>

<!-- CONECTIVA LINUX -->
<li><a href="http://www.conectiva.com/">Conectiva Linux</a>
  (<a href="http://download.kde.org/stable/3.2/Conectiva/README">README</a>) :
  <ul type="disc">
    <li>
      CL9:
      <a href="http://download.kde.org/stable/3.2/Conectiva/CL9/conectiva/">Intel i386</a>
    </li>
  </ul>
  <p />
</li>

<!--  DEBIAN -->
<!--
<li><a href="http://www.debian.org/">Debian</a>
  (<a href="http://download.kde.org/stable/3.2/Debian/README">README</a>) :
    <ul type="disc">
      <li>
         Debian stable (woody) (Intel i386) : <tt>deb http://download.kde.org/stable/3.2/Debian stable main</tt>
      </li>
    </ul>
  <p />
</li>
-->

<!--   GENTOO -->
<!--
<li>
  <a href="http://www.gentoo.org/">Gentoo</a>
  (<a href="http://download.kde.org/stable/3.2/Gentoo/README">README</a>):
  <p />
</li>
-->

<!--   MAC OS X (FINK PROJECT) -->
<!--
<li>
  <a href="http://www.apple.com/macosx/">Mac OS</a> /
  <a href="http://www.opendarwin.org/">OpenDarwin</a>
  (downloads via the <a href="http://fink.sourceforge.net/">Fink</a> project):
  <ul type="disc">
    <li>
      <a href="http://fink.sourceforge.net/news/kde.php">X</a>
    </li>
  </ul>
  <p />
</li>
-->

<!--   MANDRAKE LINUX -->
<!--
<li>
  <a href="http://www.linux-mandrake.com/">Mandrake Linux</a>
  (<a href="http://download.kde.org/stable/3.2/Mandrake/README">README</a>):
  <ul type="disc">
    <li>
      8.2:
      <a href="http://download.kde.org/stable/3.2/Mandrake/8.2/">Intel
      i586</a>
    </li>
  </ul>
  <p />
</li>
-->

<!--   Fedora LINUX -->
<li>
  <a href="http://fedora.redhat.com/">Fedora</a>:
  <ul type="disc">
    <li>
      <a href="http://download.kde.org/stable/3.2/RedHat/Fedora/noarch/">Logo and language packages</a>
    </li>
    <li>
      Core 1: <a href="http://download.kde.org/stable/3.2/RedHat/Fedora/i386/">Intel i386</a>
    </li>
  </ul>
  <p />
</li>

<!-- KDE RedHat -->
<li>
  <a href="http://kde-redhat.sourceforge.net/">KDE RedHat</a> (Unofficial contribution):
  <ul type="disc">
    <li>
       7.3: <a href="ftp://apt.kde-redhat.org/apt/kde-redhat/7.3/">RedHat 7.3</a>
    </li>
    <li>
       8.0: <a href="ftp://apt.kde-redhat.org/apt/kde-redhat/8.0/">RedHat 8.0</a>
    </li>
    <li>
       9: <a href="ftp://apt.kde-redhat.org/apt/kde-redhat/9/">RedHat 9</a>
    </li>
    <li>
       Fedora Core 1: <a href="ftp://apt.kde-redhat.org/apt/kde-redhat/1/">Fedora Core 1</a>
    </li>
  </ul>
 <p />
</li>

<!-- PLD LINUX -->
<li>
  <a href="http://www.pld-linux.org">PLD Linux Distribution</a>:
    <ul type="disc">
        <li>2.0 RPMS:
        <a href="ftp://ftp.pld-linux.org/dists/2.0/PLD/amd64/PLD/RPMS/">Amd x86-64</a>,
        <a href="ftp://ftp.pld-linux.org/dists/2.0/PLD/athlon/PLD/RPMS/">Amd Athlon</a>,
        <a href="ftp://ftp.pld-linux.org/dists/2.0/PLD/i686/PLD/RPMS/">Intel i686</a>,
        <a href="ftp://ftp.pld-linux.org/dists/2.0/PLD/i586/PLD/RPMS/">Intel i586</a>,
        <a href="ftp://ftp.pld-linux.org/dists/2.0/PLD/i386/PLD/RPMS/">Intel i386</a>,
        <a href="ftp://ftp.pld-linux.org/dists/2.0/PLD/ppc/PLD/RPMS/">PowerPC 32bit (601 or newer)</a>,
        <a href="ftp://ftp.pld-linux.org/dists/2.0/PLD/alpha/PLD/RPMS/">Alpha AXP 64 bit</a>,
        <a href="ftp://ftp.pld-linux.org/dists/2.0/PLD/sparc/PLD/RPMS/">Sun Sparc 32bit</a>,
        </li>
        <li>2.0 <a href="ftp://ftp.pld-linux.org/dists/2.0/PLD/SRPMS/SRPMS/">SRPMS</a></li>
        <li>2.0 All: Upgrade from the ac tree using poldek
        </li>
    </ul>
  <p />
</li>


<!-- SLACKWARE LINUX -->
<li>
  <a href="http://www.slackware.org/">Slackware</a> (Unofficial contribution):
   <ul type="disc">
     <li>
       <a href="http://download.kde.org/stable/3.2/contrib/Slackware/noarch/">Language packages</a> (all versions)
     </li>
     <li>
       8.1: <a href="http://download.kde.org/stable/3.2/contrib/Slackware/8.1/">Intel i386</a>
     </li>
     <li>
       9.0: <a href="http://download.kde.org/stable/3.2/contrib/Slackware/9.0/">Intel i386</a>
     </li>
     <li>
       9.1: <a href="http://download.kde.org/stable/3.2/contrib/Slackware/9.1/">Intel i486</a>
     </li>
   </ul>
  <p />
</li>

<!-- SOLARIS -->
<!--
<li>
  <a href="http://www.sun.com/solaris/">SUN Solaris</a> (Unofficial contribution):
  <ul type="disc">
    <li>
      8.0: <a href="http://download.kde.org/stable/3.2/contrib/Solaris/8.0/x86/">Intel x86</a>
    </li>
  </ul>
  <p />
</li>
-->

<!--   SUSE LINUX -->
<li>
  <a href="http://www.suse.com/">SuSE Linux</a>
     (<a href="http://download.kde.org/stable/3.2/SuSE/README">README</a>):
  <ul type="disc">
    <li>
      <a href="http://download.kde.org/stable/3.2/SuSE/noarch/">Language packages</a> (all versions and architectures)
    </li>
    <li>
      9.0:
      <a href="http://download.kde.org/stable/3.2/SuSE/ix86/9.0/">Intel i586</a>,
      <a href="http://download.kde.org/stable/3.2/SuSE/x86_64/9.0/">AMD64</a>
    </li>
    <li>
      8.2:
      <a href="http://download.kde.org/stable/3.2/SuSE/ix86/8.2/">Intel i586</a>
    </li>
  </ul>
  <p />
</li>

<!-- TURBO LINUX
<li>
    <a href="http://www.turbolinux.com/">Turbolinux</a>
    (<a href="http://download.kde.org/stable/3.2/Turbo/Readme.txt">README</a>):
    <ul type="disc">
      <li>
        <a href="http://download.kde.org/stable/3.2/Turbo/noarch/">Language
        packages</a> (all versions and architectures)</li>
      <li>
       8.0:
       <a href="http://download.kde.org/stable/3.2/Turbo/8/i586/">Intel i586</a>
      </li>
      <li>
       7.0:
       <a href="http://download.kde.org/stable/3.2/Turbo/7/i586/">Intel i586</a>
      </li>
    </ul>
  <p />
</li>
-->

<!--   TRU64 -->
<!--
<li>
  <a href="http://www.tru64unix.compaq.com/">Tru64</a>
  (<a href="http://download.kde.org/stable/3.2/contrib/Tru64/README.Tru64">README</a>):
  <ul type="disc">
    <li>
      4.0d, e, f and g and 5.x:
      <a href="http://download.kde.org/stable/3.2/contrib/Tru64/">Compaq Alpha</a>
    </li>
  </ul>
  <p />
</li>
-->

<!-- YOPER LINUX
<li>
  <a href="http://www.yoper.com/">Yoper</a>:
  <ul type="disc">
    <li>
      1.0:
      <a href="http://download.kde.org/stable/3.2/Yoper/">Intel i686 tgz</a>
    </li>
  </ul>
  <p />
</li>
-->

<!-- VECTOR LINUX
<li>
  <a href="http://www.vectorlinux.com/">VECTORLINUX</a>:
  (<a href="http://download.kde.org/stable/3.2/contrib/Vector/3.0/README">README</a>):
  <ul type="disc">
    <li>
      3.0:
      <a href="http://download.kde.org/stable/3.2/contrib/Vector/3.0/">Intel i386</a>
    </li>
  </ul>
  <p />
</li>
-->

</ul>
