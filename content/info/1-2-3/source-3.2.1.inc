<table border="0" cellpadding="4" cellspacing="0">
<tr valign="top">
  <th align="left">Location</th>
  <th align="left">Size</th>
  <th align="left">MD5&nbsp;Sum</th>
</tr>
<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.2.1/src/arts-1.2.1.tar.bz2">arts-1.2.1</a></td>
   <td align="right">1020kB</td>
   <td><tt>84877632917893438c629803e7b004f3</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.2.1/src/kde-i18n-3.2.1.tar.bz2">kde-i18n-3.2.1</a></td>
   <td align="right">165MB</td>
   <td><tt>3e076e6074df8a5abb9bdb6542c017bc</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.2.1/src/kdeaccessibility-3.2.1.tar.bz2">kdeaccessibility-3.2.1</a></td>
   <td align="right">1.7MB</td>
   <td><tt>3a6215b9b3b7a1d7bb6e51863066a61e</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.2.1/src/kdeaddons-3.2.1.tar.bz2">kdeaddons-3.2.1</a></td>
   <td align="right">1.8MB</td>
   <td><tt>c73c1777d3f5c25f46cd782fcca9fc35</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.2.1/src/kdeadmin-3.2.1.tar.bz2">kdeadmin-3.2.1</a></td>
   <td align="right">2.0MB</td>
   <td><tt>1ff46933b955cb4bc71cd533c6f730d1</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.2.1/src/kdeartwork-3.2.1.tar.bz2">kdeartwork-3.2.1</a></td>
   <td align="right">16MB</td>
   <td><tt>8c539b4ba4550e8355df03992c36cf58</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.2.1/src/kdebase-3.2.1.tar.bz2">kdebase-3.2.1</a></td>
   <td align="right">16MB</td>
   <td><tt>3a3d89b0b49dbb1eaa9e71c83019bbef</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.2.1/src/kdebindings-3.2.1.tar.bz2">kdebindings-3.2.1</a></td>
   <td align="right">10MB</td>
   <td><tt>d9bd456cb3896bf652a21c66a8f6260f</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.2.1/src/kdeedu-3.2.1.tar.bz2">kdeedu-3.2.1</a></td>
   <td align="right">21MB</td>
   <td><tt>e6eea40d3e26426e34f02bba8ffb72e2</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.2.1/src/kdegames-3.2.1.tar.bz2">kdegames-3.2.1</a></td>
   <td align="right">9.2MB</td>
   <td><tt>60b05fa22dfc0ec812ca88dacb0249aa</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.2.1/src/kdegraphics-3.2.1.tar.bz2">kdegraphics-3.2.1</a></td>
   <td align="right">5.9MB</td>
   <td><tt>5a1676b46efdd64be2f9e9604f04b176</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.2.1/src/kdelibs-3.2.1.tar.bz2">kdelibs-3.2.1</a></td>
   <td align="right">12MB</td>
   <td><tt>50ae60072c1fc4ae4e41694bc2325dcb</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.2.1/src/kdemultimedia-3.2.1.tar.bz2">kdemultimedia-3.2.1</a></td>
   <td align="right">5.2MB</td>
   <td><tt>6e03faa44ff7fdf60fd2fef0d23d5c43</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.2.1/src/kdenetwork-3.2.1.tar.bz2">kdenetwork-3.2.1</a></td>
   <td align="right">6.4MB</td>
   <td><tt>c9135b09191624490590a014da4bfb3d</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.2.1/src/kdepim-3.2.1.tar.bz2">kdepim-3.2.1</a></td>
   <td align="right">7.9MB</td>
   <td><tt>362bec23869328a85845e0501020c938</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.2.1/src/kdesdk-3.2.1.tar.bz2">kdesdk-3.2.1</a></td>
   <td align="right">4.2MB</td>
   <td><tt>f52a69718705b32f0068fb605b604b45</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.2.1/src/kdetoys-3.2.1.tar.bz2">kdetoys-3.2.1</a></td>
   <td align="right">2.8MB</td>
   <td><tt>217c8c2ddb4040abe13a719d6b18e807</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.2.1/src/kdeutils-3.2.1.tar.bz2">kdeutils-3.2.1</a></td>
   <td align="right">2.9MB</td>
   <td><tt>152030a2912609997d73d7af0bb96d52</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.2.1/src/kdevelop-3.0.2.tar.bz2">kdevelop-3.0.2</a></td>
   <td align="right">5.7MB</td>
   <td><tt>1c7df5b7abfc1981922f8a681ab93bb0</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.2.1/src/quanta-3.2.1.tar.bz2">quanta-3.2.1</a></td>
   <td align="right">3.9MB</td>
   <td><tt>37990f042bdc48b6d0e9e714549ad3d7</tt></td>
</tr>

</table>
