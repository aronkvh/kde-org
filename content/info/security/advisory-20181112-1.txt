KDE Project Security Advisory
=============================

Title:          kio-extras: HTML Thumbnailer automatic remote file access
Risk Rating:    Low
CVE:            CVE-2018-19120
Versions:       KDE Applications < 18.12.0
Date:           12 November 2018


Overview
========
Various KDE applications share a plugin system to create thumbnails
of various file types for displaying in file managers, file dialogs, etc.

kio-extras contains a thumbnailer plugin for HTML files.

The HTML thumbnailer was incorrectly accessing some content of
remote URLs listed in HTML files. This meant that the owners of the servers
referred in HTML files in your system could have seen in their access logs
your IP address every time the thumbnailer tried to create the thumbnail.

The HTML thumbnailer has been removed in upcoming KDE Applications 18.12.0
because it was actually not creating thumbnails for files at all.

Workaround
==========
Remove the HTML Thumbnailer plugin from your system.
The file name is htmlthumbnail.so and should be in your Qt plugin path.
The Qt plugin path can be queried with
    qmake -query QT_INSTALL_PLUGINS

Solution
========
Update to KDE Applications >= 18.12.0

Credits
=======
Thanks to Dennis "demlak" Klose for the report.
