
KDE Security Advisory: khtml/konqueror title XSS vulnerability
Original Release Date: 2007-02-06
URL: http://www.kde.org/info/security/advisory-20070206-1.txt

0. References
        CVE-2007-0537


1. Systems affected:

        KDE including KDE 3.5.6.


2. Overview:

        Jose Avila noticed that there is a possibility to inject
        javascript references in <title> tags on websites that allow
        user supplied data to be embeded inside the page title and
        do not properly escape the text.


3. Impact:

        On affected websites it is possible to conduct XSS attacks
        and steal authorisation data.


4. Solution:

        Source code patches have been made available which fix these
        vulnerabilities. Contact your OS vendor / binary package provider
        for information about how to obtain updated binary packages.


5. Patch:

        Patch for KDE 3.5.6 and newer
        ftp://ftp.kde.org/pub/kde/security_patches :

        edc2cba17795356e98eba6f3841c6277  post-3.5.6-kdelibs.diff



