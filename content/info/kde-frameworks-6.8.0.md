---
version: "6.8.0"
title: "KDE Frameworks 6.8.0"
type: info/frameworks6
date: 2024-11-08
signer: Nicolas Fella
signing_fingerprint: 90A968ACA84537CC27B99EAF2C8DF587A6D4AAC1
draft: false
---
