---
title: "KDE Applications 14.11.90 Info Page"
announcement: "/announcements/announce-applications-18.12-beta2"
layout: applications
build_instructions: "https://techbase.kde.org/Getting_Started/Build/Historic/KDE4"
---
